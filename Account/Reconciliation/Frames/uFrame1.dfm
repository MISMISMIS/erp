object Frame1: TFrame1
  Left = 0
  Top = 0
  Width = 951
  Height = 552
  TabOrder = 0
  object Grid: TcxGrid
    Left = 0
    Top = 0
    Width = 951
    Height = 552
    Align = alClient
    BorderStyle = cxcbsNone
    TabOrder = 0
    ExplicitLeft = 3
    ExplicitTop = 7
    ExplicitWidth = 782
    ExplicitHeight = 338
    object tvViewTable: TcxGridDBBandedTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.DataRowHeight = 30
      OptionsView.Footer = True
      OptionsView.HeaderHeight = 30
      OptionsView.Indicator = True
      OptionsView.IndicatorWidth = 20
      OptionsView.BandHeaderHeight = 30
      Bands = <
        item
        end
        item
          Caption = #36134#30446#33539#22260#9
        end
        item
          Width = 532
        end>
      object cxGridDBBandedColumn34: TcxGridDBBandedColumn
        Caption = #24207#21495
        HeaderAlignmentHorz = taCenter
        Width = 60
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object cxGridDBBandedColumn35: TcxGridDBBandedColumn
        Caption = #32534#21495
        HeaderAlignmentHorz = taCenter
        Width = 70
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cxGridDBBandedColumn36: TcxGridDBBandedColumn
        Caption = #24405#20837#26085#26399
        HeaderAlignmentHorz = taCenter
        Width = 70
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object cxGridDBBandedColumn37: TcxGridDBBandedColumn
        Caption = #20844#21496#21517#31216
        HeaderAlignmentHorz = taCenter
        Width = 70
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object cxGridDBBandedColumn38: TcxGridDBBandedColumn
        Caption = #24448#26469#21333#20301
        HeaderAlignmentHorz = taCenter
        Width = 70
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object cxGridDBBandedColumn39: TcxGridDBBandedColumn
        Caption = #36153#29992#31867#21035
        HeaderAlignmentHorz = taCenter
        Width = 70
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object cxGridDBBandedColumn40: TcxGridDBBandedColumn
        Caption = #24037#31243#21517#31216
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object cxGridDBBandedColumn41: TcxGridDBBandedColumn
        Caption = #24320#22987#26085#26399
        HeaderAlignmentHorz = taCenter
        Width = 70
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object cxGridDBBandedColumn42: TcxGridDBBandedColumn
        Caption = #32467#26463#26085#26399
        HeaderAlignmentHorz = taCenter
        Width = 70
        Position.BandIndex = 1
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cxGridDBBandedColumn43: TcxGridDBBandedColumn
        Caption = #37329#39069
        PropertiesClassName = 'TcxCalcEditProperties'
        HeaderAlignmentHorz = taCenter
        Width = 78
        Position.BandIndex = 2
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object cxGridDBBandedColumn44: TcxGridDBBandedColumn
        Caption = #24635#20215#37329#39069#22823#20889
        HeaderAlignmentHorz = taCenter
        Width = 73
        Position.BandIndex = 2
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object cxGridDBBandedColumn45: TcxGridDBBandedColumn
        Caption = #22791#27880
        HeaderAlignmentHorz = taCenter
        Width = 142
        Position.BandIndex = 2
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object tvViewTableColumn1: TcxGridDBBandedColumn
        Caption = #24080#30446#31867#21035
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object tvViewTableColumn2: TcxGridDBBandedColumn
        Caption = #21046#34920#31867#22411
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object tvViewTableColumn3: TcxGridDBBandedColumn
        Caption = #24080#30446#26126#32454
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object tvViewTableColumn5: TcxGridDBBandedColumn
        Caption = #20132#26131#29366#24577
        Position.BandIndex = 2
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object tvViewTableColumn6: TcxGridDBBandedColumn
        Caption = #23457#26680#29366#24577
        Position.BandIndex = 2
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
    end
    object Lv: TcxGridLevel
      GridView = tvViewTable
    end
  end
  object Button1: TButton
    Left = 592
    Top = 200
    Width = 161
    Height = 73
    Caption = 'Button1'
    TabOrder = 1
    OnClick = Button1Click
  end
end
