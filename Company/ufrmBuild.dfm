inherited frmBuild: TfrmBuild
  Caption = #20844#21496#33829#25910
  ClientHeight = 583
  ClientWidth = 977
  OnClose = FormClose
  ExplicitLeft = 8
  ExplicitWidth = 993
  ExplicitHeight = 621
  PixelsPerInch = 96
  TextHeight = 13
  inherited Splitter1: TSplitter
    Height = 583
    ExplicitHeight = 583
  end
  inherited Left: TPanel
    Height = 583
    ExplicitHeight = 583
    inherited TreeView1: TTreeView
      Height = 552
      ExplicitHeight = 552
    end
    inherited RzToolbar3: TRzToolbar
      ToolbarControls = (
        RzSpacer16
        RzToolButton17
        RzSpacer17
        RzToolButton18
        RzSpacer18
        RzToolButton19)
      inherited RzToolButton17: TRzToolButton
        Width = 76
        Caption = #26032#22686#39033#30446
        ExplicitWidth = 76
      end
      inherited RzSpacer17: TRzSpacer
        Left = 88
        ExplicitLeft = 88
      end
      inherited RzToolButton18: TRzToolButton
        Left = 96
        Width = 52
        ExplicitLeft = 96
        ExplicitWidth = 52
      end
      inherited RzSpacer18: TRzSpacer
        Left = 148
        ExplicitLeft = 148
      end
      inherited RzToolButton19: TRzToolButton
        Left = 156
        Width = 52
        ExplicitLeft = 156
        ExplicitWidth = 52
      end
    end
  end
  inherited RzPanel19: TRzPanel
    Width = 724
    Height = 583
    ExplicitWidth = 724
    ExplicitHeight = 583
    inherited RzPanel3: TRzPanel
      Width = 724
      ExplicitWidth = 724
    end
    inherited pcSys: TRzPageControl
      Width = 724
      Height = 545
      ExplicitWidth = 724
      ExplicitHeight = 545
      FixedDimension = 28
      inherited TabSheet1: TRzTabSheet
        ExplicitWidth = 722
        ExplicitHeight = 512
        inherited RzToolbar1: TRzToolbar
          Width = 722
          Height = 54
          ExplicitWidth = 722
          ExplicitHeight = 54
          ToolbarControls = (
            RzSpacer1
            RzToolButton1
            RzSpacer2
            RzToolButton2
            RzSpacer3
            RzToolButton3
            RzSpacer4
            RzToolButton4
            RzSpacer7
            RzToolButton5
            RzSpacer19
            cxLabel1
            cxDateEdit1
            RzSpacer20
            cxLabel2
            cxDateEdit2
            RzSpacer21
            RzToolButton15
            RzSpacer11
            RzToolButton6)
          inherited RzToolButton4: TRzToolButton
            Width = 76
            ExplicitWidth = 76
          end
          inherited RzToolButton5: TRzToolButton
            Left = 315
            ExplicitLeft = 315
          end
          inherited RzToolButton6: TRzToolButton
            Left = 12
            Top = 27
            Width = 52
            ExplicitLeft = 12
            ExplicitTop = 27
            ExplicitWidth = 52
          end
          inherited RzSpacer7: TRzSpacer
            Left = 307
            ExplicitLeft = 307
          end
          inherited RzSpacer19: TRzSpacer
            Left = 380
            ExplicitLeft = 380
          end
          inherited RzSpacer20: TRzSpacer
            Left = 534
            ExplicitLeft = 534
          end
          inherited RzSpacer21: TRzSpacer
            Left = 684
            ExplicitLeft = 684
          end
          inherited RzSpacer11: TRzSpacer
            Left = 4
            Top = 27
            ExplicitLeft = 4
            ExplicitTop = 27
          end
          inherited RzToolButton15: TRzToolButton
            Left = 692
            ExplicitLeft = 692
          end
          inherited cxLabel1: TcxLabel
            Left = 388
            ExplicitLeft = 388
          end
          inherited cxDateEdit1: TcxDateEdit
            Left = 444
            ExplicitLeft = 444
          end
          inherited cxLabel2: TcxLabel
            Left = 538
            ExplicitLeft = 538
          end
          inherited cxDateEdit2: TcxDateEdit
            Left = 594
            ExplicitLeft = 594
          end
        end
        inherited cxGrid2: TcxGrid
          Top = 54
          Width = 722
          Height = 458
          ExplicitTop = 54
          ExplicitWidth = 722
          ExplicitHeight = 458
          inherited tvDealGrid: TcxGridDBTableView
            inherited tvDealGridColumn4: TcxGridDBColumn
              Caption = #39033#30446#21517#31216
            end
          end
        end
      end
    end
  end
end
