unit ufrmCompanyWork;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, RzPanel, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  Vcl.ComCtrls, dxCore, cxDateUtils, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData, cxTextEdit,
  cxCheckBox, cxCalendar, cxDBLookupComboBox, cxDropDownEdit, cxMemo,
  cxCurrencyEdit, cxSpinEdit, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  cxMaskEdit, cxLabel, RzButton, cxGridBandedTableView, Vcl.Menus, RzTabs,ufrmBaseController,
  cxLookupEdit, cxDBLookupEdit, Data.Win.ADODB, Vcl.StdCtrls, cxGroupBox,
  cxDBLabel, dxScreenTip, dxCustomHint, cxHint, cxGridDBBandedTableView,
  Datasnap.DBClient,UnitADO;

type
  TfrmCompanyWork = class(TfrmBaseController)
    RzPanel2: TRzPanel;
    Splitter1: TSplitter;
    PeopleGrid: TcxGrid;
    tvpeopleView: TcxGridDBTableView;
    tvpeopleViewCol1: TcxGridDBColumn;
    tvpeopleViewCol2: TcxGridDBColumn;
    peopleLv: TcxGridLevel;
    qry: TADOQuery;
    ds: TDataSource;
    DataDays: TDataSource;
    ADODays: TADOQuery;
    tvpeopleViewCol4: TcxGridDBColumn;
    ADOOvertime: TADOQuery;
    DataOvertime: TDataSource;
    tvpeopleViewColumn1: TcxGridDBColumn;
    tvpeopleViewColumn2: TcxGridDBColumn;
    tvpeopleViewColumn3: TcxGridDBColumn;
    Splitter3: TSplitter;
    ADOCompany: TADOQuery;
    DataCompany: TDataSource;
    tvpeopleViewColumn4: TcxGridDBColumn;
    tvpeopleViewColumn5: TcxGridDBColumn;
    tvpeopleViewColumn6: TcxGridDBColumn;
    tvpeopleViewColumn7: TcxGridDBColumn;
    RzToolbar1: TRzToolbar;
    RzSpacer23: TRzSpacer;
    RzSpacer24: TRzSpacer;
    cxLabel13: TcxLabel;
    cxLabel14: TcxLabel;
    duties: TcxLookupComboBox;
    Section: TcxLookupComboBox;
    RzSpacer25: TRzSpacer;
    RzToolbar5: TRzToolbar;
    RzSpacer26: TRzSpacer;
    cxLabel15: TcxLabel;
    RzSpacer27: TRzSpacer;
    RzToolButton5: TRzToolButton;
    RzSpacer28: TRzSpacer;
    RzToolButton6: TRzToolButton;
    Print: TPopupMenu;
    Excel1: TMenuItem;
    N2: TMenuItem;
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    BasicHint: TBalloonHint;
    StatisticsHint: TBalloonHint;
    BalloonHint1: TBalloonHint;
    Master: TClientDataSet;
    ds2: TDataSource;
    MasterId: TIntegerField;
    MasterSignName: TWideStringField;
    Masterduties: TWideStringField;
    MasterWorkValue: TFloatField;
    MasterInputDate: TDateTimeField;
    ClientDataSet1: TClientDataSet;
    IntegerField1: TIntegerField;
    WideStringField1: TWideStringField;
    WideStringField3: TWideStringField;
    WideStringField4: TWideStringField;
    WideStringField5: TWideStringField;
    WideStringField6: TWideStringField;
    WideStringField7: TWideStringField;
    WideStringField8: TWideStringField;
    DateTimeField1: TDateTimeField;
    IntegerField2: TIntegerField;
    DataSource2: TDataSource;
    MasterWorkDays: TIntegerField;
    ClientDataSet1ParentId: TWideStringField;
    MasterParentId: TWideStringField;
    Masternumbers: TWideStringField;
    ClientDataSet1numbers: TWideStringField;
    MasterProjectName: TWideStringField;
    MasterRemarks: TWideMemoField;
    PopupMenu1: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    Search: TcxLookupComboBox;
    RzPanel15: TRzPanel;
    RzToolbar3: TRzToolbar;
    RzSpacer12: TRzSpacer;
    RzSpacer13: TRzSpacer;
    RzToolButton13: TRzToolButton;
    cxLabel2: TcxLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    Grid: TcxGrid;
    tvCompany: TcxGridDBTableView;
    tvCompanyColumn1: TcxGridDBColumn;
    tvCompanyColumn2: TcxGridDBColumn;
    lv: TcxGridLevel;
    MasterTreeId: TIntegerField;
    ClientDataSet1TreeId: TIntegerField;
    PopupMenu2: TPopupMenu;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    PopupMenu3: TPopupMenu;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    pmDays: TPopupMenu;
    N1: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N11: TMenuItem;
    N01: TMenuItem;
    N051: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    pmFrozen: TPopupMenu;
    MenuItem16: TMenuItem;
    MenuItem17: TMenuItem;
    pmOvertime: TPopupMenu;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    MenuItem10: TMenuItem;
    MenuItem11: TMenuItem;
    MenuItem18: TMenuItem;
    MenuItem19: TMenuItem;
    RzPanel1: TRzPanel;
    RzPageControl1: TRzPageControl;
    TabSheet1: TRzTabSheet;
    RzToolbar13: TRzToolbar;
    RzSpacer30: TRzSpacer;
    but1: TRzToolButton;
    RzSpacer57: TRzSpacer;
    But4: TRzToolButton;
    RzSpacer113: TRzSpacer;
    But6: TRzToolButton;
    RzSpacer114: TRzSpacer;
    But3: TRzToolButton;
    RzSpacer120: TRzSpacer;
    RzSpacer6: TRzSpacer;
    RzSpacer7: TRzSpacer;
    RzSpacer16: TRzSpacer;
    But7: TRzToolButton;
    RzToolButton12: TRzToolButton;
    RzSpacer18: TRzSpacer;
    RzToolButton16: TRzToolButton;
    RzSpacer20: TRzSpacer;
    but2: TRzToolButton;
    RzSpacer17: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzSpacer42: TRzSpacer;
    RzToolButton21: TRzToolButton;
    cxComboBox1: TcxComboBox;
    cxLabel1: TcxLabel;
    cxLabel4: TcxLabel;
    year: TcxComboBox;
    GDays: TcxGrid;
    tvDays: TcxGridDBTableView;
    tvDaysCol1: TcxGridDBColumn;
    tvDaysCol2: TcxGridDBColumn;
    tvDaysCol3: TcxGridDBColumn;
    tvDaysCol4: TcxGridDBColumn;
    tvDaysColumn2: TcxGridDBColumn;
    tvDaysColumn3: TcxGridDBColumn;
    tvDaysColumn4: TcxGridDBColumn;
    tvDaysColumn5: TcxGridDBColumn;
    tvDaysColumn6: TcxGridDBColumn;
    tvDaysColumn7: TcxGridDBColumn;
    tvDaysColumn8: TcxGridDBColumn;
    tvDaysColumn9: TcxGridDBColumn;
    tvDaysColumn10: TcxGridDBColumn;
    tvDaysColumn11: TcxGridDBColumn;
    tvDaysColumn12: TcxGridDBColumn;
    tvDaysColumn13: TcxGridDBColumn;
    tvDaysColumn14: TcxGridDBColumn;
    tvDaysColumn15: TcxGridDBColumn;
    tvDaysColumn16: TcxGridDBColumn;
    tvDaysColumn17: TcxGridDBColumn;
    tvDaysColumn18: TcxGridDBColumn;
    tvDaysColumn19: TcxGridDBColumn;
    tvDaysColumn20: TcxGridDBColumn;
    tvDaysColumn21: TcxGridDBColumn;
    tvDaysColumn22: TcxGridDBColumn;
    tvDaysColumn23: TcxGridDBColumn;
    tvDaysColumn24: TcxGridDBColumn;
    tvDaysColumn25: TcxGridDBColumn;
    tvDaysColumn26: TcxGridDBColumn;
    tvDaysColumn27: TcxGridDBColumn;
    tvDaysColumn28: TcxGridDBColumn;
    tvDaysColumn29: TcxGridDBColumn;
    tvDaysColumn30: TcxGridDBColumn;
    tvDaysColumn31: TcxGridDBColumn;
    tvDaysColumn32: TcxGridDBColumn;
    tvDaysCol5: TcxGridDBColumn;
    tvDaysCol6: TcxGridDBColumn;
    tvDaysCol7: TcxGridDBColumn;
    tvDaysCol8: TcxGridDBColumn;
    tvDaysColumn34: TcxGridDBColumn;
    Lv1: TcxGridLevel;
    TabSheet2: TRzTabSheet;
    RzToolbar2: TRzToolbar;
    RzSpacer8: TRzSpacer;
    But8: TRzToolButton;
    RzSpacer9: TRzSpacer;
    But10: TRzToolButton;
    RzSpacer10: TRzSpacer;
    But11: TRzToolButton;
    But9: TRzToolButton;
    RzSpacer11: TRzSpacer;
    RzSpacer5: TRzSpacer;
    But12: TRzToolButton;
    RzSpacer19: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzToolButton19: TRzToolButton;
    RzSpacer38: TRzSpacer;
    RzSpacer39: TRzSpacer;
    RzSpacer41: TRzSpacer;
    RzSpacer43: TRzSpacer;
    RzToolButton22: TRzToolButton;
    RzSpacer44: TRzSpacer;
    cxLabel3: TcxLabel;
    cxDateEdit3: TcxDateEdit;
    cxLabel7: TcxLabel;
    cxDateEdit4: TcxDateEdit;
    WorkGrid: TcxGrid;
    tvOvertime: TcxGridDBTableView;
    tvOvertimeCol1: TcxGridDBColumn;
    tvOvertimeColumn2: TcxGridDBColumn;
    tvOvertimeColumn1: TcxGridDBColumn;
    tvOvertimeCol2: TcxGridDBColumn;
    tvOvertimeCol3: TcxGridDBColumn;
    tvOvertimeColumn3: TcxGridDBColumn;
    tvOvertimeCol4: TcxGridDBColumn;
    tvOvertimeCol5: TcxGridDBColumn;
    tvOvertimeCol6: TcxGridDBColumn;
    tvOvertimeCol7: TcxGridDBColumn;
    lv2: TcxGridLevel;
    TabSheet3: TRzTabSheet;
    RzPanel3: TRzPanel;
    RzPanel4: TRzPanel;
    RzPanel5: TRzPanel;
    RzPanel6: TRzPanel;
    RzPanel10: TRzPanel;
    RzPanel11: TRzPanel;
    RzPanel7: TRzPanel;
    RzPanel8: TRzPanel;
    RzPanel12: TRzPanel;
    RzPanel13: TRzPanel;
    RzToolbar4: TRzToolbar;
    RzSpacer14: TRzSpacer;
    RzSpacer2: TRzSpacer;
    RzSpacer3: TRzSpacer;
    RzSpacer4: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzToolButton4: TRzToolButton;
    RzSpacer1: TRzSpacer;
    cxLabel8: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel9: TcxLabel;
    cxLabel10: TcxLabel;
    cxLabel11: TcxLabel;
    RzPanel16: TRzPanel;
    RzPanel14: TRzPanel;
    RzToolbar7: TRzToolbar;
    RzSpacer34: TRzSpacer;
    RzToolButton9: TRzToolButton;
    RzSpacer35: TRzSpacer;
    RzToolButton17: TRzToolButton;
    RzSpacer36: TRzSpacer;
    RzToolButton18: TRzToolButton;
    RzSpacer37: TRzSpacer;
    RzToolButton20: TRzToolButton;
    RzSpacer40: TRzSpacer;
    RzToolButton15: TRzToolButton;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    tvGrid2: TcxGridDBBandedTableView;
    cxGridDBBandedColumn1: TcxGridDBBandedColumn;
    cxGridDBBandedColumn2: TcxGridDBBandedColumn;
    cxGridDBBandedColumn3: TcxGridDBBandedColumn;
    cxGridDBBandedColumn8: TcxGridDBBandedColumn;
    cxGridDBBandedColumn9: TcxGridDBBandedColumn;
    cxGridDBBandedColumn10: TcxGridDBBandedColumn;
    cxGridDBBandedColumn11: TcxGridDBBandedColumn;
    cxGridDBBandedColumn12: TcxGridDBBandedColumn;
    tvGrid2Column1: TcxGridDBBandedColumn;
    tvParentIdA: TcxGridDBBandedColumn;
    tvGrid2Column2: TcxGridDBBandedColumn;
    tvGrid2Column3: TcxGridDBBandedColumn;
    Lv4: TcxGridLevel;
    Splitter4: TSplitter;
    RzPanel9: TRzPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1Column2: TcxGridDBColumn;
    cxGrid1DBTableView1Column3: TcxGridDBColumn;
    cxGrid1DBTableView1Column4: TcxGridDBColumn;
    cxGrid1DBTableView1Column5: TcxGridDBColumn;
    tvGrid1: TcxGridDBBandedTableView;
    tvWorkCol2: TcxGridDBBandedColumn;
    tvWorkCol1: TcxGridDBBandedColumn;
    tvWorkCol8: TcxGridDBBandedColumn;
    tvWorkCol9: TcxGridDBBandedColumn;
    tvWorkCol10: TcxGridDBBandedColumn;
    tvWorkCol11: TcxGridDBBandedColumn;
    tvGrid1Column2: TcxGridDBBandedColumn;
    tvWorkId: TcxGridDBBandedColumn;
    tvParentId: TcxGridDBBandedColumn;
    tvWorkCol12: TcxGridDBBandedColumn;
    tvWorkCol13: TcxGridDBBandedColumn;
    tvGrid1Column1: TcxGridDBBandedColumn;
    Lv3: TcxGridLevel;
    RzToolbar6: TRzToolbar;
    RzSpacer15: TRzSpacer;
    RzToolButton7: TRzToolButton;
    RzSpacer21: TRzSpacer;
    RzToolButton8: TRzToolButton;
    RzSpacer22: TRzSpacer;
    RzToolButton10: TRzToolButton;
    RzSpacer29: TRzSpacer;
    RzSpacer32: TRzSpacer;
    RzToolButton14: TRzToolButton;
    Btn6: TRzToolButton;
    RzSpacer31: TRzSpacer;
    RzSpacer33: TRzSpacer;
    RzToolButton11: TRzToolButton;
    cxLabel17: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxLabel37: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    RzSpacer45: TRzSpacer;
    RzToolButton23: TRzToolButton;
    RzSpacer46: TRzSpacer;
    RzToolButton24: TRzToolButton;
    PopupMenu4: TPopupMenu;
    N10: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    PopupMenu5: TPopupMenu;
    MenuItem12: TMenuItem;
    MenuItem13: TMenuItem;
    MenuItem14: TMenuItem;
    MenuItem15: TMenuItem;
    RzSpacer47: TRzSpacer;
    RzToolButton25: TRzToolButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
    procedure tvpeopleViewCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvpeopleViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvpeopleViewDblClick(Sender: TObject);
    procedure cxComboBox1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cxLookupComboBox1Enter(Sender: TObject);
    procedure cxLookupComboBox1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure but1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure ADODaysAfterInsert(DataSet: TDataSet);
    procedure RzToolButton12Click(Sender: TObject);
    procedure tvDaysEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure RzToolButton16Click(Sender: TObject);
    procedure but2Click(Sender: TObject);
    procedure But3Click(Sender: TObject);
    procedure But4Click(Sender: TObject);
    procedure But7Click(Sender: TObject);
    procedure tvDaysColumn2PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvDaysColumn37StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure tvDaysDblClick(Sender: TObject);
    procedure But8Click(Sender: TObject);
    procedure But9Click(Sender: TObject);
    procedure But10Click(Sender: TObject);
    procedure cxComboBox1PropertiesCloseUp(Sender: TObject);
    procedure yearPropertiesCloseUp(Sender: TObject);
    procedure tvDaysTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvDaysTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems1GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure ADODaysAfterOpen(DataSet: TDataSet);
    procedure tvDaysColumn2StylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure RzToolButton13Click(Sender: TObject);
    procedure tvCompanyDblClick(Sender: TObject);
    procedure ADODaysAfterClose(DataSet: TDataSet);
    procedure tvDaysKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ADOOvertimeAfterInsert(DataSet: TDataSet);
    procedure yearMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ADOOvertimeAfterOpen(DataSet: TDataSet);
    procedure ADOOvertimeAfterClose(DataSet: TDataSet);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure tvOvertimeTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvOvertimeCol3PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvOvertimeCol4PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvOvertimeEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvOvertimeTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems1GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvOvertimeEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvDaysCol5StylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure RzToolButton3Click(Sender: TObject);
    procedure RzToolButton5Click(Sender: TObject);
    procedure RzToolButton6Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure SectionPropertiesCloseUp(Sender: TObject);
    procedure dutiesPropertiesCloseUp(Sender: TObject);
    procedure tvOvertimeDblClick(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure tvOvertimeColumn2PropertiesChange(Sender: TObject);
    procedure tvOvertimeStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure tvDaysStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure tvDaysCol2PropertiesChange(Sender: TObject);
    procedure RzToolButton7Click(Sender: TObject);
    procedure RzToolButton14Click(Sender: TObject);
    procedure RzToolButton10Click(Sender: TObject);
    procedure RzToolButton8Click(Sender: TObject);
    procedure MasterAfterInsert(DataSet: TDataSet);
    procedure tvGrid1Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure tvGrid1DblClick(Sender: TObject);
    procedure RzToolButton9Click(Sender: TObject);
    procedure ClientDataSet1AfterInsert(DataSet: TDataSet);
    procedure MasterAfterOpen(DataSet: TDataSet);
    procedure ClientDataSet1AfterOpen(DataSet: TDataSet);
    procedure RzToolButton18Click(Sender: TObject);
    procedure RzToolButton20Click(Sender: TObject);
    procedure RzToolButton17Click(Sender: TObject);
    procedure tvGrid2Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvGrid2DblClick(Sender: TObject);
    procedure Btn6Click(Sender: TObject);
    procedure tvWorkCol9PropertiesCloseUp(Sender: TObject);
    procedure cxGridDBBandedColumn3PropertiesCloseUp(Sender: TObject);
    procedure ClientDataSet1AfterClose(DataSet: TDataSet);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure SearchPropertiesCloseUp(Sender: TObject);
    procedure RzToolButton19Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure MenuItem16Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N01Click(Sender: TObject);
    procedure N051Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure MenuItem7Click(Sender: TObject);
    procedure MenuItem8Click(Sender: TObject);
    procedure MenuItem9Click(Sender: TObject);
    procedure MenuItem10Click(Sender: TObject);
    procedure MenuItem18Click(Sender: TObject);
    procedure MenuItem19Click(Sender: TObject);
    procedure RzPanel16Resize(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure N14Click(Sender: TObject);
    procedure MenuItem12Click(Sender: TObject);
    procedure MenuItem13Click(Sender: TObject);
    procedure MenuItem14Click(Sender: TObject);
    procedure MenuItem15Click(Sender: TObject);
  private
    { Private declarations }
    dwIsSelectCompany : Boolean;
    dwIsSelectStaff   : Boolean;
    dwDaysSumMoney : Currency;
    dwOverSumMoney : Currency;
    dwFullname : string;
    dwParent   : string;
    dwUnitPrice: Currency;
    dwOverPrice: Currency;
    dwwagestype: string;
    dwLeastWork: Integer;
    dwsection  : string;
    dwNumbers  : string;
    dwADO      : TADO;
    dwBlendSQL : string;
    dwBlendProjectRecordSQL : string;

    dwCompayInfoIndex : Integer;
    function GetCompayInfoList(lpCompayName : string):Boolean;
    function GetSumMoney():Boolean;
    function TheValue(lpValue : Double):Boolean;
    procedure ViewHint();
    function GetStaffId(lpSingName : string):string;
  public
    { Public declarations }
  end;

var
  frmCompanyWork: TfrmCompanyWork;

implementation

uses
   uDataModule,global,DateUtils,ufrmEnclosure,System.Math;

{$R *.dfm}

function TfrmCompanyWork.GetStaffId(lpSingName : string):string;
begin
  with Self.qry do
  begin
    if Locate(tvpeopleViewCol2.DataBinding.FieldName,lpSingName,[loCaseInsensitive,loPartialKey]) then
    begin
      Result := FieldByName( Self.tvpeopleViewColumn7.DataBinding.FieldName ).AsString;
    end;
  end;
  {
  if ClientDataSet1.Locate('Name', '钱a', [loCaseInsensitive,loPartialKey]) then
    ShowMessage(ClientDataSet1.FieldValues['Age']);
  }
end;

procedure TfrmCompanyWork.ViewHint();
begin
  Self.RzToolButton4.Hint := '考勤金额：' + Self.RzPanel11.Caption + #13#10 +
                             '加班金额：' + CurrToStr(dwOverSumMoney);
end;

function TfrmCompanyWork.GetCompayInfoList(lpCompayName : string):Boolean;
var
  s : string;
  szFildName : string;

begin
  inherited;
  with DM.Qry do
  begin
    Close;
    SQL.Text := 'Select * from ' + g_Table_Maintain_CompanyInfoTree + ' Where SignName="' + lpCompayName + '"'; //parent
    Open;
    if RecordCount <> 0 then
    begin
      dwCompayInfoIndex := FieldByName('parent').AsInteger;
    end;
  end;
  S := 'Select * from '+
                g_Table_Maintain_CompanyInfoList +
                ' WHERE TreeId=' + IntToStr( dwCompayInfoIndex );

  if DM.ADOOOPEN(Self.qry,s) <> 0 then
  begin
    Self.Section.Enabled := True;
    Self.duties.Enabled  := True;
    Self.Search.Enabled := True;
    Self.RzToolButton5.Enabled := True;
    Self.RzToolButton6.Enabled := True;
  end else
  begin
    Self.Section.Enabled := False;
    Self.duties.Enabled  := False;
    Self.Search.Enabled := False;
    Self.RzToolButton5.Enabled := False;
    Self.RzToolButton6.Enabled := False;
  end;

  szFildName := Self.tvpeopleViewCol4.DataBinding.FieldName;
  S := 'Select '+ szFildName +' from '+
                g_Table_Maintain_CompanyInfoList +
                ' WHERE TreeId=' + IntToStr( dwCompayInfoIndex ) + ' GROUP BY ' + szFildName;
 if DM.ADOOOPEN(Self.ADOQuery1,s) <> 0 then
 {
  with Self.ADOQuery1 do  //获取职务
  begin
    Close;
    SQL.Text := s ;
    Open;
  end;
  }
end;

procedure TfrmCompanyWork.ADODaysAfterClose(DataSet: TDataSet);
begin
  inherited;
  Self.but1.Enabled := False;
  Self.but2.Enabled := False;
  Self.but3.Enabled := False;
  Self.but4.Enabled := False;
  Self.but6.Enabled := False;
  Self.year.Enabled := False;
  Self.cxComboBox1.Enabled := False;
  Self.RzToolButton12.Enabled := False;
  Self.RzToolButton16.Enabled := False;
  Self.RzToolButton3.Enabled  := False;
  Self.RzToolButton21.Enabled := False;

  Self.N1.Enabled := False;
  Self.N3.Enabled := False;
  Self.N4.Enabled := False;
  Self.N5.Enabled := False;
  Self.N11.Enabled:= False;
  Self.N01.Enabled:= False;
  Self.N051.Enabled:=False;
  Self.N8.Enabled := False;
  Self.N9.Enabled := False;

end;

procedure TfrmCompanyWork.ADODaysAfterInsert(DataSet: TDataSet);
begin
  inherited;
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      FieldByName(Self.tvDaysCol2.DataBinding.FieldName).Value := True;  //状态
      FieldByName(Self.tvDaysCol6.DataBinding.FieldName).Value := dwUnitPrice; //单价
      FieldByName('SignName').Value := dwFullname;
      FieldByName('parent').Value   := dwParent;
      FieldByName('TreeId').Value   := dwCompayInfoIndex;
      FieldByName('code').Value     := dwNumbers;
    end;
  end;
end;

procedure TfrmCompanyWork.ADODaysAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
  Self.but1.Enabled := True;
  Self.but2.Enabled := True;
  Self.but3.Enabled := True;
  Self.but4.Enabled := True;
  Self.but6.Enabled := True;
  Self.year.Enabled := True;
  Self.RzToolButton12.Enabled := True;
  Self.RzToolButton16.Enabled := True;
  Self.cxComboBox1.Enabled := True;
  Self.RzToolButton3.Enabled := True;
  Self.RzToolButton21.Enabled := True;
  Self.N1.Enabled := True;
  Self.N3.Enabled := True;
  Self.N4.Enabled := True;
  Self.N5.Enabled := True;
  Self.N11.Enabled:= True;
  Self.N01.Enabled:= True;
  Self.N051.Enabled:=True;
  Self.N8.Enabled := True;
  Self.N9.Enabled := True;

end;

procedure TfrmCompanyWork.ADOOvertimeAfterClose(DataSet: TDataSet);
begin
  inherited;
  Self.RzToolButton1.Enabled:=False;
  Self.but8.Enabled := False;
  Self.but9.Enabled := False;
  Self.but10.Enabled := False;
  Self.but11.Enabled := False;
  Self.cxDateEdit3.Enabled := False;
  Self.cxDateEdit4.Enabled := False;
  Self.RzToolButton19.Enabled := False;
  Self.RzToolButton22.Enabled := False;
  Self.MenuItem7.Enabled := False;
  Self.MenuItem8.Enabled := False;
  Self.MenuItem9.Enabled := False;
  Self.MenuItem10.Enabled:= False;
  Self.MenuItem19.Enabled:= False;
  Self.MenuItem18.Enabled:= False;
end;

procedure TfrmCompanyWork.ADOOvertimeAfterInsert(DataSet: TDataSet);
begin
  inherited;
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      FieldByName(Self.tvOvertimeCol2.DataBinding.FieldName).Value := Date;
      FieldByName(Self.tvOvertimeCol4.DataBinding.FieldName).Value := dwOverPrice;
      FieldByName(Self.tvOvertimeColumn2.DataBinding.FieldName).Value := True;
      FieldByName('SignName').Value:=dwFullname;
      FieldByName('parent').Value := dwParent;
      FieldByName('TreeId').Value := dwCompayInfoIndex;
      FieldByName('code').Value   := dwNumbers;

    end;
  end;
end;

procedure TfrmCompanyWork.ADOOvertimeAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
  Self.but8.Enabled := True;
  Self.but9.Enabled := True;
  Self.but10.Enabled := True;
  Self.but11.Enabled := True;
  Self.RzToolButton1.Enabled:=True;
  Self.cxDateEdit3.Enabled := True;
  Self.cxDateEdit4.Enabled := True;
  Self.RzToolButton19.Enabled := True;
  Self.RzToolButton22.Enabled := True;
  Self.MenuItem7.Enabled := True;
  Self.MenuItem8.Enabled := True;
  Self.MenuItem9.Enabled := True;
  Self.MenuItem10.Enabled:= True;
  Self.MenuItem19.Enabled:= True;
  Self.MenuItem18.Enabled:= True;
end;

procedure TfrmCompanyWork.cxComboBox1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  Self.cxComboBox1.DroppedDown := True;
end;

procedure TfrmCompanyWork.cxComboBox1PropertiesCloseUp(Sender: TObject);
var
  s : string;
begin
  inherited;
  s := Self.cxComboBox1.Text;
  with Self.ADODays do
  begin
    Close;
    SQL.Text := 'Select * from ' + g_Table_Company_Staff_CheckWork +
                ' Where ' + Self.tvDaysCol3.DataBinding.FieldName + '=' + Self.year.Text + ' AND ' +
                Self.tvDaysCol4.DataBinding.FieldName + '=' + Self.cxComboBox1.Text + ' AND parent=' + dwParent + '';
    Open;
  end;
end;

procedure TfrmCompanyWork.cxGridDBBandedColumn3PropertiesCloseUp(
  Sender: TObject);
var
  DD2 : Variant;

  szRowIndex: Integer;
  szCol1Name: Variant;
  szCol2Name: Variant;
  szSQLText : string;
  szNumbers , DD1 : string;
  i : Integer;
  szIsExist:Boolean;

begin
  inherited;

  with (Sender as TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;
    if szRowIndex >= 0 then
    begin
      szCol1Name := DataController.Values[szRowIndex, 0]; //项目名称
      ////////////////////////////////////////////////////////////////////////
      ///
      szNumbers := Self.tvGrid2Column2.EditValue;
      Self.tvParentId.EditValue := GetStaffId(szCol1Name);
      szIsExist := False;
      with Self.tvGrid2 do
      begin

        for I := 0 to ViewData.RowCount-1 do
        begin
            DD1 := ViewData.Rows[i].Values[Self.tvGrid2Column2.Index]; //编号
            DD2 := ViewData.Rows[i].Values[Self.cxGridDBBandedColumn3.Index]; //工程名称
            if (DD2 = szCol1Name) AND
               (VarToStr( DD1 ) = VarToStr( szNumbers )) and
               (i <> Controller.FocusedRowIndex) then
            begin
              szIsExist := True;
              Break;
            end;
        end;

      end;

      if not szIsExist then
      begin
        Self.cxGridDBBandedColumn3.EditValue  := szCol1Name;
      end else
      begin
        Self.cxGridDBBandedColumn3.EditValue  := null;
        Application.MessageBox('项目名称已存在不可重复！', '新增或编辑记录？', MB_OKCANCEL + MB_ICONWARNING)
      end;

    end;

  end;

end;


procedure TfrmCompanyWork.cxLookupComboBox1Enter(Sender: TObject);
begin
  inherited;
  Self.cxLookupComboBox1.DroppedDown := True;
end;

procedure TfrmCompanyWork.cxLookupComboBox1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  Self.cxLookupComboBox1.DroppedDown := True;
end;

procedure TfrmCompanyWork.cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
begin
  GetCompayInfoList(Self.cxLookupComboBox1.Text);
end;

procedure TfrmCompanyWork.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then Self.RzToolButton5.Click;
end;

procedure TfrmCompanyWork.dutiesPropertiesCloseUp(Sender: TObject);
var
  Section , s : string;
  szColName : string;
  duties : string;

begin
  inherited;
  Section := Self.Section.Text ;
  if Section <> '' then
  begin
    duties := Self.duties.Text;
    szColName := Self.tvpeopleViewColumn6.DataBinding.FieldName;
    S := 'Select * from '+ g_Table_Maintain_CompanyInfoList + ' WHERE TreeId=' +
         IntToStr( dwCompayInfoIndex ) + ' AND ' + szColName + '="' + Section + '" and ' + Self.tvpeopleViewCol4.DataBinding.FieldName + '="' + duties + '"';
    with Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      Open;
    end;
  end else
  begin
    ShowMessage('请选择一个部门');
  end;

end;

procedure TfrmCompanyWork.Excel1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.GDays,'考勤明细表');
end;

procedure TfrmCompanyWork.FormActivate(Sender: TObject);
begin
  inherited;
  Self.year.Text := IntToStr(YearOf( Date ));
  Self.cxComboBox1.ItemIndex := MonthOf(Date) -1;
  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;
  Self.cxDateEdit3.Date := Date;
  Self.cxDateEdit4.Date := Date;

  with Self.ADOCompany do
  begin
    Close;
    SQL.Text := 'Select * from ' + g_Table_Maintain_CompanyInfoTree + ' WHere PID=0';
    Open;
  end;
  Self.RzPageControl1.Height := 382;
  Self.RzPanel15.Height := 390;

//  Self.RzToolbar3.Top := Self.RzPageControl2.Top;

  Self.RzPageControl1.ActivePageIndex := 0;
end;

procedure TfrmCompanyWork.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmCompanyWork.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  inherited;
  if not dwADO.ADOExitIsSaveData(Handle,Self.Master,dwBlendSQL) then
  begin
    if MessageBox(Handle, PChar('增奖数据未保存成功，现在是否要退出?'),'提示',
         MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDNO then
    begin
      CanClose := False;
      Exit;
    end;
  end;
end;

procedure TfrmCompanyWork.But9Click(Sender: TObject);
begin
  inherited;
//保存
  IsDeleteEmptyData(Self.ADOOvertime ,
                    g_Table_Company_Staff_Over ,
                    Self.tvOvertimeCol3.DataBinding.FieldName);

end;

procedure TfrmCompanyWork.ClientDataSet1AfterClose(DataSet: TDataSet);
begin
  inherited;
  Self.RzToolButton9.Enabled  := False;
  Self.RzToolButton17.Enabled := False;
  Self.RzToolButton18.Enabled := False;
  Self.RzToolButton20.Enabled := False;
  Self.RzToolButton15.Enabled := False;
end;

procedure TfrmCompanyWork.ClientDataSet1AfterInsert(DataSet: TDataSet);
begin
  inherited;
  Self.tvGrid2Column3.EditValue := Self.tvWorkCol8.EditValue;
  with DataSet do
  begin
    FieldByName(Self.tvWorkCol8.DataBinding.FieldName).Value     := Self.tvWorkCol8.EditValue; //日期
    FieldByName(Self.tvWorkCol9.DataBinding.FieldName).Value     := Self.tvWorkCol9.EditValue;
    FieldByName(Self.tvGrid2Column1.DataBinding.FieldName).Value := Self.tvWorkId.EditValue;
    FieldByName(Self.tvParentIdA.DataBinding.FieldName).Value    := Self.tvParentId.EditValue;
    FieldByName(Self.tvGrid2Column2.DataBinding.FieldName).Value := Self.tvGrid1Column1.EditValue;
    FieldByName('TreeId').Value := dwCompayInfoIndex;
//  FieldByName(Self.tvGrid1Column1.DataBinding.FieldName).Value := ;
//  Self.tvParentIdA.EditValue := Self.tvParentId.EditValue;
  end;

end;

procedure TfrmCompanyWork.ClientDataSet1AfterOpen(DataSet: TDataSet);
begin
  inherited;
  Self.RzToolButton9.Enabled  := True;
  Self.RzToolButton17.Enabled := True;
  Self.RzToolButton18.Enabled := True;
  Self.RzToolButton20.Enabled := True;
  Self.RzToolButton15.Enabled := True;
end;

function TfrmCompanyWork.TheValue(lpValue : Double):Boolean;
var
  i , j : Integer;
  szColName : string;
  szColIndex: Integer;
  szMonth   : Integer;
  szYear : Integer;
  szDate : TDate;
  t : Integer;

begin
  inherited;
  if Self.tvDays.Controller.SelectedRowCount > 1 then
  begin
    ShowMessage('一次只允许更新一行');
  end else
  begin
    t:= GetTickCount;
    for I := 0 to Self.tvDays.Controller.SelectedColumnCount-1 do //选中列数
    begin
      szColIndex:= Self.tvDays.Columns[i].Index;
      if szColIndex <> Self.tvDaysCol5.Index then

      szColName := Self.tvDays.Controller.SelectedColumns[i].DataBinding.FilterFieldName;
      Application.ProcessMessages;
      if szColName <> '' then
      begin
        with Self.ADODays do
        begin
          if State <> dsInactive then
          begin
            if (State <> dsEdit) or (State <> dsInsert) then
              Edit;
            szMonth := StrToIntDef( FieldByName(Self.tvDaysCol4.DataBinding.FieldName).AsString , 0 ) ;
            szYear  := StrToIntDef( FieldByName(Self.tvDaysCol3.DataBinding.FieldName).AsString , 0 );
            case DaysInAMonth(szYear , szMonth) of
              28:
              begin
                if (szColName = Self.tvDaysColumn30.DataBinding.FieldName) or   //29
                   (szColName = Self.tvDaysColumn31.DataBinding.FieldName) or   //30
                   (szColName = Self.tvDaysColumn32.DataBinding.FieldName)then
                begin
                  FieldByName(szColName).Value := 0 ;
                //  Post;
                  continue;
                end;
              end;
              29:
              begin
                if (szColName = Self.tvDaysColumn31.DataBinding.FieldName) or   //30
                   (szColName = Self.tvDaysColumn32.DataBinding.FieldName)then
                begin
                  FieldByName(szColName).Value := 0 ;
                //  Post;
                  continue;
                end;
              end;
              30:
              begin
                if szColName = Self.tvDaysColumn32.DataBinding.FieldName then
                begin
                  FieldByName(szColName).Value := 0 ;
                //  Post;
                  continue;
                end;
              end;
            end;
            FieldByName(szColName).Value := lpValue ;
          //  Post;
          end;
        end;
      end;
    end;
  //  Self.ADODays.Post;
  //  ShowMessage('Timer:' + IntToStr(GetTickCount - t));
    GetSumMoney();
  end;
end;

procedure TfrmCompanyWork.RzPanel16Resize(Sender: TObject);
var
  szWidth : Integer;
begin
  inherited;
  szWidth := Round( Self.RzPanel16.Width / 2 );
//  Self.RzPanel9.Width := szWidth;
//  Self.RzPanel14.Width:= szWidth;
end;

procedure TfrmCompanyWork.RzToolButton10Click(Sender: TObject);
begin
  inherited;
//  Self.tvGrid1.DataController.UpdateData;
  dwADO.ADOSaveData(Self.Master,dwBlendSQL);
end;

procedure TfrmCompanyWork.RzToolButton12Click(Sender: TObject);
begin
  TheValue(1);
end;

function TfrmCompanyWork.GetSumMoney():Boolean;
var
  I : Integer;

  szRowIndex : Integer;
  szColIndex : Integer;

  s : string;
  szColName : string;
  Days: Double;
  szUnitPrice : Currency;
  t : Double;
  szYearWages : Currency;
  szMonthWages: Currency;
  szDaysWages : Currency;
  szWages : Currency;
  szDays1 : Double;
  szDays2 : Double;
  szDays3 : Double;
  szDays4 : Double;
  szDays5 : Double;
  szDays6 : Double;
  szDays7 : Double;
  szDays8 : Double;
  szDays9 : Double;
  szDays10 : Double;
  szDays11 : Double;
  szDays12 : Double;
  szDays13 : Double;
  szDays14 : Double;
  szDays15 : Double;
  szDays16 : Double;
  szDays17 : Double;
  szDays18 : Double;
  szDays19 : Double;
  szDays20 : Double;
  szDays21 : Double;
  szDays22 : Double;
  szDays23 : Double;
  szDays24 : Double;
  szDays25 : Double;
  szDays26 : Double;
  szDays27 : Double;
  szDays28 : Double;
  szDays29 : Double;
  szDays30 : Double;
  szDays31 : Double;

begin
  inherited;

  szRowIndex := Self.tvDays.Controller.FocusedRowIndex;
  if szRowIndex >= 0 then
  begin
    s := VarToStr( Self.tvDays.DataController.Values[szRowIndex,Self.tvDaysCol6.Index] );

    szUnitPrice := StrToCurrDef(s,0); //单价为零不能计算

    for I := 0 to Self.tvDays.ColumnCount-1 do
    begin
      szColName := Self.tvDays.Columns[i].Caption;
      szColIndex:= Self.tvDays.Columns[i].Index;

      if (szColName = '1') or
         (szColName = '2') or
         (szColName = '3') or
         (szColName = '4') or
         (szColName = '5') or
         (szColName = '6') or
         (szColName = '7') or
         (szColName = '8') or
         (szColName = '9') or
         (szColName = '10') or
         (szColName = '11') or
         (szColName = '12') or
         (szColName = '13') or
         (szColName = '14') or
         (szColName = '15') or
         (szColName = '16') or
         (szColName = '17') or
         (szColName = '18') or
         (szColName = '19') or
         (szColName = '20') or
         (szColName = '21') or
         (szColName = '22') or
         (szColName = '23') or
         (szColName = '24') or
         (szColName = '25') or
         (szColName = '26') or
         (szColName = '27') or
         (szColName = '28') or
         (szColName = '29') or
         (szColName = '30') or
         (szColName = '31')  then
      begin
        s := VarToStr( Self.tvDays.Controller.FocusedRow.Values[ szColIndex ] );
        {
        if VarToStr(s) = '√' then
        begin
          t := 1;
        end else
        if VarToStr(s) = '○' then
        begin
          t := 0.5;
        end else
        if VarToStr(s) = '×' then
        begin
          t := 0;
        end;
        }
        Days := Days + StrToFloatDef(s,0);
      end;

    end;

    with Self.ADODays do
    begin
      if State <> dsInactive then
      begin
        if State <> dsEdit then  Edit;

        FieldByName(Self.tvDaysCol5.DataBinding.FieldName).Value := Days;  //更新出勤
        if dwLeastWork =  0 then
        begin
          FieldByName(Self.tvDaysCol7.DataBinding.FieldName).Value := 0;
          Application.MessageBox( '每月最少天数为零无法正常计算工资!', '提示:', MB_OK + MB_ICONWARNING)
        end else
        begin
          if dwwagestype = '年工资' then
          begin
            szMonthWages:= szUnitPrice / 12;

            if Days >= dwLeastWork then
              szWages := szMonthWages
            else
              szWages :=  (szMonthWages / dwLeastWork) * Days;

            FieldByName(Self.tvDaysCol7.DataBinding.FieldName).Value := szWages;
          end else
          if dwwagestype = '月工资' then
          begin
            szDaysWages:= szUnitPrice / dwLeastWork;
            if Days >= dwLeastWork then
              szWages := szUnitPrice
            else
              szWages := szDaysWages * Days;

            FieldByName(Self.tvDaysCol7.DataBinding.FieldName).Value := szWages;
          end else
          if dwwagestype = '日工资' then
          begin
            FieldByName(Self.tvDaysCol7.DataBinding.FieldName).Value := szUnitPrice * Days;
          end;

        end;

      end;

    end;

  end;

end;

procedure TfrmCompanyWork.MasterAfterInsert(DataSet: TDataSet);
var
  szCode : string;
  Suffix : string;
  Prefix : string;
  szConName : string;

begin
  inherited;
  with DataSet do
  begin
    FieldByName(Self.tvWorkCol8.DataBinding.FieldName).Value     := Date;
    FieldByName(Self.tvWorkCol2.DataBinding.FieldName).Value     := dwCompayInfoIndex;
    FieldByName(Self.tvGrid1Column2.DataBinding.FieldName).Value := 1;
    FieldByName(Self.tvWorkCol11.DataBinding.FieldName).Value    := 1;

    szConName := Self.tvGrid1Column1.DataBinding.FieldName;
    szCode := Prefix + GetRowCode(g_Table_Company_BlendStaff,szConName,Suffix,170000);
    FieldByName( szConName ).Value := szCode;

  end;
end;

procedure TfrmCompanyWork.MasterAfterOpen(DataSet: TDataSet);
begin
  inherited;
  Self.RzToolButton7.Enabled  := True;
  Self.RzToolButton8.Enabled  := True;
  Self.RzToolButton11.Enabled := True;
  Self.RzToolButton10.Enabled := True;
  Self.RzToolButton14.Enabled := True;
  Self.Btn6.Enabled := True;
  Self.ClientDataSet1.Close;
  Self.cxDateEdit1.Enabled := True;
  Self.cxDateEdit2.Enabled := True;
end;

procedure TfrmCompanyWork.MenuItem10Click(Sender: TObject);
begin
  inherited;
  Self.But10.Click;
end;

procedure TfrmCompanyWork.MenuItem12Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton9.Click;
end;

procedure TfrmCompanyWork.MenuItem13Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton20.Click;
end;

procedure TfrmCompanyWork.MenuItem14Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton18.Click;
end;

procedure TfrmCompanyWork.MenuItem15Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton17.Click;
end;

procedure TfrmCompanyWork.MenuItem16Click(Sender: TObject);
var
  s : string;
  szSQL1 : string;
  szSQL2 : string;

begin
  inherited;
  case Self.RzPageControl1.ActivePageIndex of
    0:
    begin
      //签到表
    end;
    1:
    begin
      //考勤
      if sender = Self.MenuItem16 then
      begin
        s := 'no';
      end else
      if Sender = Self.MenuItem17 then
      begin
        s := 'yes';
      end;

      with Self.ADODays  do
      begin
        Close;
        SQL.Text := 'Select * from ' + g_Table_Company_Staff_CheckWork +
                      ' Where ' + 'code'  + '="' + dwNumbers + '" AND ' + Self.tvDaysCol2.DataBinding.FieldName +'=' + s ; //员工考勤

        Open;
        Sort :=  'Year ASC, Month ASC';
      end;
    end;
    2:
    begin
      //加班
      if sender = Self.MenuItem16 then
      begin
        s := 'no';
      end else
      if Sender = Self.MenuItem17 then
      begin
        s := 'yes';
      end;

      with Self.ADOOvertime do
      begin
        Close;
        SQL.Text := 'Select * from ' + g_Table_Company_Staff_Over +
                    ' Where ' +  'code' + '="' + dwNumbers + '" AND ' + Self.tvOvertimeCol2.DataBinding.FieldName + '=' + s;
        Open;
        Sort :=  'Year ASC';
      end;
    end;
  end;
end;

procedure TfrmCompanyWork.MenuItem18Click(Sender: TObject);
begin
  inherited;
  SetGridStuat(Self.tvOvertime,Self.tvOvertimeColumn2.DataBinding.FieldName,False);
end;

procedure TfrmCompanyWork.MenuItem19Click(Sender: TObject);
begin
  inherited;
  SetGridStuat(Self.tvOvertime,Self.tvOvertimeColumn2.DataBinding.FieldName,True);
end;

procedure TfrmCompanyWork.MenuItem1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.cxGrid1,'签到明细表');
end;

procedure TfrmCompanyWork.MenuItem2Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.cxGrid1;
  DM.BasePrinterLink1.ReportTitleText := '公司名称：' + Self.tvCompanyColumn2.EditValue;
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmCompanyWork.MenuItem3Click(Sender: TObject);
begin
  inherited;
   CxGridToExcel(Self.cxGrid2,'明细表');
end;

procedure TfrmCompanyWork.MenuItem4Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.cxGrid2;
  DM.BasePrinterLink1.ReportTitleText := '公司名称：' + Self.tvCompanyColumn2.EditValue;
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmCompanyWork.MenuItem5Click(Sender: TObject);
begin
  inherited;
//加班Execl
  CxGridToExcel(Self.WorkGrid,'加班明细表');
end;

procedure TfrmCompanyWork.MenuItem6Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.WorkGrid;
  DM.BasePrinterLink1.ReportTitleText := '公司名称：' + Self.tvCompanyColumn2.EditValue;
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmCompanyWork.MenuItem7Click(Sender: TObject);
begin
  inherited;
  Self.But8.Click;
end;

procedure TfrmCompanyWork.MenuItem8Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton1.Click;
end;

procedure TfrmCompanyWork.MenuItem9Click(Sender: TObject);
begin
  inherited;
  Self.But9.Click;
end;

procedure TfrmCompanyWork.N01Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton16.Click;
end;

procedure TfrmCompanyWork.N051Click(Sender: TObject);
begin
  inherited;
  self.RzToolButton3.click;
end;

procedure TfrmCompanyWork.N10Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton7.Click;
end;

procedure TfrmCompanyWork.N11Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton12.Click;
end;

procedure TfrmCompanyWork.N12Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton14.Click;
end;

procedure TfrmCompanyWork.N13Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton10.Click;
end;

procedure TfrmCompanyWork.N14Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton8.Click;
end;

procedure TfrmCompanyWork.N1Click(Sender: TObject);
begin
  inherited;
  Self.but1.Click;
end;

procedure TfrmCompanyWork.N2Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.GDays;
  DM.BasePrinterLink1.ReportTitleText := '姓名：' + dwFullname;
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmCompanyWork.N3Click(Sender: TObject);
begin
  inherited;
  Self.but2.Click;
end;

procedure TfrmCompanyWork.N4Click(Sender: TObject);
begin
  inherited;
  Self.But3.Click;
end;

procedure TfrmCompanyWork.N5Click(Sender: TObject);
begin
  inherited;
  Self.But4.Click;
end;

procedure TfrmCompanyWork.N8Click(Sender: TObject);
begin
  inherited;
  //冻结
  SetGridStuat(Self.tvDays,Self.tvDaysCol2.DataBinding.FieldName,False);
end;

procedure TfrmCompanyWork.N9Click(Sender: TObject);
begin
  inherited;
  //解冻
  SetGridStuat(Self.tvDays,Self.tvDaysCol2.DataBinding.FieldName,True);
end;

procedure TfrmCompanyWork.RzToolButton16Click(Sender: TObject);
begin
  inherited;
  TheValue(0);
end;

procedure TfrmCompanyWork.RzToolButton17Click(Sender: TObject);
begin
  inherited;
//删除
  dwADO.ADODeleteSelectionData(Self.tvGrid2,Self.ClientDataSet1,dwBlendProjectRecordSQL);
end;

procedure TfrmCompanyWork.RzToolButton18Click(Sender: TObject);
begin
  inherited;
//  Self.tvGrid2.DataController.UpdateData;
  dwADO.ADOSaveData(Self.ClientDataSet1,dwBlendProjectRecordSQL);
end;

procedure TfrmCompanyWork.RzToolButton19Click(Sender: TObject);
var
  s : string;
begin
  inherited;
  with Self.ADOOvertime do
  begin
    Close;
    SQL.Text := 'Select * from ' + g_Table_Company_Staff_Over + ' Where ' +  'code' + '="' + dwNumbers + '"';;
    Open;

    SQL.Text := 'Select * from ' + g_Table_Company_Staff_CheckWork +
                ' Where ' + Self.tvDaysCol3.DataBinding.FieldName + '=' + Self.year.Text + ' AND ' +
                  Self.tvDaysCol4.DataBinding.FieldName + '=' + Self.cxComboBox1.Text + ' AND parent=' + dwParent + '';
    Open;
  end;
end;

procedure TfrmCompanyWork.RzToolButton1Click(Sender: TObject);
begin
  inherited;
  //编辑
  if Self.tvOvertimeColumn2.EditValue = False then
  begin
    if Self.tvOvertime.Controller.FocusedRowIndex >= 0 then
    begin
      Application.MessageBox( '数据冻结状态下不可编辑！', '提示:', MB_OK + MB_ICONWARNING)
    end;
  end else
  begin
    with Self.ADOOvertime do
    begin
      if (State <> dsEdit) and (State <> dsInsert) then
      begin
        Edit;
      end;
    end;
  end;
end;

procedure TfrmCompanyWork.RzToolButton20Click(Sender: TObject);
begin
  inherited;
  //编辑
  if Self.tvGrid2.Controller.FocusedRowIndex >= 0 then dwADO.ADOIsEdit(Self.ClientDataSet1);
end;

procedure TfrmCompanyWork.RzToolButton3Click(Sender: TObject);
begin
  inherited;
  TheValue(0.5);
end;

procedure TfrmCompanyWork.RzToolButton5Click(Sender: TObject);
var
  s : string;
begin
  inherited;
  s := Search.Text;
  GridLocateRecord(Self.tvpeopleView,Self.tvpeopleViewCol2.DataBinding.FieldName,s);
  Self.tvpeopleViewDblClick(Sender);
end;

procedure TfrmCompanyWork.RzToolButton6Click(Sender: TObject);
begin
  inherited;
  Self.Master.Close;
  Self.ClientDataSet1.Close;
  Self.ADOQuery1.Requery();
  Self.qry.Requery();
  Self.ADOCompany.Requery();
  GetMaintainInfo;
  ShowMessage('信息刷新完成!');
end;

procedure TfrmCompanyWork.RzToolButton7Click(Sender: TObject);
begin
  inherited;
  with Self.Master do
  begin
    if State = dsInactive then
    begin
      Application.MessageBox( '当前无法状态无法添加，需要选择一个公司后在进行？', '提示:', MB_OKCANCEL + MB_ICONWARNING)
    end else
    begin
      dwADO.ADOAppend(Self.Master);
    end;
  end;
end;


procedure TfrmCompanyWork.RzToolButton8Click(Sender: TObject);
var
  szRowIndex : Integer;
  I: Integer;
  szCount : Integer;
  szRowsCount : string;
  s , Id , str , err : string;
  szValue : Variant;

begin
  inherited;
  szCount := Self.tvGrid1.Controller.SelectedRowCount;
  szRowsCount := IntToStr(szCount) ;

  if szCount >= 0 then
  begin
    if  Application.MessageBox( '您要删除所有选中的记录吗？', '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
    begin

      with Self.tvGrid1 do
      begin
        if DataController.IsEditing then DataController.UpdateData;

        for I := szCount -1 downto 0 do
        begin
          Id := VarToStr(  Controller.SelectedRows[i].Values[ Self.tvParentId.Index ] );
          if Length(id) <> 0 then
          begin
            if Length(str) = 0 then
              str := Id
            else
              str := str + '","' + Id;

          end;

        end;

      end;

      s := 'delete * from ' +  g_Table_Company_BlendProjectRecord +
           ' where '+ Self.tvParentIdA.DataBinding.FieldName +
           ' in("' + str + '")';

      if str <> '' then
      begin
        with DM.Qry do
        begin
          Close;
          SQL.Text := s;
          ExecSQL;
        end;
      end;
      Self.tvGrid1.Controller.DeleteSelection;  //删除选中的文本
      Self.tvGrid1.DataController.UpdateData;
      dwADO.DeleteData(Self.Master,dwBlendSQL);

      {
      DM.ADOconn.BeginTrans; //开始事务
      try
        DM.ADOconn.Committrans; //提交事务
      except
        on E:Exception do
        begin
          DM.ADOconn.RollbackTrans;           // 事务回滚
          err:=E.Message;
          ShowMessage(err);
        end;
      end;
      }
    end;

  end;

end;

procedure TfrmCompanyWork.RzToolButton9Click(Sender: TObject);
begin
  inherited;
  with Self.ClientDataSet1 do
  begin
    if State = dsInactive then
    begin
      Application.MessageBox( '当前无法状态无法添加，需要选择一个公司后在进行？', '提示:', MB_OKCANCEL + MB_ICONWARNING)
    end else
    begin
      dwADO.ADOAppend(Self.ClientDataSet1);
    end;
  end;
end;

procedure TfrmCompanyWork.SearchPropertiesCloseUp(Sender: TObject);
begin
  inherited;
  RzToolButton5.Click;
end;

procedure TfrmCompanyWork.SectionPropertiesCloseUp(Sender: TObject);
var
  s : string;
  szColName : string;

begin
  inherited;
  s := Self.Section.Text ;
  szColName := Self.tvpeopleViewColumn6.DataBinding.FieldName;
  S := 'Select * from '+ g_Table_Maintain_CompanyInfoList + ' WHERE TreeId=' +
       IntToStr( dwCompayInfoIndex ) + ' AND ' + szColName + '="' + s + '"';
  with Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;
  end;
end;

procedure TfrmCompanyWork.but2Click(Sender: TObject);
begin
  inherited;
  if Self.tvDays.Controller.FocusedRowIndex >= 0 then
  begin
    with Self.ADODays do
    begin
      if State <> dsInactive then
      begin
        if (State <> dsEdit) or (State <> dsinsert) then
        begin
          Edit;
          Self.tvDays.Controller.FocusedColumn.Editing := True;
        end;
      end;
    end;
  end;
end;

procedure TfrmCompanyWork.but1Click(Sender: TObject);
var
  szRowCount : Integer;
  szExistRowCount : Integer;
  szYear : string;
  szMonth: string;
  lvList :Variant;
  I: Integer;
  DD1 : string;
  DD2 : string;
begin
  inherited;
  szYear := Self.year.Text;
  szMonth:= cxComboBox1.Text;
  if StrToIntDef(szMonth,1) <= 12  then
  begin
    {
    for I := 0 to Self.tvDays.ViewData.RowCount-1 do
    begin

    end;
    }
    with DM.Qry do
    begin
      Close;
      SQL.Text := 'Select Year,Month from ' + g_Table_Company_Staff_CheckWork + 
                ' Where ' + 'code'  + '="' + dwNumbers + '" and Year='+ szYear +' and  Month=' + szMonth;
      Open;
      if RecordCount <> 0 then
      begin
        ShowMessage(szYear + '年' + szMonth + '月' + '已经存在不可重复添加!');
        Exit;
      end;    
    end;

    with Self.ADODays do
    begin
      if State <> dsInactive then
      begin
        Append;
        FieldByName(Self.tvDaysCol4.DataBinding.FieldName).Value := szMonth;  //月份
        FieldByName(Self.tvDaysCol3.DataBinding.FieldName).Value := szYear;  //年份
        dwIsSelectStaff := True;
      end;

    end;
    Self.GDays.SetFocus;
    Self.tvDaysCol4.FocusWithSelection;
  
  end;

  
{
var v:Variant;
begin

//根据ID，姓名，搜索，返回姓名，年龄
   v:=ADOTable1.Lookup('id;age',VarArrayOf(['108',21]),'name;age');

if VarType(v) <> varNull then
   begin
     ShowMessage(v[0]+inttostr(v[1]));
   end;
end;

循环

with ADOTable1 do
begin
DisableControls;
First;
while not eof do
begin
    ShowMessage(Fields[1].Value);
    Next;
end;
EnableControls;
end;
}

//  ShowMessage(szYear);
  {
  with Self.ADODays do
  begin
    if State <> dsInactive then
    begin
      Append;
      dwIsSelectStaff := True;
    end;

  end;
  Self.GDays.SetFocus;
  Self.tvDaysCol4.FocusWithSelection;
  }
//  Self.tvDaysCol2.FocusWithSelection;
  {
  szRowCount := Self.tvDays.DataController.RecordCount;
  if szRowCount < 12 then
  begin

    with DM.Qry do
    begin
      Close;
      SQL.Text := 'Select * from ' + g_Table_Company_Staff_CheckWork +
      ' Where ' + 'Parent=' + dwParent + ' AND ' + Self.tvDaysCol3.DataBinding.FieldName +
      '="' + Self.year.Text +'" AND ' + Self.tvDaysCol4.DataBinding.FieldName + '="' +
      Self.cxComboBox1.Text + '"'; //员工表
      Open;
      szExistRowCount := RecordCount;
    end;

    if szExistRowCount = 0 then
    begin

    end else
    begin
      ShowMessage('相同年份月份只允许增加一条');
    end;

  end else
  begin
    ShowMessage('考勤月份最多可以添加12条,已到最大数');
  end;
  }
end;

procedure TfrmCompanyWork.But7Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmCompanyWork.But4Click(Sender: TObject);
begin
  if Application.MessageBox(PWideChar('确定要删除所选中的记录吗？' ), '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
  begin
    Self.tvDays.Controller.DeleteSelection;
  end;
  
end;


procedure TfrmCompanyWork.But8Click(Sender: TObject);
begin
  inherited;
//添加
  with Self.ADOOvertime do
  begin
    if State <> dsInactive then
    begin
      Append;
      dwIsSelectStaff := True;
    end;
  end;
  Self.WorkGrid.SetFocus;
  Self.tvOvertimeCol3.FocusWithSelection;
end;

procedure TfrmCompanyWork.But3Click(Sender: TObject);
begin
  inherited;
  IsDeleteEmptyData(Self.ADODays ,
                    g_Table_Company_Staff_CheckWork ,
                    Self.tvDaysCol3.DataBinding.FieldName);

end;

procedure TfrmCompanyWork.Btn6Click(Sender: TObject);
var
  sqltext : string;
begin
  inherited;
  //范围查询
  if dwFullname <> '' then
  begin
    sqltext := 'select * from ' + g_Table_Company_BlendStaff +
             ' where '+ Self.tvWorkCol9.DataBinding.FieldName +
             '= "' + dwFullname +
             '" AND ' + Self.tvWorkCol2.DataBinding.FieldName +
             '=' + IntToStr(dwCompayInfoIndex)+
             ' and ' + Self.tvWorkCol8.DataBinding.FieldName + ' between :t1 and :t2';
    dwADO.ADOSearchDateRange(Self.cxDateEdit1.Text,Self.cxDateEdit2.Text,sqltext,Self.Master);
  end else
  begin
    Application.MessageBox(PWideChar('日期范围查询需要绑定员工姓名和公司名称，请在左侧选择一个公司名称和员工姓名后在选择起始日期和结束日期查询' ), '查询记录？', MB_OKCANCEL + MB_ICONWARNING)
  end;

end;

procedure TfrmCompanyWork.But10Click(Sender: TObject);
begin
  inherited;
//删除
  if Application.MessageBox(PWideChar('确定要删除所选中的记录吗？' ), '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
  begin
    Self.tvOvertime.Controller.DeleteSelection;
  end;

end;

procedure TfrmCompanyWork.tvCompanyDblClick(Sender: TObject);
var
  szRowIndex : Integer;
  s : string;

begin
  inherited;
  if (Self.qry.Active) and (dwIsSelectCompany) then
  begin
     if MessageBox(handle, PChar('是否切换公司?'),'提示',
               MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDNO then
    begin
      Exit;
    end;
  end;

  szRowIndex := Self.tvCompany.Controller.FocusedRowIndex;
  if szRowIndex >= 0 then
  begin
    s := VarToStr(Self.tvCompany.DataController.Values[szRowIndex,Self.tvCompanyColumn2.Index]);
    GetCompayInfoList(s);
    dwIsSelectCompany := True;

  end;

  dwBlendSQL := 'Select * from ' + g_Table_Company_BlendStaff +
                ' Where ' + Self.tvWorkCol2.DataBinding.FieldName + '=' + IntToStr( dwCompayInfoIndex );

  with DM.ADOQuery1 do
  begin
    Close;
    SQL.Text := dwBlendSQL;
    Open;
    Self.Master.Data := DM.DataSetProvider1.Data;
    Self.Master.Open;
  end;

end;

procedure TfrmCompanyWork.tvDaysCol2PropertiesChange(Sender: TObject);
begin
  inherited;
  Self.ADODays.UpdateBatch(arAll);
end;

procedure TfrmCompanyWork.tvDaysCol5StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var

  Value : Variant;
  s : string;

begin
  inherited;
  Value := ARecord.Values[Self.tvDaysCol5.Index];
  if Value <> null then
  begin
    s := VarToStr(Value);
    if StrToFloatDef(s,0) <> 0 then
    begin
      AStyle := DM.cxStyle11;
    end;
  end;

end;

procedure TfrmCompanyWork.tvDaysColumn2PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  if TcxSpinEdit(Sender).InplaceParams.MultiRowParent then
  begin
    with TcxGridColumn(TcxSpinEdit(Sender).InplaceParams.Position.Item) do
    begin
      EditValue := DisplayValue;
    end;
  end;
  GetSumMoney();
  {
  if TcxSpinEdit(Sender).InplaceParams.MultiRowParent then
  begin
    with TcxGridColumn(TcxSpinEdit(Sender).InplaceParams.Position.Item) do
    begin
      
    //  showmessage(Name + '  ' + DisplayValue);
    end;
  end;  
  }  
end;

procedure TfrmCompanyWork.tvDaysColumn2StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  s : string;

begin
  inherited;
  s := VarToStr( ARecord.Values[Self.tvDaysColumn2.Index] );
  if StrToFloatDef(s,0) > 1 then
  begin
    AStyle := DM.cxStyle377;
  end;

end;

function GetCurrDay(CurrDate : TDateTime):Integer;
var
  Year,Month,Day:word;
  TempDate : TDateTime;
begin
  DecodeDate(CurrDate,Year,Month,Day);
  if Month = 12 then Result := 30
  else 
  begin
    TempDate := EncodeDate(Year,Month+1,1)-1;
    DecodeDate(TempDate,Year,Month,Day);
    Result := Day;
  end;
end;

procedure TfrmCompanyWork.tvDaysColumn37StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  Value : Variant;
  s : string;

begin
  inherited;
  Value := ARecord.Values[Self.tvDaysCol5.Index];
  if Value <> null then
  begin
    s := VarToStr(Value);
    if StrToFloatDef(s,0) <> 0 then
    begin
      AStyle := DM.SumMoney;
    end;  
  end;  
  
end;

procedure TfrmCompanyWork.tvDaysDblClick(Sender: TObject);
begin
  inherited;
  if Self.tvDays.Controller.FocusedRowIndex >= 0 then
  begin
    if Self.tvDays.Controller.FocusedRow.Values[ Self.tvDaysCol2.Index] then
    begin
      Self.but2.Click;
    end;
  end;

end;

procedure TfrmCompanyWork.tvDaysEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
var
  szMonth : string;
  szRowIndex : Integer;
  szValue : Variant;

begin
  inherited;
  szValue := Self.tvDays.Controller.FocusedRow.Values[Self.tvDaysCol2.Index];
  if Self.tvDaysCol2.Index <> AItem.Index then
  begin
    if (szValue = False) then
    begin
      AAllow := False
    end else
    begin

      with Self.ADODays do
      begin
        if State <> dsInactive then
        begin
          if (State <> dsEdit) and (State <> dsInsert) then
          begin
            AAllow := False;
          end else
          begin
            AAllow := True;

            szMonth := FieldByName(Self.tvDaysCol4.DataBinding.FieldName).AsString;
            if szMonth = '2' then
            begin
              if (AItem.Index = Self.tvDaysColumn31.Index) or (AItem.Index = Self.tvDaysColumn32.Index) then
              begin
                AAllow := False;
              end;
            end else
            if (szMonth = '4') or (szMonth = '6') or (szMonth = '9') or (szMonth = '11') then
            begin
              if (AItem.Index = Self.tvDaysColumn32.Index) then
              begin
                AAllow := False;
              end;
            end;

          end;

        end;

      end;

    end;

  end;

  if (AItem.Index = Self.tvDaysCol1.Index)  or
     (AItem.Index = Self.tvDaysCol5.Index) then  //出勤数
  begin
    AAllow := False;
  end;

end;

procedure TfrmCompanyWork.tvDaysKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 46 then
  begin
    Self.But4.Click;
  end;
end;

procedure TfrmCompanyWork.tvDaysStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;
  Value := ARecord.Values[Self.tvDaysCol2.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;
end;

procedure TfrmCompanyWork.tvDaysTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
  inherited;
  if AValue <> null then
  begin
    Self.RzPanel8.Caption := VarToStr(AValue);
  end else
  begin
    Self.RzPanel8.Caption := '';
  end;
end;

procedure TfrmCompanyWork.tvDaysTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems1GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  s : string;
  szSumMoney : Currency;

begin
  inherited;
  if AValue <> null then
  begin
    s := VarToStr(AValue);
    dwDaysSumMoney := StrToCurrDef(s,0);
    szSumMoney := RoundTo( dwDaysSumMoney + dwOverSumMoney , -2 ) ;

    Self.RzPanel11.Caption := CurrToStr(szSumMoney) ;
    Self.RzPanel10.Caption := MoneyConvert( szSumMoney );

    ViewHint();

  end else
  begin
    Self.RzPanel11.Caption := '';
    Self.RzPanel10.Caption := '';
  end;
end;

procedure TfrmCompanyWork.tvGrid1DblClick(Sender: TObject);
var
  szValue : Variant;

begin
  inherited;
  //获取工程记录
  if Self.tvGrid1.Controller.FocusedRowIndex >= 0 then
  begin
    szValue := Self.tvGrid1Column1.EditValue;
    if szValue <> null then
    begin
      //  sk_Company_BlendProjectRecord
      dwBlendProjectRecordSQL := 'Select * from ' + g_Table_Company_BlendProjectRecord +
                                 ' Where ' + Self.tvGrid2Column2.DataBinding.FieldName +
                                 '="' + VarToStr( szValue ) +
                                 '" AND ' + Self.tvWorkCol8.DataBinding.FieldName +
                                 '=#' + VarToStr( Self.tvWorkCol8.EditValue )+ '#';
      with DM do
      begin
        ADOQuery1.Close;
        ADOQuery1.SQL.Clear;
        ADOQuery1.SQL.Text := dwBlendProjectRecordSQL;
        ADOQuery1.Open;
        Self.ClientDataSet1.Data := DataSetProvider1.Data;
        Self.ClientDataSet1.Open;
      end;

    end;
  end;

end;

procedure TfrmCompanyWork.tvGrid1Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  dwADO.ADOIsEditing(Self.Master,AAllow);

  if AItem.Index = Self.tvWorkCol1.Index then
  begin
    AAllow := False;
  end;

end;

procedure TfrmCompanyWork.tvGrid2DblClick(Sender: TObject);
begin
  inherited;
  Self.RzToolButton20.Click;
end;

procedure TfrmCompanyWork.tvGrid2Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  dwADO.ADOIsEditing(Self.ClientDataSet1,AAllow);
end;

function GetOvertimeSumMoney(lpCount : Double; lpUnitPrice : Currency ; lpADO : TADOQuery ; lpFieldName : string ):Boolean;
begin
  with lpADO do
  begin
    if State <> dsInactive then
    begin
      FieldByName(lpFieldName).Value := lpCount * lpUnitPrice;
    end;
  end;
end;

procedure TfrmCompanyWork.tvOvertimeCol3PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;
  szColName : string;
  szUnitPrice : Currency;

begin
  inherited;
  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue); //加班数量

    szColName := Self.tvOvertimeCol4.DataBinding.FieldName;
    with Self.ADOOvertime do
    begin
      if State <> dsInactive then
      begin
        szUnitPrice := FieldByName(szColName).AsCurrency;
        FieldByName(Self.tvOvertimeCol3.DataBinding.FieldName).Value := DisplayValue;
      end;
    end;

    GetOvertimeSumMoney(StrToFloatDef(s,0),szUnitPrice,Self.ADOOvertime,Self.tvOvertimeCol5.DataBinding.FieldName);

  end;

end;

procedure TfrmCompanyWork.tvOvertimeCol4PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;
  szColName : string;
  szUnitPrice : Currency;

begin
  inherited;
  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue);  //单价
    szColName := Self.tvOvertimeCol3.DataBinding.FieldName;
    with Self.ADOOvertime do
    begin
      if State <> dsInactive then
      begin
        szUnitPrice := FieldByName(szColName).AsCurrency;
        FieldByName(Self.tvOvertimeCol4.DataBinding.FieldName).Value := DisplayValue;
      end;
    end;

    GetOvertimeSumMoney(szUnitPrice,StrToFloatDef(s,0),Self.ADOOvertime,Self.tvOvertimeCol5.DataBinding.FieldName);

  end;
end;

procedure TfrmCompanyWork.tvOvertimeColumn2PropertiesChange(Sender: TObject);
begin
  inherited;
  Self.ADOOvertime.UpdateBatch(arAll);
end;

procedure TfrmCompanyWork.tvOvertimeDblClick(Sender: TObject);
begin
  inherited;
  Self.RzToolButton1.Click;
end;

procedure TfrmCompanyWork.tvOvertimeEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  if AItem.Index <> Self.tvOvertimeColumn2.Index then
  begin
    with Self.ADOOvertime do
    begin
      if State <> dsInactive then
      begin

        if (State <> dsEdit) and (State <> dsInsert) then
        begin
          AAllow := False;
        end else
        begin
          AAllow := True;
        end;

      end;
    end;
  end;

  if (AItem.Index = Self.tvOvertimeCol1.Index) then AAllow := False;
end;

procedure TfrmCompanyWork.tvOvertimeEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = 13) and (AItem.Index = tvOvertimeCol5.Index) then
  begin
  //  Self.tvOvertimeCol3.Editing := True;
     Self.But8.Click;
     Self.tvOvertimeColumn2.Editing := True;
  end;
end;

procedure TfrmCompanyWork.tvOvertimeStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;
  Value := ARecord.Values[Self.tvOvertimeColumn2.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;
end;

procedure TfrmCompanyWork.tvOvertimeTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
  inherited;
  if AValue <> null then
    Self.RzPanel13.Caption := VarToStr(AValue)
  else
    Self.RzPanel13.Caption := '';
end;

procedure TfrmCompanyWork.tvOvertimeTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems1GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  s : string;
  szSumMoney : Currency;
begin
  inherited;
  if AValue <> null then
  begin
    s := VarToStr(AValue);
    dwOverSumMoney := StrToCurrDef(s,0);
    szSumMoney := RoundTo( dwDaysSumMoney + dwOverSumMoney , -2 );
    //RoundTo( dwDaysSumMoney + dwOverSumMoney , -2 ) ;
    Self.RzPanel11.Caption := CurrToStr(szSumMoney) ;
    Self.RzPanel10.Caption := MoneyConvert( szSumMoney );
    ViewHint();
  end else
  begin
    Self.RzPanel11.Caption := '' ;
    Self.RzPanel10.Caption := '';
  end;
end;

procedure TfrmCompanyWork.tvpeopleViewCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmCompanyWork.tvpeopleViewDblClick(Sender: TObject);
var
  szRowIndex : Integer;
  szColName : string;
  szPrice : Currency;

begin
  inherited;
  szRowIndex := Self.tvpeopleView.Controller.FocusedRowIndex;
//  http://ymg97526.blog.163.com/blog/static/1736581602011332353527/
  if szRowIndex >= 0 then
  begin
    if (Self.ADODays.Active) and (dwIsSelectStaff) then
    begin
      if MessageBox(handle, PChar('是否切换员工?'),'提示',
           MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDNO then
      begin
        Exit;
      end;
    end;

    with Self.qry do
    begin
      if State <> dsInactive then
      begin
        dwParent   := FieldByName('ID').AsString;
        dwFullname := FieldByName(Self.tvpeopleViewCol2.DataBinding.FieldName).AsString;
        Self.cxLabel9.Caption := dwFullname;
        Self.cxLabel10.Caption:= FieldByName(Self.tvpeopleViewColumn5.DataBinding.FieldName).AsString;//性别
        dwUnitPrice:= FieldByName(Self.tvpeopleViewColumn1.DataBinding.FieldName).AsCurrency; //单价
        dwOverPrice:= FieldByName(Self.tvpeopleViewColumn2.DataBinding.FieldName).AsCurrency; //加班单价
        dwwagestype:= FieldByName(Self.tvpeopleViewColumn3.DataBinding.FieldName).AsString;   //工资类型
        dwLeastWork:= FieldByName(Self.tvpeopleViewColumn4.DataBinding.FieldName).AsInteger;  //最少工作时间

        dwsection  := FieldByName(Self.tvpeopleViewColumn6.DataBinding.FieldName).AsString;   //部门
        szColName  := Self.tvpeopleViewColumn7.DataBinding.FieldName;
        dwNumbers  := FieldByName(Self.tvpeopleViewColumn7.DataBinding.FieldName).AsString;   //编号
        Self.cxLabel11.Caption := dwsection;
        {
        Self.N1.Caption := '工资类型:' + dwwagestype+'&';
        Self.N2.Caption := '合同金额:' + CurrToStr( dwUnitPrice )  + ' 加班单价:' + CurrToStr( dwOverPrice )+'&';
        Self.N4.Caption := '最少天数:' + IntToStr( dwLeastWork ) ;
        }
        RzToolButton2.Hint := '工资类型:' + dwwagestype+ #13#10 +
                              '合同金额:' + CurrToStr( dwUnitPrice ) + #13#10 +
                              '最少天数:' + IntToStr( dwLeastWork ) ;
      end;
    end;

    with Self.ADODays  do
    begin
      Close;
      SQL.Text := 'Select * from ' + g_Table_Company_Staff_CheckWork + ' Where ' + 'code'  + '="' + dwNumbers + '"'; //员工表
      Open;
      if RecordCount <> 0 then
      begin
        dwIsSelectStaff := True;
      end;
      Sort :=  'Year ASC, Month ASC';
    end;

    with Self.ADOOvertime do
    begin
      Close;
      SQL.Text := 'Select * from ' + g_Table_Company_Staff_Over + ' Where ' +  'code' + '="' + dwNumbers + '"';;
      Open;
      Sort :=  'Year ASC';
    end;

    with DM do
    begin
      dwBlendSQL := 'Select * from ' + g_Table_Company_BlendStaff +
                    ' Where ' + Self.tvWorkCol2.DataBinding.FieldName + //公司名称
                    '=' + IntToStr( dwCompayInfoIndex ) + ' AND ' +
                    Self.tvWorkCol9.DataBinding.FieldName +
                    '="' + dwFullname + '"';

      ADOQuery1.Close;
      ADOQuery1.SQL.Clear;
      ADOQuery1.SQL.Text := dwBlendSQL;
      ADOQuery1.Open;
      Self.Master.Close;

      Self.Master.Data := DataSetProvider1.Data;
      Self.Master.Open;

    end;
  end;

end;

procedure TfrmCompanyWork.tvpeopleViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  AAllow := False;
end;


procedure TfrmCompanyWork.tvWorkCol9PropertiesCloseUp(Sender: TObject);
var
  DD2 : Variant;

  szRowIndex: Integer;
  szCol1Name: Variant;
  szCol2Name: Variant;
  szSQLText : string;
  szDate , DD1 : string;
  i : Integer;
  szIsExist:Boolean;
  szCol3Name : Variant;
  szCol4Name : Variant;

begin
  inherited;
  szCol3Name := Self.tvWorkCol9.EditValue;
  szCol4Name := Self.tvWorkCol10.EditValue;

  with (Sender as TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;
    if szRowIndex >= 0 then
    begin
      szCol1Name := DataController.Values[szRowIndex, 0]; //姓名
      ////////////////////////////////////////////////////////////////////////
      szCol2Name := DataController.Values[szRowIndex, 1]; //职务
      szDate := Self.tvWorkCol8.EditValue;
      Self.tvParentId.EditValue := GetStaffId(szCol1Name);
      szIsExist := False;

      with Self.tvGrid1 do
      begin

        for I := 0 to ViewData.RowCount-1 do
        begin
            DD1 := ViewData.Rows[i].Values[Self.tvWorkCol8.Index]; //日期
            DD2 := ViewData.Rows[i].Values[Self.tvWorkCol9.Index];
            if (DD2 = szCol1Name) AND
               (VarToStr( DD1 ) = VarToStr( szDate )) and
               (i <> Controller.FocusedRowIndex) then
            begin
              szIsExist := True;
              Break;
            end;
        end;

      end;

      with DM.Qry do
      begin
        if (State <> dsInactive) and (not szIsExist) then
        begin
          szSQLText := 'Select * from ' + g_Table_Company_BlendStaff +
                       ' Where ' + Self.tvWorkCol9.DataBinding.FieldName +
                       '="' + VarToStr( szCol1Name ) +
                       '" AND ' + Self.tvWorkCol8.DataBinding.FieldName +
                       '=#' + szDate + '#';
          SQL.Clear;
          SQL.Text := szSQLText;
          Open;
          if RecordCount <> 0 then szIsExist := True;
        end;

      end;

      if not szIsExist then
      begin
        Self.tvWorkCol9.EditValue  := szCol1Name;
        Self.tvWorkCol10.EditValue := szCol2Name;
      end else
      begin
        if szCol3Name <> null then
        begin
          Self.tvWorkCol9.EditValue  := szCol3Name;
          Self.tvWorkCol10.EditValue := szCol4Name;
        end else
        begin
          Self.tvWorkCol9.EditValue  := Null;
          Self.tvWorkCol10.EditValue := Null;
        end;

        Application.MessageBox(PWideChar('当前日期（' + VarToStr( szDate ) + '）'  + '已有（' + VarToStr(szCol1Name) +'）存在' ), '新增或编辑记录？', MB_OKCANCEL + MB_ICONWARNING)
      end;

    end;

  end;

end;

procedure TfrmCompanyWork.yearMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  Self.year.DroppedDown := True;
end;

procedure TfrmCompanyWork.yearPropertiesCloseUp(Sender: TObject);
begin
  inherited;
  with Self.ADODays do
  begin
    Close;
    SQL.Text := 'Select * from ' + g_Table_Company_Staff_CheckWork +
                ' Where ' + Self.tvDaysCol3.DataBinding.FieldName +
                '=' + Self.year.Text + ' AND parent=' + dwParent + '';
    Open;
  end;
end;

procedure TfrmCompanyWork.RzToolButton13Click(Sender: TObject);
begin
  GetCompayInfoList(Self.cxLookupComboBox1.Text);
end;

procedure TfrmCompanyWork.RzToolButton14Click(Sender: TObject);
begin
  inherited;
//编辑
  if Self.tvGrid1.Controller.FocusedRowIndex >= 0 then dwADO.ADOIsEdit(Self.Master);

end;

end.
