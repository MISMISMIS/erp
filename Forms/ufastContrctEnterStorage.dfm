inherited fastContrctEnterStorage: TfastContrctEnterStorage
  Caption = #24555#25463#21333'-'#21512#21516#20837#24211
  PixelsPerInch = 96
  TextHeight = 13
  inherited RzToolbar4: TRzToolbar
    ToolbarControls = (
      RzSpacer26
      RzToolButton1
      RzSpacer1
      RzToolButton7
      RzSpacer6
      RzToolButton6
      RzSpacer2
      RzToolButton2
      RzSpacer4
      RzToolButton23
      RzSpacer31
      RzToolButton8
      RzSpacer7
      RzToolButton24
      RzSpacer32
      RzToolButton4
      RzSpacer33
      cxDBNavigator1
      RzSpacer5
      RzToolButton5
      RzSpacer3
      RzToolButton9
      RzSpacer8
      RzToolButton3)
  end
  inherited cxGroupBox1: TcxGroupBox
    inherited cxDBComboBox1: TcxDBComboBox
      Properties.Items.Strings = (
        #36134#30446#20837#24211#21333
        #36827#36135#20837#24211#21333
        #31199#20837#36827#24211#21333
        #20511#20837#36827#36135#21333
        #35843#20837#36827#36135#21333)
    end
    inherited cxDBButtonEdit1: TcxDBButtonEdit
      Hint = #25910#36135
      DataBinding.DataField = 'ReceiveCompany'
    end
    inherited cxDBButtonEdit2: TcxDBButtonEdit
      Hint = #21457#36135
      DataBinding.DataField = 'DeliverCompany'
    end
  end
  inherited cxGroupBox3: TcxGroupBox
    inherited cxDBComboBox7: TcxDBComboBox
      Properties.Items.Strings = (
        #25104#26412#25346#36134
        #29616#37329#25903#27454
        #31199#20837#32479#35745
        #20511#20837#32479#35745
        #35843#20837#32479#35745)
    end
  end
  inherited RzPanel5: TRzPanel
    inherited RzPanel1: TRzPanel
      inherited cxTextEdit1: TcxTextEdit
        Style.IsFontAssigned = True
      end
    end
  end
  inherited Panel2: TPanel
    inherited cxGroupBox2: TcxGroupBox
      inherited Navigator: TDBNavigator
        Left = 791
        Top = 168
        Hints.Strings = ()
        ExplicitLeft = 791
        ExplicitTop = 168
      end
    end
  end
  inherited dsGroupUnitPrice: TClientDataSet
    Left = 376
    Top = 240
  end
  inherited DataGroupUnitPrice: TDataSource
    Left = 376
    Top = 304
  end
end
