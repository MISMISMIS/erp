inherited fastContrctGooutStorage: TfastContrctGooutStorage
  Caption = #24555#25463#21333'-'#21512#21516#20986#24211
  PixelsPerInch = 96
  TextHeight = 13
  inherited RzToolbar4: TRzToolbar
    ToolbarControls = (
      RzSpacer26
      RzToolButton1
      RzSpacer1
      RzToolButton7
      RzSpacer6
      RzToolButton6
      RzSpacer2
      RzToolButton2
      RzSpacer4
      RzToolButton23
      RzSpacer31
      RzToolButton8
      RzSpacer7
      RzToolButton24
      RzSpacer32
      RzToolButton4
      RzSpacer33
      cxDBNavigator1
      RzSpacer5
      RzToolButton5
      RzSpacer3
      RzToolButton9
      RzSpacer8
      RzToolButton3)
    inherited RzToolButton3: TRzToolButton
      Left = 841
      ExplicitLeft = 841
    end
    inherited RzSpacer8: TRzSpacer
      Left = 833
      ExplicitLeft = 833
    end
    inherited RzToolButton9: TRzToolButton
      Width = 96
      ExplicitWidth = 96
    end
  end
  inherited cxGroupBox1: TcxGroupBox
    inherited cxLabel1: TcxLabel
      Caption = #21457#36135#20179#24211#65306
    end
    inherited cxLabel3: TcxLabel
      Caption = #21457#36135#21333#20301#65306
    end
    inherited cxDBComboBox1: TcxDBComboBox
      Properties.Items.Strings = (
        #36134#30446#20986#24211#21333
        #38144#36135#20986#24211#21333
        #31199#20986#38144#36135#21333
        #20511#20986#38144#36135#21333
        #35843#20986#38144#36135#21333)
    end
    inherited cxDBButtonEdit1: TcxDBButtonEdit
      Left = 421
      Top = 21
      Hint = #21457#36135
      DataBinding.DataField = 'DeliverCompany'
      ExplicitLeft = 421
      ExplicitTop = 21
      ExplicitHeight = 22
    end
    inherited cxDBButtonEdit2: TcxDBButtonEdit
      Left = 759
      Top = 18
      Hint = #25910#36135
      DataBinding.DataField = 'ReceiveCompany'
      ExplicitLeft = 759
      ExplicitTop = 18
    end
  end
  inherited cxGroupBox3: TcxGroupBox
    inherited cxDBComboBox7: TcxDBComboBox
      Properties.Items.Strings = (
        #33829#25910#25346#36134
        #29616#37329#25910#27454
        #31199#20986#32479#35745
        #20511#20986#32479#35745
        #35843#20986#32479#35745)
    end
  end
  inherited RzPanel5: TRzPanel
    inherited RzPanel6: TRzPanel
      inherited cxDBTextEdit7: TcxDBTextEdit
        ExplicitHeight = 22
      end
    end
    inherited RzPanel8: TRzPanel
      inherited cxDBCurrencyEdit1: TcxDBCurrencyEdit
        ExplicitHeight = 22
      end
    end
    inherited RzPanel1: TRzPanel
      inherited cxTextEdit1: TcxTextEdit
        Style.IsFontAssigned = True
        ExplicitHeight = 22
      end
    end
  end
  inherited Panel2: TPanel
    inherited cxGroupBox2: TcxGroupBox
      inherited Navigator: TDBNavigator
        Hints.Strings = ()
      end
      inherited cxDBCheckBox1: TcxDBCheckBox
        ExplicitWidth = 48
      end
    end
  end
end
