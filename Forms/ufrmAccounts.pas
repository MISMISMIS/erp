unit ufrmAccounts;

interface

{
   结算窗体
}

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, RzEdit, RzButton, ComCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver, cxMemo,
  cxTextEdit, cxCurrencyEdit, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010,
  dxSkinWhiteprint, Vcl.ExtCtrls, RzPanel;

type
  TfrmAccounts = class(TForm)
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    RzBitBtn1: TRzBitBtn;
    RzBitBtn4: TRzBitBtn;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxCurrencyEdit2: TcxCurrencyEdit;
    cxCurrencyEdit3: TcxCurrencyEdit;
    cxMemo1: TcxMemo;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    cxTextEdit3: TcxTextEdit;
    procedure RzBitBtn2Click(Sender: TObject);
    procedure RzBitBtn3Click(Sender: TObject);
    procedure cxCurrencyEdit1PropertiesChange(Sender: TObject);
    procedure cxCurrencyEdit2PropertiesChange(Sender: TObject);
    procedure cxCurrencyEdit3PropertiesChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    dwTable_ParentCode : string;
    dwTable_Projectcode: string;
    dwTable_ProjectName: string;
    dwTable_ParentName : string;
    dwTable_SumMoney   : string;
    dwTable_MoneyTypes : string;
    dwTable_MoneyType  : string;
    dwTable_Status     : string;
    dwTable_remarks    : string;

  public
    { Public declarations }
    g_ProjectName : string;
    g_ParentName  : string;

    g_ProjectCode : string;
    g_ParentCode  : string;

    g_PostCode : string;
    g_SettleStatus   : string;
    g_tableName : string;
    g_Classify  : Integer;
  end;

var
  frmAccounts: TfrmAccounts;

implementation

uses
   ufrmManage,global,uDataModule;

{$R *.dfm}

procedure TfrmAccounts.cxCurrencyEdit1PropertiesChange(Sender: TObject);
begin
  Self.cxTextEdit1.Text := MoneyConvert(Self.cxCurrencyEdit1.Value);
end;

procedure TfrmAccounts.cxCurrencyEdit2PropertiesChange(Sender: TObject);
begin
  Self.cxTextEdit2.Text := MoneyConvert(Self.cxCurrencyEdit2.Value);
end;

procedure TfrmAccounts.cxCurrencyEdit3PropertiesChange(Sender: TObject);
begin
  Self.cxTextEdit3.Text := MoneyConvert(Self.cxCurrencyEdit3.Value);
end;

procedure TfrmAccounts.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    13:
    begin
      Self.RzBitBtn1.Click;
    end;
    27:
    begin
      Close;
    end;
  end;
end;

procedure TfrmAccounts.FormShow(Sender: TObject);
begin
  dwTable_remarks    := 'remarks';
  dwTable_Status     := 'status';
  dwTable_ParentCode := 'Code';
  dwTable_Projectcode:= 'Projectcode';
  dwTable_ProjectName:= 'ProjectName';
  dwTable_ParentName := 'ParentName';
  dwTable_SumMoney   := 'SumMoney';
  dwTable_MoneyTypes := 'MoneyTypes';
  dwTable_MoneyType  := 'MoneyType';

  Caption := '结算 (' + g_ParentName  +  ' - ' + g_ProjectName + ')' ;
end;

procedure TfrmAccounts.RzBitBtn2Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmAccounts.RzBitBtn3Click(Sender: TObject);
var
  t : Currency;
  i : Integer;

  s : string;
  sqlstr : string;
  time : string;
  szCode : string;
  szSumMoney  : Currency;

begin
  t := Self.cxCurrencyEdit3.Value;
  if  t < 0 then
  begin
    s := '支';
    i := 0;
    szSumMoney := 0 - t;
  end
  else
  begin
    szSumMoney := t;
    i := 1;
    s := '收';
    if t = 0 then
    begin
      s := '清';
      i := 2;
    end;
  end;
  DM.ADOconn.BeginTrans; //开始事务
  try
    with DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := 'Select * from '+ g_Table_pactDebtmoney +
                  ' WHERE '+ dwTable_ParentCode +'="' + g_ParentCode + '" AND ' + dwTable_Projectcode + '="' + g_ProjectCode +'"' ;
      Open;

      if RecordCount <> 0 then
      begin
        Edit;

          FieldByName( dwTable_Status).Value := True;
          FieldByName( dwTable_ParentCode ).Value := g_ParentCode;
          FieldByName( dwTable_Projectcode).Value := g_ProjectCode;
          FieldByName( dwTable_ParentName).Value  := g_ParentName;
          FieldByName( dwTable_ProjectName).Value := g_ProjectName;
          FieldByName( dwTable_SumMoney).Value    := szSumMoney;
          FieldByName( dwTable_MoneyTypes).Value  := s;
          FieldByName( dwTable_MoneyType).Value   := i;
          FieldByName( dwTable_remarks ).Value    := Self.cxMemo1.Text;
        UpdateBatch();
      end else
      begin
        if szSumMoney > 0 then
        begin
          Append;
            FieldByName( dwTable_Status).Value := True;
            FieldByName( dwTable_ParentCode ).Value := g_ParentCode;
            FieldByName( dwTable_Projectcode).Value := g_ProjectCode;
            FieldByName( dwTable_ParentName).Value  := g_ParentName;
            FieldByName( dwTable_ProjectName).Value := g_ProjectName;
            FieldByName( dwTable_SumMoney).Value    := szSumMoney;
            FieldByName( dwTable_MoneyTypes).Value  := s;
            FieldByName( dwTable_MoneyType).Value   := i;
            FieldByName( dwTable_remarks ).Value    := Self.cxMemo1.Text;
          Post;
        end;
      end;
      Close;
      SQL.Clear;
      SQL.Text := 'UPDATE ' + g_Table_Collect_Receivables +' SET status=no WHERE Code="'+ g_ProjectCode +'"'; //冻结合同总价
      ExecSQL;
      Close;
      SQL.Clear;
      SQL.Text := 'UPDATE ' + g_Table_Income +' SET status=no WHERE Code="'+ g_ProjectCode +'"';//冻结交易记录
      ExecSQL;
      Close;
      SQL.Clear;
      SQL.Text := 'UPDATE ' + g_Table_RuningAccount +' SET status=no WHERE ProjectName="'+ g_ProjectName +'" AND fromType=0';//冻结收款的交易流水
      ExecSQL;
    end;
    DM.ADOconn.Committrans; //提交事务

  except
    on E:Exception do
    begin
      DM.ADOconn.RollbackTrans;           // 事务回滚
      ShowMessage(E.Message);
    end;
  end;
  Close;
end;




end.
