object frmCompanyManage: TfrmCompanyManage
  Left = 0
  Top = 0
  ActiveControl = RzToolbar4
  Caption = #24448#26469#21333#20301
  ClientHeight = 594
  ClientWidth = 1071
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter2: TSplitter
    Left = 281
    Top = 30
    Width = 10
    Height = 545
    Color = 16250613
    ParentColor = False
    ResizeStyle = rsUpdate
    ExplicitLeft = 245
    ExplicitTop = -198
    ExplicitHeight = 614
  end
  object RzPanel4: TRzPanel
    Left = 0
    Top = 30
    Width = 281
    Height = 545
    Align = alLeft
    BorderOuter = fsFlat
    BorderSides = [sdLeft, sdRight]
    TabOrder = 0
    object Tv: TTreeView
      Left = 1
      Top = 32
      Width = 279
      Height = 513
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      DragMode = dmAutomatic
      HideSelection = False
      Images = DM.TreeImageList
      Indent = 19
      RowSelect = True
      SortType = stBoth
      TabOrder = 0
      OnCancelEdit = TvCancelEdit
      OnChange = TvChange
      OnDragDrop = TvDragDrop
      OnDragOver = TvDragOver
      OnEdited = TvEdited
      OnKeyDown = TvKeyDown
      Items.NodeData = {
        0301000000260000000000000000000000FFFFFFFFFFFFFFFF00000000000000
        0002000000010406527B7CA17B0674220000000100000000000000FFFFFFFFFF
        FFFFFF000000000000000001000000010236653E6B2600000001000000000000
        00FFFFFFFFFFFFFFFF0000000000000000000000000104FA5EBE8B55534D4F22
        0000000100000000000000FFFFFFFFFFFFFFFF00000000000000000300000001
        022F653E6B280000000100000000000000FFFFFFFFFFFFFFFF00000000000000
        000000000001057998EE76E89050679965280000000100000000000000FFFFFF
        FFFFFFFFFF00000000000000000000000001057998EE76E89006520553280000
        000100000000000000FFFFFFFFFFFFFFFF000000000000000000000000010579
        98EE76E8900380E452}
    end
    object RzToolbar1: TRzToolbar
      Left = 1
      Top = 0
      Width = 279
      AutoStyle = False
      Images = DM.cxImageList1
      RowHeight = 28
      BorderInner = fsNone
      BorderOuter = fsFlat
      BorderSides = [sdTop]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 1
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer1
        RzToolButton1
        RzSpacer3
        RzToolButton2
        RzSpacer2
        RzToolButton6
        RzSpacer23
        RzToolButton25
        RzSpacer24
        RzToolButton26)
      object RzSpacer1: TRzSpacer
        Left = 4
        Top = 4
      end
      object RzToolButton1: TRzToolButton
        Left = 12
        Top = 4
        Width = 24
        SelectionColorStop = 16119543
        ImageIndex = 37
        ShowCaption = False
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        OnClick = RzToolButton1Click
      end
      object RzToolButton2: TRzToolButton
        Left = 44
        Top = 4
        Width = 24
        SelectionColorStop = 16119543
        ImageIndex = 0
        ShowCaption = False
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #20445#23384
        OnClick = RzToolButton2Click
      end
      object RzSpacer2: TRzSpacer
        Left = 68
        Top = 4
      end
      object RzToolButton6: TRzToolButton
        Left = 76
        Top = 4
        Width = 24
        SelectionColorStop = 16119543
        ImageIndex = 51
        ShowCaption = False
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21024#38500
        OnClick = RzToolButton6Click
      end
      object RzSpacer23: TRzSpacer
        Left = 100
        Top = 4
      end
      object RzToolButton25: TRzToolButton
        Left = 108
        Top = 4
        SelectionColorStop = 16119543
        ImageIndex = 29
        OnClick = RzToolButton25Click
      end
      object RzSpacer24: TRzSpacer
        Left = 133
        Top = 4
      end
      object RzToolButton26: TRzToolButton
        Left = 141
        Top = 4
        SelectionColorStop = 16119543
        ImageIndex = 31
        OnClick = RzToolButton26Click
      end
      object RzSpacer3: TRzSpacer
        Left = 36
        Top = 4
      end
    end
  end
  object RzPanel6: TRzPanel
    Left = 291
    Top = 30
    Width = 780
    Height = 545
    Align = alClient
    BorderOuter = fsFlat
    BorderSides = [sdLeft, sdRight]
    TabOrder = 1
    object RzToolbar4: TRzToolbar
      Left = 1
      Top = 0
      Width = 778
      AutoStyle = False
      Images = DM.cxImageList1
      RowHeight = 28
      BorderInner = fsNone
      BorderOuter = fsFlat
      BorderSides = [sdTop]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 0
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer6
        RzToolButton10
        RzSpacer5
        RzToolButton4
        RzSpacer9
        RzToolButton11
        RzSpacer7
        RzToolButton13
        RzSpacer8
        cxLabel1
        RzSpacer11
        cxTextEdit1
        RzSpacer12
        RzToolButton14
        RzSpacer4
        RzToolButton5
        RzSpacer10
        RzToolButton3)
      object RzSpacer6: TRzSpacer
        Left = 4
        Top = 4
      end
      object RzToolButton10: TRzToolButton
        Left = 12
        Top = 4
        Width = 24
        SelectionColorStop = 16119543
        ImageIndex = 37
        ShowCaption = False
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        OnClick = RzToolButton10Click
      end
      object RzToolButton11: TRzToolButton
        Left = 77
        Top = 4
        Width = 24
        SelectionColorStop = 16119543
        ImageIndex = 3
        ShowCaption = False
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #20445#23384
        OnClick = RzToolButton11Click
      end
      object RzSpacer7: TRzSpacer
        Left = 101
        Top = 4
      end
      object RzToolButton13: TRzToolButton
        Left = 109
        Top = 4
        Width = 24
        SelectionColorStop = 16119543
        ImageIndex = 51
        ShowCaption = False
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21024#38500
        OnClick = RzToolButton10Click
      end
      object RzSpacer8: TRzSpacer
        Left = 133
        Top = 4
      end
      object RzSpacer11: TRzSpacer
        Left = 181
        Top = 4
      end
      object RzSpacer12: TRzSpacer
        Left = 370
        Top = 4
      end
      object RzToolButton14: TRzToolButton
        Left = 378
        Top = 4
        SelectionColorStop = 16119543
        ImageIndex = 10
        OnClick = RzToolButton14Click
      end
      object RzSpacer4: TRzSpacer
        Left = 403
        Top = 4
      end
      object RzToolButton3: TRzToolButton
        Left = 461
        Top = 4
        SelectionColorStop = 16119543
        ImageIndex = 8
        OnClick = RzToolButton3Click
      end
      object RzSpacer5: TRzSpacer
        Left = 36
        Top = 4
      end
      object RzSpacer9: TRzSpacer
        Left = 69
        Top = 4
      end
      object RzToolButton4: TRzToolButton
        Left = 44
        Top = 4
        SelectionColorStop = 16119543
        ImageIndex = 0
        OnClick = RzToolButton4Click
      end
      object RzSpacer10: TRzSpacer
        Left = 453
        Top = 4
      end
      object RzToolButton5: TRzToolButton
        Left = 411
        Top = 4
        Width = 42
        SelectionColorStop = 16119543
        DropDownMenu = Print
        ImageIndex = 5
        ShowCaption = True
        UseToolbarShowCaption = False
        ToolStyle = tsDropDown
      end
      object cxLabel1: TcxLabel
        Left = 141
        Top = 8
        Caption = #20851#38190#23383
        Transparent = True
      end
      object cxTextEdit1: TcxTextEdit
        Left = 189
        Top = 6
        TabOrder = 1
        TextHint = #24448#26469#21333#20301
        OnKeyDown = cxTextEdit1KeyDown
        Width = 181
      end
    end
    object Grid: TcxGrid
      Left = 1
      Top = 32
      Width = 778
      Height = 513
      Align = alClient
      TabOrder = 1
      object tvView: TcxGridDBTableView
        OnDblClick = tvViewDblClick
        OnKeyDown = tvViewKeyDown
        Navigator.Buttons.CustomButtons = <>
        OnEditing = tvViewEditing
        OnEditKeyDown = tvViewEditKeyDown
        DataController.DataSource = ds
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.FocusFirstCellOnNewRecord = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsBehavior.FocusCellOnCycle = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsSelection.InvertSelect = False
        OptionsSelection.MultiSelect = True
        OptionsSelection.CellMultiSelect = True
        OptionsView.DataRowHeight = 23
        OptionsView.GroupByBox = False
        OptionsView.HeaderHeight = 20
        OptionsView.Indicator = True
        OptionsView.IndicatorWidth = 16
        object tvCol1: TcxGridDBColumn
          Caption = #24207#21495
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taCenter
          OnGetDisplayText = tvCol1GetDisplayText
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Sorting = False
          Width = 40
        end
        object tvCol6: TcxGridDBColumn
          Caption = #29366#24577
          DataBinding.FieldName = 'Status'
          PropertiesClassName = 'TcxCheckBoxProperties'
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Sorting = False
          Width = 43
        end
        object tvCol2: TcxGridDBColumn
          Caption = #32534#21495
          DataBinding.FieldName = 'Numbers'
          Visible = False
          HeaderAlignmentHorz = taCenter
          Width = 100
        end
        object tvCol4: TcxGridDBColumn
          Caption = #20132#26131#21495
          DataBinding.FieldName = 'PayNumber'
          HeaderAlignmentHorz = taCenter
          Width = 100
        end
        object tvCol3: TcxGridDBColumn
          Caption = #21517#31216
          DataBinding.FieldName = 'SignName'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.OnValidate = tvCol3PropertiesValidate
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Width = 220
        end
        object tvCol5: TcxGridDBColumn
          Caption = #31616#30721
          DataBinding.FieldName = 'PYCode'
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Width = 70
        end
        object tvCol7: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object tvViewColumn2: TcxGridDBColumn
          Caption = #32852#31995#20154
          DataBinding.FieldName = 'Contacts'
          Width = 78
        end
        object tvViewColumn1: TcxGridDBColumn
          Caption = #30005#35805
          DataBinding.FieldName = 'Tel'
          Width = 84
        end
        object tvViewColumn3: TcxGridDBColumn
          Caption = #22320#22336
          DataBinding.FieldName = 'Address'
          Width = 113
        end
        object tvViewColumn4: TcxGridDBColumn
          Caption = #32534#21495
          DataBinding.FieldName = 'Numbers'
          Visible = False
          Width = 60
        end
      end
      object Lv: TcxGridLevel
        GridView = tvView
      end
    end
  end
  object RzPanel5: TRzPanel
    Left = 0
    Top = 0
    Width = 1071
    Height = 30
    Align = alTop
    BorderOuter = fsNone
    Caption = #24448#26469#21333#20301
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenuHighlight
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 575
    Width = 1071
    Height = 19
    Panels = <
      item
        Width = 150
      end>
  end
  object qry: TADOQuery
    Connection = DM.ADOconn
    AfterOpen = qryAfterOpen
    AfterInsert = qryAfterInsert
    AfterEdit = qryAfterEdit
    AfterScroll = qryAfterScroll
    Parameters = <>
    Left = 320
    Top = 120
  end
  object ds: TDataSource
    DataSet = qry
    Left = 320
    Top = 176
  end
  object ADOTree: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 72
    Top = 144
  end
  object DataTree: TDataSource
    DataSet = ADOTree
    Left = 72
    Top = 208
  end
  object Print: TPopupMenu
    Images = DM.cxImageList1
    Left = 360
    Top = 176
    object Excel1: TMenuItem
      Caption = #36755#20986'Excel'
      OnClick = Excel1Click
    end
    object N2: TMenuItem
      Caption = #25171#21360#25253#34920
      ImageIndex = 6
      OnClick = N2Click
    end
  end
end
