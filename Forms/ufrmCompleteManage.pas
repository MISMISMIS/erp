unit ufrmCompleteManage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh,
  ComCtrls, RzListVw, GridsEh, DBAxisGridsEh, DBGridEh, StdCtrls, RzCmboBx,
  Mask, RzEdit, RzLabel, RzPanel, PrnDbgeh, RzButton, ExtCtrls, ImgList, DB,
  ADODB,TreeFillThrd,TreeUtils, FileCtrl, RzFilSys, Menus, EhLibVCL, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxCore,
  cxDateUtils, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, cxLabel,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, cxDBData, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,ufrmBaseController;

type
  TfrmCompleteManage = class(TfrmBaseController)
    RzPanel1: TRzPanel;
    DataSource1: TDataSource;
    ADOQuery1: TADOQuery;
    RzToolbar12: TRzToolbar;
    RzSpacer100: TRzSpacer;
    RzToolButton75: TRzToolButton;
    RzSpacer101: TRzSpacer;
    RzToolButton76: TRzToolButton;
    RzSpacer102: TRzSpacer;
    RzToolButton77: TRzToolButton;
    RzSpacer103: TRzSpacer;
    RzToolButton80: TRzToolButton;
    RzSpacer106: TRzSpacer;
    RzSpacer107: TRzSpacer;
    RzToolButton81: TRzToolButton;
    RzSpacer109: TRzSpacer;
    RzSpacer110: TRzSpacer;
    cxLabel50: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxLabel51: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    Grid: TcxGrid;
    TvTableView: TcxGridDBTableView;
    TvCol1: TcxGridDBColumn;
    TvCol2: TcxGridDBColumn;
    TvCol5: TcxGridDBColumn;
    TvCol3: TcxGridDBColumn;
    TvCol4: TcxGridDBColumn;
    TvTableViewColumn1: TcxGridDBColumn;
    TvTableViewColumn2: TcxGridDBColumn;
    Lv: TcxGridLevel;
    RzPanel4: TRzPanel;
    Splitter1: TSplitter;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzToolButton75Click(Sender: TObject);
    procedure ADOQuery1AfterInsert(DataSet: TDataSet);
    procedure ADOQuery1AfterOpen(DataSet: TDataSet);
    procedure RzToolButton80Click(Sender: TObject);
    procedure RzToolButton81Click(Sender: TObject);
    procedure RzToolButton76Click(Sender: TObject);
    procedure RzToolButton77Click(Sender: TObject);
    procedure TvCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure TvTableViewColumn1GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure TvTableViewDblClick(Sender: TObject);
    procedure TvTableViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure TvTableViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    g_PatTree  : TTreeUtils;
    g_ProjectDir : string;
    g_Table_PactTree : string;

    g_ProjectName : string;
    g_PostCode    : string;
    IsSave : Boolean;
    Prefix : string;
    Suffix : string;
  public
    { Public declarations }
    procedure ShowData(lpCodeList: string);
    procedure WndProc(var Message: TMessage); override;  // 第一优先权
  end;

var
  frmCompleteManage: TfrmCompleteManage;

implementation

uses
   uProjectFrame,
   ShellAPI,
   global,
   uDataModule,
   ufunctions;

{$R *.dfm}

procedure TfrmCompleteManage.WndProc(var Message: TMessage);
var
  pMsg : PProject;
  s : string;
  szCodeList : string;
  szTableList : array[0..1] of TTableList;
begin
  case Message.Msg of
    WM_FrameClose:
    begin
      Self.ADOQuery1.Close;
    end;
    WM_FrameView :
    begin
      pMsg := PProject(Message.LParam);
      g_PostCode := pMsg.dwCode;
      g_ProjectName := pMsg.dwName;

      with Self.ADOQuery1 do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'select * from ' + g_Table_Project_Complete + ' where code in(''' + g_PostCode + ''')';
        Open;
      end;

    end;
    WM_FrameDele :
    begin
      pMsg   := PProject(Message.LParam);
      szCodeList := pMsg.dwCodeList;
      szTableList[0].ATableName := g_Table_Project_Complete;
      szTableList[0].ADirectory := g_Dir_Project_Complete;

      DeleteTableFile(szTableList,szCodeList);
    end;
  end;
  // 一定要加上这句，否则编译通不过。因为绝大部分消息没人处理了
  inherited WndProc(Message); // 会一路向上调用，直到TControl.WndProc调用Dispatch来寻找消息处理函数
end;


procedure TfrmCompleteManage.ADOQuery1AfterInsert(DataSet: TDataSet);
var
  szCode : string;
  szColName : string;
  Aptitude  : string;
  ProjectDepartment : string;

begin
  Aptitude := GetProjectSurvey(g_PostCode,'AptitudeCompany');
  ProjectDepartment := GetProjectSurvey(g_PostCode,'ProjectDepartment');

  with DataSet do
  begin
    if State <> dsInactive then
    begin
      szColName := Self.TvCol2.DataBinding.FieldName;
      szCode := Prefix + GetRowCode( g_Table_Project_Complete ,szColName,Suffix,130000);
      FieldByName(Self.TvCol2.DataBinding.FieldName).Value := szCode;
      FieldByName(Self.TvCol5.DataBinding.FieldName).Value := Date;
      FieldByName(Self.TvCol3.DataBinding.FieldName).Value := g_ProjectName;
      FieldByName('Code').Value := g_PostCode;
      FieldByName(Self.TvCol4.DataBinding.FieldName).Value := Aptitude;
      FieldByName(Self.TvTableViewColumn2.DataBinding.FieldName).Value := ProjectDepartment;
    end;

  end;

end;

procedure TfrmCompleteManage.ADOQuery1AfterOpen(DataSet: TDataSet);
begin
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmCompleteManage.FormActivate(Sender: TObject);
begin
  g_ProjectDir := Concat(g_Resources,'\CompleteManage');
end;

procedure TfrmCompleteManage.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmCompleteManage.FormCreate(Sender: TObject);
var
  i : Integer;
  List : TStringList;
  ChildFrame : TProjectFrame;
begin
  inherited;
  Prefix := 'JG-';
  Suffix := '00012';

  ChildFrame := TProjectFrame.Create(Self);
  ChildFrame.dwParentHandle := Handle;
  ChildFrame.IsSystem := True;
  ChildFrame.Parent := Self.RzPanel4;
  ChildFrame.FrameClick(Self);

  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;
end;

procedure TfrmCompleteManage.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then
  begin
    Close;
  end;
end;

procedure TfrmCompleteManage.RzToolButton75Click(Sender: TObject);
begin
  with Self.ADOQuery1 do
  begin
    if State <> dsInactive then
    begin
      First;
      Insert;
    end;
  end;
end;

procedure TfrmCompleteManage.RzToolButton76Click(Sender: TObject);
begin
  IsDeleteEmptyData(Self.ADOQuery1,
                    g_Table_Project_Complete ,
                    Self.TvCol4.DataBinding.FieldName);
  IsSave := False;
end;

procedure TfrmCompleteManage.RzToolButton77Click(Sender: TObject);
begin
  DeleteSelection(Self.TvTableView,'',g_Table_Project_Complete,
  Self.TvCol2.DataBinding.FieldName);
  Self.ADOQuery1.Requery();
end;

procedure TfrmCompleteManage.RzToolButton80Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmCompleteManage.RzToolButton81Click(Sender: TObject);
var
  sqltext : string;
  szColName : string;
begin
  inherited;
  szColName   := Self.TvCol5.DataBinding.FieldName;
  sqltext := 'select *from '  + g_Table_Project_Complete +
             ' where Code= "' + g_PostCode +
             ' " and ' + szColName + ' between :t1 and :t2';

  SearchDateRange(Self.cxDateEdit1.Text,Self.cxDateEdit2.Text,sqltext,Self.ADOQuery1);
end;

procedure TfrmCompleteManage.ShowData(lpCodeList: string);
var
  szCode: string;
begin
  with Self.ADOQuery1 do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'select * from ' + g_Table_Project_Complete + ' where code in(''' + lpCodeList + ''')';
    Open;
  end;
end;

procedure TfrmCompleteManage.TvCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmCompleteManage.TvTableViewColumn1GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  with (AProperties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    Items.Add('审计');
    Items.Add('保修');
  end;
end;

procedure TfrmCompleteManage.TvTableViewDblClick(Sender: TObject);
var
  szDir : string;
begin
  inherited;
  szDir :=  Concat( g_Resources,'\' + g_Dir_Project_Complete );
  CreateEnclosure(Self.TvTableView.DataController.DataSource.DataSet,Handle,szDir);
end;

procedure TfrmCompleteManage.TvTableViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  if (AItem.Index = Self.TvCol2.Index) or ( AItem.Index = Self.TvCol1.Index ) then
  begin
    AAllow := False;
  end;
end;

procedure TfrmCompleteManage.TvTableViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then
  begin
    Self.RzToolButton77.Click;
  end;
end;

end.
