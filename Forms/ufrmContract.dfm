object frmContract: TfrmContract
  Left = 0
  Top = 0
  Caption = #21512#21516#31649#29702
  ClientHeight = 570
  ClientWidth = 977
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 0
    Top = 122
    Width = 977
    Height = 8
    Align = alTop
    Shape = bsSpacer
    Style = bsRaised
    ExplicitTop = 117
    ExplicitWidth = 910
  end
  object Bevel2: TBevel
    Left = 0
    Top = 29
    Width = 977
    Height = 8
    Align = alTop
    Shape = bsSpacer
    Style = bsRaised
    ExplicitLeft = -2
    ExplicitTop = 15
    ExplicitWidth = 910
  end
  object cxGroupBox2: TcxGroupBox
    Left = 0
    Top = 130
    Align = alClient
    Caption = #21830#21697#20449#24687
    Style.BorderStyle = ebsFlat
    Style.Edges = [bTop, bBottom]
    Style.LookAndFeel.NativeStyle = False
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 0
    Height = 440
    Width = 977
    object Grid: TcxGrid
      Left = 2
      Top = 18
      Width = 973
      Height = 401
      Align = alClient
      BevelEdges = [beLeft, beTop, beRight]
      BevelInner = bvSpace
      BevelKind = bkFlat
      BorderStyle = cxcbsNone
      TabOrder = 0
      object tContractView: TcxGridDBTableView
        PopupMenu = pm
        OnDblClick = tContractViewDblClick
        OnKeyDown = tContractViewKeyDown
        Navigator.Buttons.CustomButtons = <>
        Navigator.InfoPanel.Visible = True
        Navigator.Visible = True
        OnEditing = tContractViewEditing
        OnEditKeyDown = tContractViewEditKeyDown
        DataController.DataSource = DetailedSource
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Kind = skSum
          end
          item
            Kind = skSum
          end
          item
            Kind = skCount
          end>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.FocusFirstCellOnNewRecord = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsBehavior.FocusCellOnCycle = True
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsSelection.InvertSelect = False
        OptionsSelection.MultiSelect = True
        OptionsSelection.CellMultiSelect = True
        OptionsView.DataRowHeight = 23
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        OptionsView.HeaderHeight = 23
        OptionsView.Indicator = True
        OptionsView.IndicatorWidth = 14
        OnColumnSizeChanged = tContractViewColumnSizeChanged
        object tContractViewColumn1: TcxGridDBColumn
          Caption = #24207#21495
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.ReadOnly = True
          OnGetDisplayText = tContractViewColumn1GetDisplayText
          Options.Editing = False
          Width = 40
        end
        object tContractViewColumn2: TcxGridDBColumn
          Caption = #29366#24577
          DataBinding.FieldName = 'status'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Visible = False
          Width = 40
        end
        object tContractViewColumn3: TcxGridDBColumn
          Caption = #21333#21495
          DataBinding.FieldName = 'NoId'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.ReadOnly = True
          Visible = False
          Width = 160
        end
        object tContractViewColumn4: TcxGridDBColumn
          Caption = #21830#21697#21517#31216
          DataBinding.FieldName = 'GoodsName'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = tViewCol4PropertiesButtonClick
          Width = 133
        end
        object tContractViewColumn5: TcxGridDBColumn
          Caption = #22411#21495
          DataBinding.FieldName = 'Model'
          Width = 92
        end
        object tContractViewColumn6: TcxGridDBColumn
          Caption = #35268#26684
          DataBinding.FieldName = 'Spec'
          Width = 98
        end
        object tContractViewColumn17: TcxGridDBColumn
          Caption = #31867#21035
          DataBinding.FieldName = 'Category'
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.Items.Strings = (
            #21830#21697#20027#26448
            #32452#21512#36741#26448)
          Width = 80
        end
        object tContractViewColumn7: TcxGridDBColumn
          Caption = #21697#29260
          DataBinding.FieldName = 'brand'
          Width = 60
        end
        object tContractViewColumn8: TcxGridDBColumn
          Caption = #21378#23478
          DataBinding.FieldName = 'Manufactor'
          PropertiesClassName = 'TcxTextEditProperties'
          Width = 80
        end
        object tContractViewColumn9: TcxGridDBColumn
          Caption = #39068#33394
          DataBinding.FieldName = 'NoColour'
          PropertiesClassName = 'TcxTextEditProperties'
          Width = 75
        end
        object tContractViewColumn10: TcxGridDBColumn
          Caption = #26448#36136
          DataBinding.FieldName = 'Texture'
          Width = 60
        end
        object tContractViewColumn11: TcxGridDBColumn
          Caption = #24037#33402#20570#27861
          DataBinding.FieldName = 'Process'
          PropertiesClassName = 'TcxMemoProperties'
          Width = 116
        end
        object tContractViewColumn12: TcxGridDBColumn
          Caption = #36135#21495#20998#31867
          DataBinding.FieldName = 'NoType'
          Width = 80
        end
        object tContractViewColumn13: TcxGridDBColumn
          Caption = #36135#21495#32534#21495
          DataBinding.FieldName = 'NoNumber'
          Width = 80
        end
        object tContractViewColumn14: TcxGridDBColumn
          Caption = #21333#20301
          DataBinding.FieldName = 'Company'
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.ImmediateDropDownWhenActivated = True
          OnGetPropertiesForEdit = tContractViewColumn14GetPropertiesForEdit
          Width = 60
        end
        object tContractViewColumn15: TcxGridDBColumn
          Caption = #21333#20215
          DataBinding.FieldName = 'UnitPrice'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Width = 70
        end
        object tContractViewColumn16: TcxGridDBColumn
          Caption = #22791#27880
          DataBinding.FieldName = 'Remarks'
          Width = 118
        end
      end
      object Lv: TcxGridLevel
        GridView = tContractView
      end
    end
    object StatusBar1: TStatusBar
      Left = 2
      Top = 419
      Width = 973
      Height = 19
      Panels = <
        item
          Width = 50
        end
        item
          Width = 50
        end>
    end
    object DBNavigator1: TDBNavigator
      Left = 674
      Top = 243
      Width = 240
      Height = 25
      DataSource = MasterSource
      TabOrder = 2
      Visible = False
    end
    object cxDBTextEdit1: TcxDBTextEdit
      Left = 650
      Top = 139
      DataBinding.DataField = 'Code'
      DataBinding.DataSource = MasterSource
      TabOrder = 3
      Visible = False
      Width = 199
    end
    object cxDBTextEdit5: TcxDBTextEdit
      Left = 650
      Top = 112
      DataBinding.DataField = 'TreeId'
      DataBinding.DataSource = MasterSource
      TabOrder = 4
      Visible = False
      Width = 199
    end
  end
  object RzToolbar4: TRzToolbar
    Left = 0
    Top = 0
    Width = 977
    Height = 29
    AutoStyle = False
    Images = DM.cxImageList1
    BorderInner = fsNone
    BorderOuter = fsNone
    BorderSides = [sdBottom]
    BorderWidth = 0
    Caption = #38468#20214
    GradientColorStyle = gcsCustom
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    VisualStyle = vsGradient
    DesignSize = (
      977
      29)
    ToolbarControls = (
      RzSpacer26
      RzToolButton1
      RzSpacer1
      RzToolButton7
      RzSpacer6
      RzToolButton6
      RzSpacer2
      RzToolButton2
      RzSpacer4
      RzToolButton23
      RzSpacer31
      RzToolButton8
      RzSpacer7
      RzToolButton24
      RzSpacer32
      RzToolButton4
      RzSpacer33
      cxDBNavigator1
      RzSpacer5
      RzToolButton5
      RzSpacer3
      RzToolButton3)
    object RzSpacer26: TRzSpacer
      Left = 4
      Top = 2
    end
    object RzToolButton23: TRzToolButton
      Left = 305
      Top = 2
      Width = 65
      Hint = #21482#21024#38500#21830#21697#20449#24687
      SelectionColorStop = 16119543
      SelectionFrameColor = clBtnShadow
      ImageIndex = 51
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #21024#38500
      ParentShowHint = False
      ShowHint = True
      OnClick = RzToolButton23Click
    end
    object RzSpacer31: TRzSpacer
      Left = 370
      Top = 2
    end
    object RzToolButton24: TRzToolButton
      Left = 473
      Top = 2
      Width = 65
      SelectionColorStop = 16119543
      SelectionFrameColor = clBtnShadow
      ImageIndex = 5
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      ToolStyle = tsDropDown
      Caption = #25253#34920
      Enabled = False
    end
    object RzSpacer32: TRzSpacer
      Left = 538
      Top = 2
    end
    object RzSpacer33: TRzSpacer
      Left = 611
      Top = 2
    end
    object RzSpacer1: TRzSpacer
      Left = 77
      Top = 2
    end
    object RzToolButton1: TRzToolButton
      Left = 12
      Top = 2
      Width = 65
      Hint = #26032#22686#21512#21516#20449#24687#25110#21830#21697#20449#24687
      SelectionColorStop = 16119543
      ImageIndex = 37
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #26032#22686'&'
      ParentShowHint = False
      ShowHint = True
      OnClick = RzToolButton1Click
    end
    object RzSpacer2: TRzSpacer
      Left = 224
      Top = 2
    end
    object RzToolButton2: TRzToolButton
      Left = 232
      Top = 2
      Width = 65
      Hint = #20445#23384#21040#25968#25454#24211
      SelectionColorStop = 16119543
      ImageIndex = 3
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #20445#23384'&'
      ParentShowHint = False
      ShowHint = True
      OnClick = RzToolButton2Click
    end
    object RzSpacer3: TRzSpacer
      Left = 776
      Top = 2
    end
    object RzToolButton3: TRzToolButton
      Left = 784
      Top = 2
      Width = 65
      SelectionColorStop = 16119543
      ImageIndex = 8
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #36864#20986'&'
      OnClick = RzToolButton3Click
    end
    object RzToolButton4: TRzToolButton
      Left = 546
      Top = 2
      Width = 65
      SelectionColorStop = 16119543
      ImageIndex = 29
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #19978#19968#39029
      OnClick = RzToolButton4Click
    end
    object RzSpacer4: TRzSpacer
      Left = 297
      Top = 2
    end
    object RzToolButton5: TRzToolButton
      Left = 711
      Top = 2
      Width = 65
      SelectionColorStop = 16119543
      ImageIndex = 31
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #19979#19968#39029
      OnClick = RzToolButton5Click
    end
    object RzToolButton6: TRzToolButton
      Left = 159
      Top = 2
      Width = 65
      Hint = #20020#26102#20445#23384
      SelectionColorStop = 16119543
      ImageIndex = 17
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #30830#23450
      ParentShowHint = False
      ShowHint = True
      OnClick = RzToolButton6Click
    end
    object RzSpacer5: TRzSpacer
      Left = 703
      Top = 2
    end
    object RzSpacer6: TRzSpacer
      Left = 150
      Top = 2
      Width = 9
    end
    object RzToolButton7: TRzToolButton
      Left = 85
      Top = 2
      Width = 65
      SelectionColorStop = 16119543
      ImageIndex = 0
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #32534#36753
      OnClick = RzToolButton7Click
    end
    object RzSpacer7: TRzSpacer
      Left = 465
      Top = 2
    end
    object RzToolButton8: TRzToolButton
      Left = 378
      Top = 2
      Width = 87
      SelectionColorStop = 16119543
      ImageIndex = 4
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #34920#26684#35774#32622
      OnClick = RzToolButton8Click
    end
    object cxDBNavigator1: TcxDBNavigator
      Left = 619
      Top = 4
      Width = 84
      Height = 20
      Buttons.CustomButtons = <>
      Buttons.First.Visible = False
      Buttons.PriorPage.Visible = False
      Buttons.Prior.Visible = False
      Buttons.Next.Visible = False
      Buttons.NextPage.Visible = False
      Buttons.Last.Visible = False
      Buttons.Insert.Visible = False
      Buttons.Delete.Visible = False
      Buttons.Edit.Visible = False
      Buttons.Post.Visible = False
      Buttons.Cancel.Visible = False
      Buttons.Refresh.Visible = False
      Buttons.SaveBookmark.Visible = False
      Buttons.GotoBookmark.Visible = False
      Buttons.Filter.Visible = False
      DataSource = MasterSource
      InfoPanel.DisplayMask = #31532'[RecordIndex]'#39029#65292#20849'[RecordCount]'#39029
      InfoPanel.Visible = True
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
    end
  end
  object cxGroupBox1: TcxGroupBox
    Left = 0
    Top = 37
    Align = alTop
    Caption = #21512#21516#20449#24687
    Style.BorderStyle = ebsFlat
    Style.Edges = [bTop, bBottom]
    Style.LookAndFeel.NativeStyle = False
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 2
    Height = 85
    Width = 977
    object Shape2: TShape
      Left = 801
      Top = 44
      Width = 117
      Height = 34
      Brush.Style = bsClear
      Pen.Color = clRed
      Pen.Width = 3
    end
    object cxLabel1: TcxLabel
      Left = 11
      Top = 50
      Caption = #24448#26469#21333#20301#65306
    end
    object cxLabel2: TcxLabel
      Left = 241
      Top = 50
      Caption = #21512#21516#21517#31216#65306
    end
    object cxLabel4: TcxLabel
      Left = 490
      Top = 19
      Caption = #26085#12288#12288#26399#65306
    end
    object cxLabel5: TcxLabel
      Left = 490
      Top = 50
      Caption = #22791#12288#12288#27880#65306
    end
    object cxDBTextEdit3: TcxDBTextEdit
      Left = 546
      Top = 49
      DataBinding.DataField = 'remarks'
      DataBinding.DataSource = MasterSource
      ParentColor = True
      Properties.MaxLength = 255
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.Kind = lfUltraFlat
      Style.LookAndFeel.NativeStyle = False
      Style.LookAndFeel.SkinName = ''
      StyleDisabled.LookAndFeel.Kind = lfUltraFlat
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.SkinName = ''
      StyleFocused.LookAndFeel.Kind = lfUltraFlat
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.SkinName = ''
      StyleHot.LookAndFeel.Kind = lfUltraFlat
      StyleHot.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.SkinName = ''
      TabOrder = 4
      Width = 129
    end
    object cxDBDateEdit1: TcxDBDateEdit
      Left = 546
      Top = 18
      DataBinding.DataField = 'InputDate'
      DataBinding.DataSource = MasterSource
      ParentColor = True
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      Style.ButtonStyle = btsSimple
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 5
      Width = 129
    end
    object cxDBTextEdit4: TcxDBTextEdit
      Left = 748
      Top = 18
      DataBinding.DataField = 'NoId'
      DataBinding.DataSource = MasterSource
      ParentColor = True
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 6
      Width = 178
    end
    object cxLabel6: TcxLabel
      Left = 692
      Top = 19
      Caption = #32534#12288#12288#21495#65306
    end
    object cxDBCheckBox1: TcxDBCheckBox
      Left = 692
      Top = 47
      Caption = #26159#21542#20923#32467
      DataBinding.DataField = 'status'
      DataBinding.DataSource = MasterSource
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
      OnClick = cxDBCheckBox1Click
    end
    object cxLabel7: TcxLabel
      Left = 802
      Top = 45
      AutoSize = False
      Caption = #20837#24211#21333
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -21
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.TransparentBorder = True
      Style.IsFontAssigned = True
      Properties.Alignment.Horz = taCenter
      Properties.Alignment.Vert = taVCenter
      Height = 29
      Width = 112
      AnchorX = 858
      AnchorY = 60
    end
    object cxDBButtonEdit1: TcxDBButtonEdit
      Left = 305
      Top = 18
      Hint = #25910#36135#21333#20301
      DataBinding.DataField = 'StorageName'
      DataBinding.DataSource = MasterSource
      ParentColor = True
      Properties.Buttons = <
        item
          Caption = #36873#25321
          Default = True
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000004A220966AC5116F07B380FAB0000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            00004D230A69B85617FFBB5617FFAE4F16F30000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000004D24
            0A69BC5819FFBB5718FFB95318FC401D08570000000000000000000000000000
            000000000000281406368A4315B7B3571AF0B35818F0854012B466300E89BD59
            19FFBC5919FFBA5719FC401E0857000000000000000000000000000000000000
            00005A2D0F78C05F1FFFC06021FFCD8250FFCD824FFFBE5D1FFFBE5A1BFFBD5A
            19FFBB5819FC401E095700000000000000000000000000000000000000003018
            083FC0601FFCCD804CFFF7EAE2FFFFFFFFFFFFFFFFFFF7EAE2FFCA7A46FFBC5A
            1BFF622F0D84000000000000000000000000000000000000000000000000944B
            18C3C36929FFF6E9E0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E8DFFFC063
            23FF854012B4000000000000000000000000000000000000000000000000B75D
            20F0D18958FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCF84
            54FFAD541AEA000000000000000000000000000000000000000000000000B85E
            20F0D18A59FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD085
            55FFAE541AEA000000000000000000000000000000000000000000000000964D
            1BC3C5692BFFF7EBE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6E8DFFFC164
            26FF854214B40000000000000000000000000000000000000000000000003019
            093FC36525FFCF844FFFF7EBE2FFFFFFFFFFFFFFFFFFF7EBE2FFCD804CFFBF5E
            1FFF361B08480000000000000000000000000000000000000000000000000000
            00005E31127BC36625FFC46726FFCF8551FFCF8350FFC46523FFC16020FF5D2E
            0F7B000000000000000000000000000000000000000000000000000000000000
            0000000000003019093F874518B1AD591EE4AD591EE4864517B13018083F0000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000}
          Kind = bkGlyph
        end>
      Properties.OnButtonClick = cxDBButtonEdit1PropertiesButtonClick
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      Style.ButtonStyle = btsSimple
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 10
      Width = 160
    end
    object cxLabel8: TcxLabel
      Left = 241
      Top = 19
      Caption = #20179#24211#21517#31216#65306
    end
    object cxLabel9: TcxLabel
      Left = 12
      Top = 19
      Caption = #20844#21496#21517#31216#65306
    end
    object cxDBButtonEdit2: TcxDBButtonEdit
      Left = 73
      Top = 49
      DataBinding.DataField = 'SignName'
      DataBinding.DataSource = MasterSource
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = cxDBButtonEdit2PropertiesButtonClick
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Color = clBtnFace
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      Style.Shadow = False
      Style.ButtonStyle = btsSimple
      Style.ButtonTransparency = ebtNone
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 13
      Width = 162
    end
    object cxDBButtonEdit3: TcxDBButtonEdit
      Left = 305
      Top = 46
      DataBinding.DataField = 'ContractName'
      DataBinding.DataSource = MasterSource
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = cxDBButtonEdit3PropertiesButtonClick
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Color = clBtnFace
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      Style.ButtonStyle = btsSimple
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 14
      Width = 160
    end
    object DBCompany: TcxDBButtonEdit
      Left = 73
      Top = 19
      DataBinding.DataField = 'CompanyName'
      DataBinding.DataSource = MasterSource
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = DBCompanyPropertiesButtonClick
      Style.BorderColor = clBlack
      Style.BorderStyle = ebsSingle
      Style.Color = clBtnFace
      Style.Edges = [bBottom]
      Style.HotTrack = False
      Style.LookAndFeel.NativeStyle = False
      Style.ButtonStyle = btsSimple
      Style.ButtonTransparency = ebtNone
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.NativeStyle = False
      TabOrder = 15
      Width = 162
    end
  end
  object DetailedSource: TDataSource
    DataSet = Detailed
    Left = 216
    Top = 312
  end
  object Detailed: TClientDataSet
    Aggregates = <>
    CommandText = 'Select * from sk_Company_Storage_ContractDetailed WHERE 1=2'
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'NoId'
    MasterFields = 'NoId'
    MasterSource = MasterSource
    PacketRecords = 0
    Params = <>
    StoreDefs = True
    AfterInsert = DetailedAfterInsert
    Left = 216
    Top = 248
  end
  object Master: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = MasterAfterInsert
    Left = 144
    Top = 248
    object MasterSignName: TWideStringField
      FieldName = 'SignName'
      Size = 255
    end
    object MasterNoId: TWideStringField
      FieldName = 'NoId'
      Size = 255
    end
    object MasterStorageName: TWideStringField
      FieldName = 'StorageName'
      Size = 255
    end
    object Masterremarks: TWideStringField
      FieldName = 'remarks'
      Size = 255
    end
    object MasterTreeId: TWideStringField
      FieldName = 'TreeId'
      Size = 255
    end
    object MasterInputDate: TDateTimeField
      FieldName = 'InputDate'
    end
    object MasterContractName: TWideStringField
      FieldName = 'ContractName'
      Size = 255
    end
    object Masterstatus: TBooleanField
      FieldName = 'status'
    end
    object MasterModule: TIntegerField
      FieldName = 'ModuleIndex'
    end
    object MasterCompanyName: TWideStringField
      FieldName = 'CompanyName'
      Size = 255
    end
    object MasterCode: TWideStringField
      FieldName = 'Code'
      Size = 255
    end
  end
  object MasterSource: TDataSource
    DataSet = Master
    Left = 144
    Top = 312
  end
  object ClientMakings: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 56
    Top = 248
  end
  object DataMakings: TDataSource
    DataSet = ClientMakings
    Left = 56
    Top = 312
  end
  object pm: TPopupMenu
    Images = DM.cxImageList1
    Left = 488
    Top = 248
    object N10: TMenuItem
      Caption = #28155#21152#26126#32454
      OnClick = N10Click
    end
    object N1: TMenuItem
      Caption = #26032#12288#12288#22686
      ImageIndex = 37
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #30830#12288#12288#23450
      ImageIndex = 17
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = #20445#12288#12288#23384
      ImageIndex = 3
      OnClick = N3Click
    end
    object N4: TMenuItem
      Caption = #21024#12288#12288#38500
      ImageIndex = 51
      OnClick = N4Click
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object N8: TMenuItem
      Caption = #38468#12288#12288#20214
      ImageIndex = 55
      OnClick = N8Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N6: TMenuItem
      Caption = #19978#19968#39029
      ImageIndex = 29
      OnClick = N6Click
    end
    object N7: TMenuItem
      Caption = #19979#19968#39029
      ImageIndex = 31
      OnClick = N7Click
    end
  end
  object Timer1: TTimer
    Interval = 100
    OnTimer = Timer1Timer
    Left = 448
    Top = 248
  end
  object Company: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 288
    Top = 248
  end
  object CompanySource: TDataSource
    DataSet = Company
    Left = 288
    Top = 312
  end
end
