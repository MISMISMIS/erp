unit ufrmContractInputBase;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, cxNavigator, cxDBNavigator, RzButton, RzPanel,
  Vcl.ExtCtrls, cxContainer, cxEdit, cxButtonEdit, cxDBEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxDropDownEdit, cxCalendar, cxTextEdit,
  cxMaskEdit, cxLabel, cxGroupBox, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, Data.DB, cxDBData, cxCheckBox, cxSpinEdit,
  cxCurrencyEdit, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxMemo, Vcl.DBCtrls,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, Datasnap.DBClient,UnitADO,ufrmBaseController;

type
  TNavgator         = class(TDBNavigator);
  TCusDropDownEdit  = class(TcxCustomDropDownEdit);
  TfrmContractInputBase = class(TfrmBaseController)
    RzToolbar4: TRzToolbar;
    RzSpacer26: TRzSpacer;
    RzToolButton23: TRzToolButton;
    RzSpacer31: TRzSpacer;
    RzToolButton24: TRzToolButton;
    RzSpacer32: TRzSpacer;
    RzSpacer33: TRzSpacer;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzToolButton4: TRzToolButton;
    RzSpacer4: TRzSpacer;
    RzToolButton5: TRzToolButton;
    RzToolButton6: TRzToolButton;
    RzSpacer5: TRzSpacer;
    RzSpacer6: TRzSpacer;
    RzToolButton7: TRzToolButton;
    RzSpacer7: TRzSpacer;
    RzToolButton8: TRzToolButton;
    cxDBNavigator1: TcxDBNavigator;
    cxGroupBox1: TcxGroupBox;
    cxLabel7: TcxLabel;
    cxLabel1: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel9: TcxLabel;
    cxDBComboBox1: TcxDBComboBox;
    BillNumber: TcxDBTextEdit;
    cxDBDateEdit1: TcxDBDateEdit;
    DBCompany: TcxDBLookupComboBox;
    cxDBButtonEdit1: TcxDBButtonEdit;
    cxLabel2: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel10: TcxLabel;
    cxLabel11: TcxLabel;
    cxLabel12: TcxLabel;
    cxDBButtonEdit2: TcxDBButtonEdit;
    cxGroupBox2: TcxGroupBox;
    Navigator: TDBNavigator;
    cxDBCheckBox1: TcxDBCheckBox;
    cxDBTextEdit1: TcxDBTextEdit;
    cxDBTextEdit2: TcxDBTextEdit;
    cxGroupBox3: TcxGroupBox;
    cxLabel22: TcxLabel;
    cxDBComboBox7: TcxDBComboBox;
    cxLabel23: TcxLabel;
    cxDBTextEdit8: TcxDBTextEdit;
    cxLabel24: TcxLabel;
    cxDBTextEdit9: TcxDBTextEdit;
    cxLabel25: TcxLabel;
    cxDBTextEdit10: TcxDBTextEdit;
    cxLabel26: TcxLabel;
    cxDBTextEdit11: TcxDBTextEdit;
    cxLabel27: TcxLabel;
    cxDBDateEdit4: TcxDBDateEdit;
    cxDBComboBox8: TcxDBComboBox;
    cxLabel28: TcxLabel;
    cxLabel29: TcxLabel;
    cxLabel30: TcxLabel;
    cxDBLookupComboBox4: TcxDBLookupComboBox;
    cxDBLookupComboBox5: TcxDBLookupComboBox;
    cxLabel16: TcxLabel;
    RzPanel5: TRzPanel;
    RzPanel6: TRzPanel;
    cxDBTextEdit7: TcxDBTextEdit;
    RzPanel7: TRzPanel;
    RzPanel8: TRzPanel;
    cxDBCurrencyEdit1: TcxDBCurrencyEdit;
    Grid: TcxGrid;
    tView: TcxGridDBTableView;
    tViewCol1: TcxGridDBColumn;
    tViewCol2: TcxGridDBColumn;
    tViewColumn8: TcxGridDBColumn;
    tViewCol3: TcxGridDBColumn;
    tViewCol4: TcxGridDBColumn;
    tViewCol5: TcxGridDBColumn;
    tViewCol6: TcxGridDBColumn;
    tViewColumn3: TcxGridDBColumn;
    tViewColumn4: TcxGridDBColumn;
    tViewColumn6: TcxGridDBColumn;
    tViewColumn7: TcxGridDBColumn;
    tViewColumn1: TcxGridDBColumn;
    tViewColumn2: TcxGridDBColumn;
    tViewColumn5: TcxGridDBColumn;
    tViewColumn12: TcxGridDBColumn;
    tViewColumn13: TcxGridDBColumn;
    tViewCol7: TcxGridDBColumn;
    tViewColumn10: TcxGridDBColumn;
    tViewCol9: TcxGridDBColumn;
    tViewCol8: TcxGridDBColumn;
    tViewCol15: TcxGridDBColumn;
    tViewCol16: TcxGridDBColumn;
    tViewCol10: TcxGridDBColumn;
    tViewColumn14: TcxGridDBColumn;
    tViewCol11: TcxGridDBColumn;
    tViewCol12: TcxGridDBColumn;
    tViewCol13: TcxGridDBColumn;
    tViewCol14: TcxGridDBColumn;
    tRuning: TcxGridDBTableView;
    tRuningCol9: TcxGridDBColumn;
    tRuningCol1: TcxGridDBColumn;
    tRuningCol12: TcxGridDBColumn;
    tRuningCol2: TcxGridDBColumn;
    tRuningCol3: TcxGridDBColumn;
    tRuningCol4: TcxGridDBColumn;
    tRuningCol11: TcxGridDBColumn;
    tRuningCol5: TcxGridDBColumn;
    tRuningCol6: TcxGridDBColumn;
    tRuningCol7: TcxGridDBColumn;
    tRuningCol8: TcxGridDBColumn;
    tRuningCol10: TcxGridDBColumn;
    tGroupPrice: TcxGridDBTableView;
    tGroupPriceColumn1: TcxGridDBColumn;
    tGroupPriceColumn2: TcxGridDBColumn;
    tGroupPriceColumn3: TcxGridDBColumn;
    Lv: TcxGridLevel;
    Lv3: TcxGridLevel;
    Lv2: TcxGridLevel;
    Master: TClientDataSet;
    MasterSource: TDataSource;
    Company: TClientDataSet;
    CompanySource: TDataSource;
    tmr1: TTimer;
    Detailed: TClientDataSet;
    DetailedSource: TDataSource;
    DataSource1: TDataSource;
    dsRuning: TClientDataSet;
    DataGroupUnitPrice: TDataSource;
    dsGroupUnitPrice: TClientDataSet;
    procedure RzToolButton3Click(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure RzToolButton7Click(Sender: TObject);
    procedure RzToolButton6Click(Sender: TObject);
    procedure cxDBButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxDBButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure tViewCol4PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure tViewCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure MasterAfterInsert(DataSet: TDataSet);
    procedure DetailedAfterInsert(DataSet: TDataSet);
  private
    { Private declarations }
    dwADO : TADO;
    function IsFormEmpty():Boolean;
    function UpdateInfo():Boolean;
    function RuningAccount(lpFormType : Integer):Boolean;
  public
    { Public declarations }
    dwIsEdit : Boolean;
    dwModule : Integer;
    dwDetailSQLText : string;
    dwDetailedTable : string;
    dwDataType : Integer;
    dwNodeText : string;
    dwNodeId   : Integer;
    dwTreeCode : string;
    dwBillNumber : Variant;
    dwIsContrct: Boolean;

  end;

var
  frmContractInputBase: TfrmContractInputBase;

implementation

uses
   global,ufrmCompanyManage,ufrmStorageTreeView,ufrmMakings;

{$R *.dfm}


function IsDetailed(lpDataSet : TClientDataSet):Boolean;
begin
  with lpDataSet do
  begin
    if IsEmpty then
    begin
      Append ;
    end else
    begin
      First;
      Insert;
    end;
  end;
end;


procedure TfrmContractInputBase.RzToolButton1Click(Sender: TObject);
begin
  if dwIsEdit then
  begin
    IsDetailed(Self.Detailed);
    Self.Grid.SetFocus;
    Self.tViewCol4.Editing := True;
  end else
  begin
    if Navigator.DataSource.State in [ dsEdit , dsInsert] then
    begin
       IsDetailed(Self.Detailed);
       Self.Grid.SetFocus;
       Self.tViewCol4.Editing := True;
    end else
    begin
      TNavgator(Navigator).Buttons[nbInsert].Click;
      Self.DBCompany.SetFocus;
      Self.DBCompany.DroppedDown := True;
    end;
  end;
end;

procedure TfrmContractInputBase.RzToolButton3Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmContractInputBase.cxDBButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  szParent : Integer;

begin
  inherited;
  with Self.Company do
  begin
    if Locate(DBCompany.Properties.KeyFieldNames,DBCompany.Text,[]) then
      szParent := FieldByName('parent').Value;
  end;

  if szParent <> 0 then
  begin
    Application.CreateForm(TfrmStorageTreeView,frmStorageTreeView);
    try
      frmStorageTreeView.dwTableName := g_Table_Company_StorageTree;
      frmStorageTreeView.dwParent    := szParent;
      frmStorageTreeView.dwTreeName  := '公司仓库';
      frmStorageTreeView.ShowModal;

      cxDBButtonEdit1.EditValue := frmStorageTreeView.dwStorageName;
      cxDBButtonEdit1.PostEditValue;

      dwNodeId   := frmStorageTreeView.dwNodeId;
      dwTreeCode := frmStorageTreeView.dwTreeCode;
      Self.cxDBTextEdit2.EditValue := dwTreeCode;
      Self.cxDBTextEdit2.PostEditValue;
      Self.cxDBTextEdit1.EditValue := dwNodeId;
      Self.cxDBTextEdit1.PostEditValue;
      {
      with Self.Master do
      begin
        Edit;
        FieldByName('TreeId').Value := dwNodeId;
        FieldByName('Code').Value   := dwTreeCode;
      end;
      }
    finally
      frmStorageTreeView.Free;
    end;
  end else
  begin
    Application.MessageBox( '重新选择公司名称！', 'Error', MB_OKCANCEL + MB_ICONWARNING);
  end;

end;


procedure TfrmContractInputBase.cxDBButtonEdit2PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  Child: TfrmCompanyManage;
  s : Variant;
begin
  inherited;
  Child := TfrmCompanyManage.Create(nil);
  try
    Child.dwIsReadonly := True;
    Child.ShowModal;
    s := Child.dwSignName;
    if s <> null then
    begin
      (Sender as TcxDBButtonEdit).EditValue := s;
      (Sender as TcxDBButtonEdit).PostEditValue;
    end;
  finally
    Child.Free;
  end;
end;

procedure TfrmContractInputBase.DetailedAfterInsert(DataSet: TDataSet);
var
  Suffix : string;
  szConName : string;
  szCode : string;

begin
  Suffix := '000000';
  szConName := 'OddNumbers';
  szCode := GetRowCode(g_Table_Company_StorageDetailed,szConName,Suffix,970002);
  Self.tViewCol2.EditValue     := Self.cxDBCheckBox1.EditValue;
  Self.tViewColumn13.EditValue := 1;
  Self.tViewColumn10.EditValue := 1;
  with DataSet do
  begin
    FieldByName('IsContrct').Value := dwIsContrct;
    FieldByName(szConName).Value:= szCode;
    FieldByName('DataType').Value  := dwDataType;
  end;
end;

function TfrmContractInputBase.IsFormEmpty():Boolean;
var
  s : PWideChar;
  s1: PWideChar;
  s2: PWideChar;

begin
  Result := False;
  s := '新增商品记录？';
  if Self.BillNumber.EditValue = null then
  begin
    //单号不能为空
    Result := True;
    Application.MessageBox( '单号不能为空！', s, MB_OKCANCEL + MB_ICONWARNING);
    Exit;
  end else
  if Self.DBCompany.EditingValue = null then
  begin
    //公司名不能为空
    Result := True;
    Application.MessageBox( '公司名不能为空！', s, MB_OKCANCEL + MB_ICONWARNING);
    Self.DBCompany.DroppedDown := True;
    Exit;
  end else
  if Self.cxDBDateEdit1.EditingValue = null then
  begin
    //开票日期不能为空
    Result := True;
    Application.MessageBox( '开票日期不能为空！', s, MB_OKCANCEL + MB_ICONWARNING);
    Self.cxDBDateEdit1.DroppedDown := True;
    Exit;
  end else
  if Self.cxDBComboBox1.EditingValue = null then
  begin
    //结算状态不能为空
    Result := True;
    Application.MessageBox( '请选择票据类别！', s, MB_OKCANCEL + MB_ICONWARNING);
    Self.cxDBComboBox1.DroppedDown := True;
    Exit;
  end else
  if Not Self.cxDBCheckBox1.Checked then
  begin
    Result := True;
    Application.MessageBox( '冻结状态，不能编辑和新增商品！', s, MB_OKCANCEL + MB_ICONWARNING);
    Exit;
  end;

  case dwDataType of
    0:
    begin
      s1 := '发货单位不能为空！';
      s2 := '收货单位不能为空！';
    end;
    1..2:
    begin
      s1 := '收货单位不能为空！';
      s2 := '发货单位不能为空！';
    end;
  end;

  if Self.cxDBButtonEdit2.Text = '' then
  begin
    //发货单位不能为空
    Result := True;
    Application.MessageBox(s1 , s, MB_OKCANCEL + MB_ICONWARNING);
    Exit;
  end else
  if Self.cxDBButtonEdit1.Text = '' then
  begin
    //收货单位不能为空
    Result := True;
    Application.MessageBox(s2, s, MB_OKCANCEL + MB_ICONWARNING);
    Exit;
  end;

  if Self.cxDBComboBox7.ItemIndex = 1 then //交易状态
  begin
    //现金入库
    if Length(Self.cxDBLookupComboBox4.Text) = 0 then
    begin
      Result := True;
      Application.MessageBox( '请选择一个交易方式', '现金保存', MB_OKCANCEL + MB_ICONWARNING);
      Self.cxDBLookupComboBox4.DroppedDown := True;
      Exit;
    end else
    if Length(Self.cxDBLookupComboBox5.Text) = 0 then
    begin
      Result := True;
      Application.MessageBox( '请选择一个费用科目', '现金保存', MB_OKCANCEL + MB_ICONWARNING);
      Self.cxDBLookupComboBox5.DroppedDown := True;
    end;

  end;

end;

procedure TfrmContractInputBase.MasterAfterInsert(DataSet: TDataSet);
var
  szConName : string;
  Prefix : string;
  Suffix : string;
  szCode : string;
  szSignName : Variant;
begin
  inherited;
//  Prefix := 'ST-';
  Suffix := '000000';
  szConName := Self.BillNumber.DataBinding.DataField;
  szCode := Prefix + GetRowCode(g_Table_Company_StorageBillList,szConName,Suffix,970001);
  with DataSet do
  begin
    FieldByName(Self.BillNumber.DataBinding.DataField).Value := szCode;
    FieldByName(Self.cxDBDateEdit1.DataBinding.DataField).Value := Date;
    FieldByName(Self.cxDBCheckBox1.DataBinding.DataField).Value := True;
    FieldByName('IsContrct').Value := dwIsContrct;
    FieldByName('DataType').Value  := dwDataType;
  end;
  Self.DBCompany.Enabled := True;
end;

function TfrmContractInputBase.UpdateInfo():Boolean;
var
  i : Integer;
  s : Variant;
begin
  inherited;
  //更新明细主要信息
  with Self.Detailed do
  begin
    First;
    while Not Eof do
    begin
      Edit;
      FieldByName(Self.cxDBDateEdit1.DataBinding.DataField).Value    := Self.cxDBDateEdit1.EditingValue;
      FieldByName(Self.cxDBButtonEdit2.DataBinding.DataField).Value  := Self.cxDBButtonEdit2.EditingValue;
      FieldByName(Self.cxDBButtonEdit1.DataBinding.DataField).Value  := Self.cxDBButtonEdit1.EditingValue;
      FieldByName(Self.cxDBComboBox7.DataBinding.DataField).Value    := Self.cxDBComboBox7.EditingValue;
      FieldByName(Self.tViewCol3.DataBinding.FieldName).Value        := Self.BillNumber.EditingValue;
      FieldByName(Self.cxDBTextEdit1.DataBinding.DataField).Value    := Self.cxDBTextEdit1.EditingValue;
      FieldByName(Self.cxDBTextEdit2.DataBinding.DataField).Value    := Self.cxDBTextEdit2.EditingValue;

      Post;
      {
      if null = FieldByName(lpFieldName).Value then
      begin
        Delete;
        continue;
      end;
      }
      Next;
    end;
  end;
end;

function TfrmContractInputBase.RuningAccount(lpFormType : Integer):Boolean;
var
  s : string;
  szItemsIndex : Integer;
begin
  // numbers      编号
  // Receivables  收款单位
  // TradeType    交易方式
  // PaymentType  付款单位
  // ProjectName  工程/名称
  // CategoryName 费用科目
  // Payee        收款人名称
  // Drawee       付款人名称
  // SumMoney     金额
  // CapitalMoney 大写金额
  // Remarks      备注

  Result := False;
  szItemsIndex := Self.cxDBComboBox7.ItemIndex;  //交易状态
  if szItemsIndex = 1 then
  begin
    //现金入库
    case dwDataType of
      0:
      begin
        //入库
        //g_Table_RuningAccount //现金收款
        //g_RunningIndex := 2;
        lpFormType := 2;
        if Self.cxDBTextEdit8.EditingValue = null then
        begin
          //经手人

        end;

      end;
      1:
      begin
        //出库
        //g_RunningIndex := 3;
        //g_Table_RuningAccount //现金支款
        lpFormType := 3;
      end;
    end;
    if Self.cxDBLookupComboBox4.EditingValue = null then
    begin
      //交易方式
    end;

    if Self.cxDBLookupComboBox5.EditingValue = null then
    begin
      //费用科目
    end;

    with Self.dsRuning do
    begin
      if State <> dsInactive then
      begin
        if Locate(Self.tRuningCol1.DataBinding.FieldName,BillNumber.EditingValue,[]) then
          Edit
        else
          Append;

        FieldByName(Self.tRuningCol1.DataBinding.FieldName).Value := BillNumber.EditingValue;
        case dwDataType of
          0:
          begin

            s := '现金支款';
            FieldByName(Self.tRuningCol2.DataBinding.FieldName).Value := cxDBButtonEdit2.Text;
            FieldByName(Self.tRuningCol3.DataBinding.FieldName).Value := cxDBTextEdit8.Text;  //经手人
          end;
          1:
          begin
            FieldByName(Self.tRuningCol3.DataBinding.FieldName).Value := cxDBButtonEdit2.Text;
            s := '现金收款';
          end;
        end;
        FieldByName(Self.tRuningCol11.DataBinding.FieldName).Value     := s;
        FieldByName(Self.tRuningCol5.DataBinding.FieldName).Value := cxDBLookupComboBox4.Text;
        FieldByName(Self.tRuningCol4.DataBinding.FieldName).Value := cxDBButtonEdit1.Text;
        FieldByName(Self.tRuningCol6.DataBinding.FieldName).Value := cxDBLookupComboBox5.Text;
        FieldByName(Self.tRuningCol7.DataBinding.FieldName).Value := cxDBCurrencyEdit1.EditingValue;
        FieldByName(Self.tRuningCol8.DataBinding.FieldName).Value := cxDBTextEdit7.EditingValue;
        FieldByName(Self.tRuningCol10.DataBinding.FieldName).Value:= lpFormType;
        FieldByName(Self.tRuningCol9.DataBinding.FieldName).Value := True;
        FieldByName(Self.tRuningCol12.DataBinding.FieldName).Value:= cxDBDateEdit1.EditingValue;
        Post;
      end;
    end;

  end;

end;



procedure TfrmContractInputBase.RzToolButton6Click(Sender: TObject);
begin

  if Not IsFormEmpty then
  begin
    //删除空行
    dwADO.DeleteRowEmpty(Detailed,tViewCol4.DataBinding.FieldName);
    //更新现金
    RuningAccount(0);
    //更新信息
    UpdateInfo;
    //提交PoST
    with Self.Navigator do
    begin
      if DataSource.State in [dsEdit , dsInsert] then
      begin
        TNavgator(Navigator).Buttons[nbPost].Click;
      end;
    end;

  end;

end;

procedure TfrmContractInputBase.RzToolButton7Click(Sender: TObject);
begin
  if Self.cxDBCheckBox1.EditValue = False then
  begin
    ShowMessage('主合同冻结状态商品明细禁止编辑!');
    Exit;
  end;
  if dwIsEdit then
    dwADO.ADOIsEdit(Self.Master);
  dwADO.ADOIsEdit(Self.Detailed);
end;

procedure TfrmContractInputBase.tmr1Timer(Sender: TObject);
var
  s : string;
begin
  tmr1.Enabled := False;
  dwADO.OpenSQL(Self.Company, 'Select * from ' + g_Table_Company_StorageTree + ' Where PID=0'); //读取公司
  //进销合同管理
  if dwIsEdit then
    s := ' Where ' + Self.tViewCol3.DataBinding.FieldName + '="' + dwBillNumber + '"'
  else
    s := ' Where 1=2';

  dwADO.OpenSQL(Self.Master, 'Select * from ' + g_Table_Company_StorageBillList  + s  );
  dwDetailSQLText := 'Select * from ' + g_Table_Company_StorageDetailed  + s;
  dwADO.OpenSQL(Self.Detailed,dwDetailSQLText);

end;

procedure TfrmContractInputBase.tViewCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmContractInputBase.tViewCol4PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  Child : TfrmMakings;
begin
  inherited;
  Child := TfrmMakings.Create(nil);
  try
    Child.dwParentHandle := Handle;
    Child.dwIsReadonly   := True;
    Child.ShowModal;
  finally
    Child.Free;
  end;
end;


end.
