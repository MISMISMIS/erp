unit ufrmCustomerCostCalc;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxLabel, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxDBEdit, cxCurrencyEdit, Vcl.Menus, Vcl.StdCtrls,
  cxButtons, cxSpinEdit,ufrmBaseController;

type
  TfrmCustomerCostCalc = class(TfrmBaseController)
    cxLabel1: TcxLabel;
    cxDBComboBox1: TcxDBComboBox;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxDBCurrencyEdit2: TcxDBCurrencyEdit;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxDBSpinEdit1: TcxDBSpinEdit;
    cxDBCurrencyEdit1: TcxDBCurrencyEdit;
    procedure cxButton2Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxDBComboBox1PropertiesChange(Sender: TObject);
    procedure cxDBSpinEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBSpinEdit1PropertiesChange(Sender: TObject);
  private
    { Private declarations }
    dwTmpSummary : Currency;
    procedure GetSummary(lpIndex : Byte);
    procedure CostCalc();
  public
    { Public declarations }
    dwGroupName : string;
    dwGroupNameA: string;

    dwMainMaterialTotal : Currency; //主材
    dwManpowerSummary : Currency;   //人工
    dwDirectFee : Currency; //直接费
    dwExtrasSummary : Currency; //费用
    dwAfterDismantSummary : Currency; //当前选中的金额
    dwAcreage : Double; //面积

  end;

var
  frmCustomerCostCalc: TfrmCustomerCostCalc;

implementation

{$R *.dfm}

uses
   uDataModule;

procedure TfrmCustomerCostCalc.CostCalc();
var
  s : string;
  lvSummary : Currency;
  lvDisplayValue: Variant;

begin
  lvDisplayValue:= Self.cxDBSpinEdit1.Value;
  if lvDisplayValue <> null then
  begin
    s := VarToStr(lvDisplayValue);
    if Pos('%',cxLabel3.Caption) <> 0 then
      lvSummary :=  (dwTmpSummary / 100) * StrToCurrDef(s,0)
    else
      lvSummary :=  dwTmpSummary * StrToCurrDef(s,0);

    Self.cxDBCurrencyEdit2.EditValue := lvSummary;
    Self.cxDBCurrencyEdit1.EditValue := lvSummary;
  end;
end;

procedure TfrmCustomerCostCalc.cxButton1Click(Sender: TObject);
begin
  Self.cxDBCurrencyEdit1.PostEditValue;
  Self.cxDBCurrencyEdit2.PostEditValue;
  Self.cxDBComboBox1.PostEditValue;
  Self.cxDBSpinEdit1.PostEditValue;
  CostCalc;
  Close;
end;

procedure TfrmCustomerCostCalc.cxButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmCustomerCostCalc.cxDBComboBox1PropertiesChange(Sender: TObject);
var s : string;
begin
  s := VarToStr( cxDBComboBox1.EditValue );
//  ShowMessage(s);
  {
  if s = dwGroupNameA then
  begin
    GetSummary(1);
  end else
  begin
    case cxDBComboBox1.ItemIndex of
      0: GetSummary(2);
      1: GetSummary(3);
    end;
  end;

  }
  if s = g_AutoCalcParameter[0] then  //税金
  begin
    GetSummary(1);
  end else
  if s = g_AutoCalcParameter[1] then  //直接费
  begin
    GetSummary(2);
  end else
  if s = g_AutoCalcParameter[2] then  //面积
  begin
    GetSummary(3);
  end;

end;

procedure TfrmCustomerCostCalc.cxDBSpinEdit1PropertiesChange(Sender: TObject);
begin
  inherited;
  CostCalc;
end;

procedure TfrmCustomerCostCalc.cxDBSpinEdit1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  TcxDBSpinEdit(Sender).Value := DisplayValue;
  CostCalc;
end;

procedure TfrmCustomerCostCalc.GetSummary(lpIndex : Byte);
begin
  case lpIndex of
    1:
    begin
      dwTmpSummary  := dwDirectFee + dwManpowerSummary + ( dwExtrasSummary - dwAfterDismantSummary ); //税金
      {
      ShowMessage(CurrToStr(dwTmpSummary) + ' - ' + CurrToStr( dwExtrasSummary - dwAfterDismantSummary ) +  ' - ' +
                  CurrToStr( dwExtrasSummary ) + ' - ' +
                  CurrToStr( dwAfterDismantSummary ) + ' - ' + CurrToStr(dwManpowerSummary));
      }
    end;
    2: dwTmpSummary  := dwDirectFee + dwManpowerSummary; //直接费总额
    3:
    begin//面积
      dwTmpSummary := dwAcreage;
      cxLabel4.Caption := Format('(%2.2f㎡)',[ dwTmpSummary ]);
      cxLabel3.Caption := '元 = ';
      Exit;
    end;
  end;
  {
  ShowMessage(CurrToStr(dwTmpSummary) + ' lpIndex:' + IntToStr(lpIndex));
  }
  cxLabel3.Caption := '% = ';
  cxLabel4.Caption := Format('(%2.2m)',[ dwTmpSummary ]);
end;

procedure TfrmCustomerCostCalc.FormActivate(Sender: TObject);
var
  lvTmpSummary : Currency;
  H : TcxHandle;
begin
  inherited;
//税金（三个加起来的费用*百分数）
//客户的直接费 + 人工费 = 工程直接费的总和
//工程直接费的总额 (不含主材 含人工 不包含费用)
  dwGroupNameA := g_AutoCalcParameter[0];
  if dwGroupName = dwGroupNameA then
  begin
    //税金(人工费 + 直接费 + 费用)
    Self.cxDBComboBox1.Properties.Items.Add(dwGroupNameA);
    Self.cxDBComboBox1.ItemIndex := 0;

  end else
  begin
    Self.cxDBComboBox1.Properties.Items.Add( g_AutoCalcParameter[1] ); //(不含费用组)
  //  Self.cxDBComboBox1.Properties.Items.Add('工程直接费的总额(加人工和主材费)');
    Self.cxDBComboBox1.Properties.Items.Add( g_AutoCalcParameter[2] ); //(面积*金额)
  //  cxDBComboBox1PropertiesChange(Sender);
  end;

end;

end.
