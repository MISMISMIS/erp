unit ufrmPostPact;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Mask, RzEdit, ComCtrls, RzButton, StdCtrls,
  UtilsTree,  //树管理
  FillThrdTree, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  cxTextEdit, cxMemo, dxCore, cxDateUtils, cxMaskEdit, cxDropDownEdit,
  cxCalendar, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, Data.DB,
  Data.Win.ADODB;

type
  TfrmPostPact = class(TForm)
    cxMemo1: TcxMemo;
    cxDateEdit1: TcxDateEdit;
    Label9: TLabel;
    Label12: TLabel;
    Label10: TLabel;
    RzBitBtn1: TRzBitBtn;
    RzBitBtn2: TRzBitBtn;
    qry: TADOQuery;
    ds: TDataSource;
    cxLookupComboBox1: TcxLookupComboBox;
    procedure RzBitBtn1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure RzBitBtn2Click(Sender: TObject);
    procedure RzBitBtn1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    g_ProjectDir : string;
    g_PatTree : TNewUtilsTree;

    //g_PatTree  : TTreeUtils;
    g_IsModify : Boolean;
    g_PostCode : string;
    g_TableName: string;
    g_Prefix   : string;
    g_Suffix   : string;
    g_TreeNode : TTreeNode;
    g_IsOnly : Boolean;
  end;

var
  frmPostPact: TfrmPostPact;

implementation

uses
   uDataModule,global;

{$R *.dfm}

procedure TfrmPostPact.FormActivate(Sender: TObject);
begin
  if not g_IsModify then
  begin
    g_PostCode := GetRowCode(g_TableName,'Code','0001',20000);// DM.getDataMaxDate(g_TableName);
  end;
  g_PostCode := g_Prefix + g_PostCode + g_Suffix;
end;

procedure TfrmPostPact.FormCreate(Sender: TObject);
begin
  with Self.qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' +  g_Table_Maintain_Project;
    Open;
  end;

end;

procedure TfrmPostPact.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    13:
    begin
      if Self.ActiveControl <> Self.cxLookupComboBox1 then
      begin
        Self.RzBitBtn1.Click;
      end;

    end;
    27:
    begin
      Close;
    end;
  end;
end;

procedure TfrmPostPact.RzBitBtn1Click(Sender: TObject);
var
  szCaption:string;
  szRemarks:string;
  i : Integer;

begin

  if Length(g_PostCode) = 0 then
  begin
    Application.MessageBox('请输入合同编号!',m_title,MB_OK + MB_ICONQUESTION)
  end
  else
  begin

    szCaption := Self.cxLookupComboBox1.Text;

    with DM.Qry do
    begin
      {
      Close;
      SQL.Clear;
      SQL.Text := 'Select * from ' +  g_Table_Maintain_Project + ' WHERE ProjectName="' + szCaption + '"';
      Open  ;

      if RecordCount <> 0 then
      begin
      end else
      begin
        ShowMessage('dddd');
      end;
      }
        if Length(szCaption) = 0 then
        begin
          Application.MessageBox('请输入合同名称!',m_title,MB_OK + MB_ICONQUESTION)
        end
        else
        begin
          if g_IsOnly then
          begin
            Close;
            SQL.Clear;
            SQL.Text := 'Select * from ' +  g_TableName + ' WHERE SignName="' + szCaption + '"';
            Open;
            if (RecordCount <> 0)  then
            begin
              Application.MessageBox('此工程名称已存在!',m_title,MB_OK + MB_ICONQUESTION);
              Exit;
            end;

          end;

            szRemarks := Self.cxMemo1.Text;
            if g_IsModify then
            begin
              //编辑
              if g_PatTree.ModifyNodeData(szCaption,g_PostCode,Self.cxDateEdit1.Date, szRemarks,g_TreeNode) then
                 Application.MessageBox('修改成功!',m_title,MB_OK + MB_ICONQUESTION)
              else
                 Application.MessageBox('修改失败!',m_title,MB_OK + MB_ICONQUESTION)

            end else
            begin
              //新增
              i := DM.SelectCode(g_TableName,g_PostCode);
              if i <> 0 then
              begin
                Application.MessageBox('工程编号已存在',m_title,MB_OK + MB_ICONQUESTION) ;
              end else
              begin

                if g_PatTree.AddChildNode( g_PostCode,szRemarks,Self.cxDateEdit1.Date,szCaption) then
                begin
                  Self.FormActivate(Sender);
                  Application.MessageBox('新增成功!',m_title,MB_OK + MB_ICONQUESTION)
                end
                else
                  Application.MessageBox('新增失败!',m_title,MB_OK + MB_ICONQUESTION)

              end;

            end;


        end;
    end;

  end;

end;

procedure TfrmPostPact.RzBitBtn1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then
  begin
    Self.RzBitBtn1.Click;
  end;
end;

procedure TfrmPostPact.RzBitBtn2Click(Sender: TObject);
begin
  Close;
end;

end.
