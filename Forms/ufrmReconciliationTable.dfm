object frmReconciliationTable: TfrmReconciliationTable
  Left = 0
  Top = 0
  Caption = #28165#31639#23545#24080#34920
  ClientHeight = 724
  ClientWidth = 1494
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Visible = True
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter2: TSplitter
    Left = 250
    Top = 30
    Width = 10
    Height = 694
    Color = 16250613
    ParentColor = False
    ResizeStyle = rsUpdate
    ExplicitLeft = 219
    ExplicitTop = 35
    ExplicitHeight = 590
  end
  object Splitter4: TSplitter
    Left = 1054
    Top = 30
    Width = 10
    Height = 694
    Align = alRight
    Color = 16250613
    ParentColor = False
    ResizeStyle = rsUpdate
    ExplicitLeft = 1146
    ExplicitTop = 22
    ExplicitHeight = 621
  end
  object RzPanel34: TRzPanel
    Left = 0
    Top = 0
    Width = 1494
    Height = 30
    Align = alTop
    Alignment = taLeftJustify
    BorderInner = fsFlat
    BorderOuter = fsNone
    BorderSides = [sdRight, sdBottom]
    BorderColor = clTeal
    BorderHighlight = clBtnFace
    BorderShadow = clFuchsia
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnResize = RzPanel34Resize
    object RzPanel3: TRzPanel
      Left = 249
      Top = 0
      Width = 1244
      Height = 29
      Align = alClient
      BorderOuter = fsNone
      BorderSides = [sdRight, sdBottom]
      BorderColor = clSilver
      BorderHighlight = clSilver
      BorderShadow = clMoneyGreen
      Caption = #24448#26469#21333#20301
      FlatColor = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object RzPanel4: TRzPanel
      Left = 0
      Top = 0
      Width = 249
      Height = 29
      Align = alLeft
      Anchors = [akLeft, akTop, akRight, akBottom]
      BorderOuter = fsNone
      BorderSides = [sdRight, sdBottom]
      BorderHighlight = clBtnFace
      BorderShadow = clBtnFace
      Caption = #28165#31639#23545#24080#34920
      FlatColor = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHighlight
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
  object RzPanel1: TRzPanel
    Left = 1064
    Top = 30
    Width = 430
    Height = 694
    Align = alRight
    BorderOuter = fsFlat
    BorderSides = [sdLeft, sdRight, sdBottom]
    TabOrder = 1
    object DetGrid: TcxGrid
      Left = 1
      Top = 29
      Width = 428
      Height = 664
      Align = alClient
      TabOrder = 0
      object tvDet: TcxGridDBTableView
        PopupMenu = pmDet
        Navigator.Buttons.CustomButtons = <>
        OnEditing = tvDetEditing
        DataController.DataSource = DataDet
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.InvertSelect = False
        OptionsSelection.MultiSelect = True
        OptionsSelection.CellMultiSelect = True
        OptionsView.NoDataToDisplayInfoText = '<'#27809#26377#20219#20309#20538#21153#25968#25454'>'
        OptionsView.DataRowHeight = 28
        OptionsView.GroupByBox = False
        OptionsView.HeaderHeight = 23
        OptionsView.Indicator = True
        OptionsView.IndicatorWidth = 14
        Styles.OnGetContentStyle = tvDetStylesGetContentStyle
        object DetCol1: TcxGridDBColumn
          Caption = #24207#21495
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taCenter
          OnCustomDrawCell = DetCol1CustomDrawCell
          OnGetDisplayText = DetCol1GetDisplayText
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Focusing = False
          Width = 40
        end
        object DetCol2: TcxGridDBColumn
          Caption = #20538#21153#26085#26399
          DataBinding.FieldName = 'detDate'
          PropertiesClassName = 'TcxDateEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.DisplayFormat = 'yyyy-mm-dd'
          Properties.EditFormat = 'yyyy-mm-dd'
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Focusing = False
          Options.Sorting = False
          Width = 84
        end
        object DetCol5: TcxGridDBColumn
          Caption = #24448#26469#21333#20301
          DataBinding.FieldName = 'SignName'
          Width = 89
        end
        object DetCol3: TcxGridDBColumn
          Caption = #20538#21153#37329#39069
          DataBinding.FieldName = 'detSumMoney'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Properties.Alignment.Horz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Focusing = False
          Options.Sorting = False
          Width = 126
        end
        object DetCol4: TcxGridDBColumn
          Caption = #20538#21153#31867#22411
          DataBinding.FieldName = 'detType'
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taCenter
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Focusing = False
          Options.Sorting = False
          Width = 60
        end
      end
      object LayoutView: TcxGridDBLayoutView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = DataDet
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        object LayoutView1: TcxGridDBLayoutViewItem
          Caption = #24207#21495
          OnGetDisplayText = LayoutView1GetDisplayText
          LayoutItem = LayoutViewLayoutItem1
        end
        object LayoutView2: TcxGridDBLayoutViewItem
          Caption = #20538#21153#26085#26399
          DataBinding.FieldName = 'detDate'
          LayoutItem = LayoutViewLayoutItem2
        end
        object LayoutView3: TcxGridDBLayoutViewItem
          Caption = #20538#21153#37329#39069
          DataBinding.FieldName = 'detSumMoney'
          LayoutItem = LayoutViewLayoutItem3
        end
        object LayoutView4: TcxGridDBLayoutViewItem
          Caption = #20538#21153#31867#22411
          DataBinding.FieldName = 'detType'
          LayoutItem = LayoutViewLayoutItem4
        end
        object LayoutViewGroup_Root: TdxLayoutGroup
          AlignHorz = ahLeft
          AlignVert = avTop
          CaptionOptions.Text = #27169#26495#21345
          ButtonOptions.Buttons = <>
          Hidden = True
          ShowBorder = False
          Index = -1
        end
        object LayoutViewLayoutItem1: TcxGridLayoutItem
          Parent = LayoutViewGroup_Root
          SizeOptions.Height = 75
          SizeOptions.Width = 317
          Index = 0
        end
        object LayoutViewLayoutItem2: TcxGridLayoutItem
          Parent = LayoutViewGroup_Root
          SizeOptions.Height = 65
          Index = 1
        end
        object LayoutViewLayoutItem3: TcxGridLayoutItem
          Parent = LayoutViewGroup_Root
          SizeOptions.Height = 66
          Index = 2
        end
        object LayoutViewLayoutItem4: TcxGridLayoutItem
          Parent = LayoutViewGroup_Root
          SizeOptions.Height = 76
          Index = 3
        end
      end
      object DetLv: TcxGridLevel
        GridView = tvDet
      end
    end
    object RzToolbar4: TRzToolbar
      Left = 1
      Top = 0
      Width = 428
      Height = 29
      AutoStyle = False
      Images = DM.cxImageList1
      BorderInner = fsNone
      BorderOuter = fsNone
      BorderSides = [sdTop]
      BorderWidth = 0
      Caption = #28165#31639
      GradientColorStyle = gcsCustom
      TabOrder = 1
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer17
        cxLabel5
        cxComboBox1
        RzSpacer18
        RzToolButton8
        RzSpacer12
        RzToolButton10)
      object RzSpacer17: TRzSpacer
        Left = 4
        Top = 2
      end
      object RzSpacer18: TRzSpacer
        Left = 161
        Top = 2
      end
      object RzToolButton8: TRzToolButton
        Left = 169
        Top = 2
        Width = 70
        SelectionColorStop = 16119543
        DropDownMenu = pmDetReport
        ImageIndex = 5
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        ToolStyle = tsDropDown
        Caption = #25253#34920
      end
      object RzSpacer12: TRzSpacer
        Left = 239
        Top = 2
      end
      object RzToolButton10: TRzToolButton
        Left = 247
        Top = 2
        Width = 52
        ImageIndex = 51
        ShowCaption = True
        UseToolbarShowCaption = False
        Caption = #21024#38500
        Visible = False
      end
      object cxLabel5: TcxLabel
        Left = 12
        Top = 6
        Caption = #20538#21153#31867#22411#65306
        Transparent = True
      end
      object cxComboBox1: TcxComboBox
        Left = 76
        Top = 4
        Properties.DropDownListStyle = lsFixedList
        Properties.Items.Strings = (
          #25910
          #25903)
        Properties.ReadOnly = False
        Properties.OnCloseUp = cxComboBox1PropertiesCloseUp
        TabOrder = 1
        Width = 85
      end
    end
  end
  object RzPanel2: TRzPanel
    Left = 260
    Top = 30
    Width = 794
    Height = 694
    Align = alClient
    BorderOuter = fsFlat
    BorderSides = [sdLeft, sdRight, sdBottom]
    TabOrder = 2
    object RzPageControl2: TRzPageControl
      Left = 1
      Top = 30
      Width = 792
      Height = 663
      Hint = ''
      ActivePage = Tab2
      Align = alClient
      BackgroundColor = clBtnFace
      BoldCurrentTab = True
      ButtonColor = 16250613
      Color = clWhite
      UseColoredTabs = True
      HotTrackStyle = htsText
      ParentBackgroundColor = False
      ParentColor = False
      ShowShadow = False
      TabOverlap = -4
      TabHeight = 26
      TabIndex = 0
      TabOrder = 0
      TabStyle = tsSquareCorners
      TabWidth = 80
      FixedDimension = 26
      object Tab1: TRzTabSheet
        Color = clWhite
        TabVisible = False
        Caption = #36827#38144#31649#29702
      end
      object Tab2: TRzTabSheet
        Color = 16250613
        Caption = #20538#21069#27719#24635
        ParentShowHint = False
        ShowHint = False
        object RzToolbar1: TRzToolbar
          Left = 0
          Top = 0
          Width = 790
          Height = 29
          AutoStyle = False
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsNone
          BorderSides = [sdTop]
          BorderWidth = 0
          Caption = #28165#31639
          GradientColorStyle = gcsCustom
          TabOrder = 0
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer4
            RzToolButton2
            RzSpacer2
            RzToolButton5
            RzSpacer10
            RzToolButton9
            RzSpacer6
            cxLabel6
            cxDateEdit1
            RzSpacer3
            cxLabel8
            cxDateEdit2
            RzSpacer8
            RzToolButton3
            RzSpacer9
            RzToolButton6)
          object RzSpacer4: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzToolButton5: TRzToolButton
            Left = 80
            Top = 2
            Width = 75
            SelectionColorStop = 16119543
            DropDownMenu = pmSumReport
            ImageIndex = 5
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #25253#34920
          end
          object RzSpacer6: TRzSpacer
            Left = 238
            Top = 2
          end
          object RzToolButton6: TRzToolButton
            Left = 603
            Top = 2
            Width = 75
            SelectionColorStop = 16119543
            ImageIndex = 8
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #36864#20986
            OnClick = RzToolButton6Click
          end
          object RzSpacer10: TRzSpacer
            Left = 155
            Top = 2
            Visible = False
          end
          object RzToolButton9: TRzToolButton
            Left = 163
            Top = 2
            Width = 75
            SelectionColorStop = 16119543
            ImageIndex = 12
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #28165#31639
            Enabled = False
            OnClick = RzToolButton9Click
          end
          object RzSpacer2: TRzSpacer
            Left = 72
            Top = 2
          end
          object RzToolButton2: TRzToolButton
            Left = 12
            Top = 2
            Width = 60
            SelectionColorStop = 16119543
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #20840#37096#27719#24635
            Enabled = False
            OnClick = RzToolButton2Click
          end
          object RzSpacer3: TRzSpacer
            Left = 400
            Top = 2
            Visible = False
          end
          object RzSpacer8: TRzSpacer
            Left = 562
            Top = 2
            Visible = False
          end
          object RzToolButton3: TRzToolButton
            Left = 570
            Top = 2
            SelectionColorStop = 16119543
            ImageIndex = 59
            Visible = False
            OnClick = RzToolButton3Click
          end
          object RzSpacer9: TRzSpacer
            Left = 595
            Top = 2
          end
          object cxLabel6: TcxLabel
            Left = 246
            Top = 6
            Caption = #24320#22987#26085#26399#65306
            Transparent = True
            Visible = False
          end
          object cxDateEdit1: TcxDateEdit
            Left = 310
            Top = 4
            TabOrder = 1
            Visible = False
            Width = 90
          end
          object cxLabel8: TcxLabel
            Left = 408
            Top = 6
            Caption = #32467#26463#26085#26399#65306
            Transparent = True
            Visible = False
          end
          object cxDateEdit2: TcxDateEdit
            Left = 472
            Top = 4
            TabOrder = 3
            Visible = False
            Width = 90
          end
        end
        object Grid: TcxGrid
          Left = 0
          Top = 29
          Width = 790
          Height = 577
          Touch.InteractiveGestures = [igZoom, igPan, igRotate, igTwoFingerTap, igPressAndTap]
          Align = alClient
          TabOrder = 1
          LockedStateImageOptions.AssignedValues = [lsiavColor]
          LockedStateImageOptions.Color = clBlack
          LockedStateImageOptions.Effect = lsieDark
          LockedStateImageOptions.ShowText = True
          object tvGrid: TcxGridDBTableView
            PopupMenu = pmRigth
            OnKeyDown = tvGridKeyDown
            Navigator.Buttons.CustomButtons = <>
            OnCustomDrawCell = tvGridCustomDrawCell
            OnEditing = tvGridEditing
            DataController.DataSource = DataSumInfo
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvGridColumn5
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvGridColumn6
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvGridColumn7
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvGridColumn8
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvGridColumn10
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvGridColumn13
              end
              item
                Kind = skCount
                Column = tvGridColumn14
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #21333#20987#27492#22788#23450#20041#31579#36873#22120
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.FocusFirstCellOnNewRecord = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsBehavior.FocusCellOnCycle = True
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.NoDataToDisplayInfoText = '<'#26080#21487#26174#31034#30340#25968#25454'>'
            OptionsView.DataRowHeight = 23
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 23
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 14
            object tvGridColumn1: TcxGridDBColumn
              Caption = #24207#21495
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              OnGetDisplayText = tvGridColumn1GetDisplayText
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Options.Filtering = False
              Options.FilteringAddValueItems = False
              Options.Sorting = False
              Width = 40
            end
            object tvGridColumn2: TcxGridDBColumn
              Caption = #23545#24080#26085#26399
              DataBinding.FieldName = 'InputDate'
              PropertiesClassName = 'TcxDateEditProperties'
              Properties.DisplayFormat = 'yyyy-mm-dd'
              Properties.EditFormat = 'yyyy-mm-dd'
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 80
            end
            object tvGridColumn3: TcxGridDBColumn
              Caption = #24448#26469#21333#20301
              DataBinding.FieldName = 'SignName'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Options.Filtering = False
              Options.Sorting = False
              Width = 129
            end
            object tvGridColumn4: TcxGridDBColumn
              Caption = #24402#32435#20998#31867
              DataBinding.FieldName = 'InduceType'
              PropertiesClassName = 'TcxComboBoxProperties'
              Properties.Items.Strings = (
                #25104#26412)
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object tvGridColumn5: TcxGridDBColumn
              Caption = #24080#30446#24635#20215
              DataBinding.FieldName = 'SumMoney'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              OnCustomDrawCell = tvGridColumn5CustomDrawCell
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Options.Filtering = False
              Options.Sorting = False
              Width = 113
            end
            object tvGridColumn6: TcxGridDBColumn
              Caption = #24050#20184#37329#39069
              DataBinding.FieldName = 'PaidMoney'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Options.Filtering = False
              Options.Sorting = False
              Styles.Content = DM.cxStyle14
              Width = 115
            end
            object tvGridColumn7: TcxGridDBColumn
              Caption = #25187#38500
              DataBinding.FieldName = 'DeductMoney'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              OnCustomDrawCell = tvGridColumn7CustomDrawCell
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Options.Filtering = False
              Options.Sorting = False
              Width = 94
            end
            object tvGridColumn8: TcxGridDBColumn
              Caption = #22686#22870
              DataBinding.FieldName = 'RewardMoney'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Options.Filtering = False
              Options.Sorting = False
              Styles.Content = DM.cxStyle207
              Width = 96
            end
            object tvGridColumn9: TcxGridDBColumn
              Caption = #27424#25454#26085#26399
              DataBinding.FieldName = 'DocumentDate'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 60
            end
            object tvGridColumn10: TcxGridDBColumn
              Caption = #27424#25454#37329#39069
              DataBinding.FieldName = 'DocumentMonery'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Visible = False
              OnCustomDrawCell = tvGridColumn10CustomDrawCell
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object tvGridColumn11: TcxGridDBColumn
              Caption = #23545#24080#29366#24577
              DataBinding.FieldName = 'CashAccountStatus'
              PropertiesClassName = 'TcxComboBoxProperties'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 60
            end
            object tvGridColumn12: TcxGridDBColumn
              Caption = #28165#24080#29366#24577
              DataBinding.FieldName = 'ClearingStatus'
              PropertiesClassName = 'TcxComboBoxProperties'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 60
            end
            object tvGridColumn13: TcxGridDBColumn
              Caption = #28165#31639#20313#27454
              DataBinding.FieldName = 'ClearBalance'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.OnValidate = tvGridColumn13PropertiesValidate
              OnCustomDrawCell = tvGridColumn10CustomDrawCell
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Styles.Content = DM.cxStyle36
              Width = 112
            end
            object tvGridColumn14: TcxGridDBColumn
              Caption = #28165#31639#20313#27454#22823#20889
              DataBinding.FieldName = 'ClearBalanceCapital'
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.OnValidate = tvGridColumn14PropertiesValidate
              Visible = False
              HeaderAlignmentHorz = taCenter
              Width = 280
            end
            object tvGridColumn15: TcxGridDBColumn
              Caption = #24080#30446#24635#20215#27604#29575
              DataBinding.FieldName = 'ratio'
              PropertiesClassName = 'TcxSpinEditProperties'
              Properties.DisplayFormat = '0.##%;-,0.##%'
              Properties.EditFormat = '0.##%;-,0.##%'
              Properties.ValueType = vtFloat
              Options.Editing = False
              Width = 78
            end
          end
          object Lv: TcxGridLevel
            GridView = tvGrid
          end
        end
        object RzPanel6: TRzPanel
          Left = 0
          Top = 606
          Width = 790
          Height = 26
          Align = alBottom
          BorderOuter = fsFlat
          BorderSides = [sdTop, sdBottom]
          TabOrder = 2
          OnResize = RzPanel6Resize
          object RzPanel7: TRzPanel
            Left = 626
            Top = 1
            Width = 208
            Height = 24
            Align = alLeft
            BorderOuter = fsFlat
            BorderSides = []
            TabOrder = 0
          end
          object RzPanel8: TRzPanel
            Left = 556
            Top = 1
            Width = 70
            Height = 24
            Align = alLeft
            BorderOuter = fsFlat
            BorderSides = [sdRight]
            Caption = #28165#31639#20313#39069':'
            TabOrder = 1
          end
          object RzPanel9: TRzPanel
            Left = 278
            Top = 1
            Width = 70
            Height = 24
            Align = alLeft
            BorderOuter = fsFlat
            BorderSides = [sdRight]
            Caption = #24050#20184#37329#39069':'
            TabOrder = 2
          end
          object RzPanel10: TRzPanel
            Left = 348
            Top = 1
            Width = 208
            Height = 24
            Align = alLeft
            BorderOuter = fsFlat
            BorderSides = [sdRight]
            TabOrder = 3
          end
          object RzPanel11: TRzPanel
            Left = 0
            Top = 1
            Width = 70
            Height = 24
            Align = alLeft
            BorderOuter = fsFlat
            BorderSides = [sdRight]
            Caption = #24080#30446#24635#20215':'
            TabOrder = 4
          end
          object RzPanel12: TRzPanel
            Left = 70
            Top = 1
            Width = 208
            Height = 24
            Align = alLeft
            BorderOuter = fsFlat
            BorderSides = [sdRight]
            TabOrder = 5
          end
        end
      end
      object TabSheet1: TRzTabSheet
        Color = clWhite
        Caption = #27719#24635#22270#34920
        object DBChart1: TDBChart
          Left = 0
          Top = 0
          Width = 790
          Height = 632
          Border.Visible = True
          Title.ClipText = False
          Title.Text.Strings = (
            #24080#30446#24635#20215)
          Title.Visible = False
          Legend.Visible = False
          Panning.MouseWheel = pmwNone
          View3DOptions.Elevation = 315
          View3DOptions.Orthogonal = False
          View3DOptions.Perspective = 0
          View3DOptions.Rotation = 360
          ZoomWheel = pmwNormal
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          DefaultCanvas = 'TGDIPlusCanvas'
          PrintMargins = (
            15
            22
            15
            22)
          ColorPaletteIndex = 9
          object Button2: TButton
            Left = 568
            Top = 16
            Width = 75
            Height = 25
            Caption = 'Button2'
            TabOrder = 0
            Visible = False
            OnClick = Button2Click
          end
          object Series2: TPieSeries
            Marks.Frame.Visible = False
            Marks.Style = smsLabelPercentTotal
            Marks.Callout.Length = 20
            DataSource = ADOSumInfo
            Title = #24080#30446#24635#20215
            XLabelsSource = 'SignName'
            XValues.Order = loAscending
            YValues.Name = 'Pie'
            YValues.Order = loNone
            YValues.ValueSource = 'SumMoney'
            Frame.InnerBrush.BackColor = clRed
            Frame.InnerBrush.Gradient.EndColor = clGray
            Frame.InnerBrush.Gradient.MidColor = clWhite
            Frame.InnerBrush.Gradient.StartColor = 4210752
            Frame.InnerBrush.Gradient.Visible = True
            Frame.MiddleBrush.BackColor = clYellow
            Frame.MiddleBrush.Gradient.EndColor = 8553090
            Frame.MiddleBrush.Gradient.MidColor = clWhite
            Frame.MiddleBrush.Gradient.StartColor = clGray
            Frame.MiddleBrush.Gradient.Visible = True
            Frame.OuterBrush.BackColor = clGreen
            Frame.OuterBrush.Gradient.EndColor = 4210752
            Frame.OuterBrush.Gradient.MidColor = clWhite
            Frame.OuterBrush.Gradient.StartColor = clSilver
            Frame.OuterBrush.Gradient.Visible = True
            Frame.Width = 4
            Shadow.Color = clWhite
            Gradient.MidColor = clWhite
            OtherSlice.Legend.Visible = False
            PiePen.Color = clWhite
          end
        end
      end
      object TabSheet2: TRzTabSheet
        Color = clWhite
        Caption = #28165#36134#26085#24535
        object RzToolbar2: TRzToolbar
          Left = 0
          Top = 0
          Width = 790
          Height = 29
          AutoStyle = False
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsNone
          BorderSides = [sdTop]
          BorderWidth = 0
          Caption = #28165#31639
          GradientColorStyle = gcsCustom
          TabOrder = 0
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer5
            RzToolButton12
            cxLabel2
            cxLookupComboBox1
            RzSpacer14
            cxLabel1
            cxDateEdit3
            RzSpacer16
            cxLabel4
            cxDateEdit4
            RzSpacer15
            RzToolButton7
            RzSpacer19
            RzToolButton14
            RzSpacer11
            RzToolButton11
            RzSpacer20)
          object RzSpacer14: TRzSpacer
            Left = 248
            Top = 2
          end
          object RzToolButton14: TRzToolButton
            Left = 613
            Top = 2
            Width = 75
            SelectionColorStop = 16119543
            DropDownMenu = pmLogReport
            ImageIndex = 5
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #25253#34920
          end
          object RzSpacer16: TRzSpacer
            Left = 410
            Top = 2
          end
          object RzSpacer5: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzSpacer15: TRzSpacer
            Left = 572
            Top = 2
          end
          object RzToolButton7: TRzToolButton
            Left = 580
            Top = 2
            DisabledIndex = 59
            SelectionColorStop = 16119543
            ImageIndex = 59
            Caption = #26597#35810
            OnClick = RzToolButton7Click
          end
          object RzSpacer19: TRzSpacer
            Left = 605
            Top = 2
          end
          object RzSpacer11: TRzSpacer
            Left = 688
            Top = 2
          end
          object RzToolButton11: TRzToolButton
            Left = 696
            Top = 2
            Width = 65
            SelectionColorStop = 16119543
            ImageIndex = 8
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #36864#20986
            OnClick = RzToolButton6Click
          end
          object RzSpacer20: TRzSpacer
            Left = 761
            Top = 2
          end
          object RzToolButton12: TRzToolButton
            Left = 12
            Top = 2
            Width = 76
            SelectionColorStop = 16119543
            ImageIndex = 21
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #20840#37096#26085#24535
            OnClick = RzToolButton12Click
          end
          object cxLabel2: TcxLabel
            Left = 88
            Top = 6
            Caption = #20851#38190#23383':'
            Transparent = True
          end
          object cxLabel1: TcxLabel
            Left = 256
            Top = 6
            Caption = #24320#22987#26085#26399#65306
            Transparent = True
          end
          object cxDateEdit3: TcxDateEdit
            Left = 320
            Top = 4
            TabOrder = 2
            Width = 90
          end
          object cxLabel4: TcxLabel
            Left = 418
            Top = 6
            Caption = #32467#26463#26085#26399#65306
            Transparent = True
          end
          object cxDateEdit4: TcxDateEdit
            Left = 482
            Top = 4
            TabOrder = 4
            Width = 90
          end
          object cxLookupComboBox1: TcxLookupComboBox
            Left = 132
            Top = 4
            AutoSize = False
            Properties.DropDownWidth = 260
            Properties.ImmediateDropDownWhenActivated = True
            Properties.KeyFieldNames = 'SignName'
            Properties.ListColumns = <
              item
                Caption = #24448#26469#21333#20301
                Width = 150
                FieldName = 'SignName'
              end
              item
                Caption = #31616#30721
                Width = 60
                FieldName = 'PYCode'
              end>
            Properties.ListSource = DataSign
            Properties.OnCloseUp = cxLookupComboBox1PropertiesCloseUp
            TabOrder = 5
            Height = 21
            Width = 116
          end
        end
        object LogInfoGrid: TcxGrid
          Left = 0
          Top = 29
          Width = 790
          Height = 603
          Align = alClient
          TabOrder = 1
          object tvLogInfoView: TcxGridDBTableView
            PopupMenu = pmLog
            OnKeyDown = tvGridKeyDown
            Navigator.Buttons.CustomButtons = <>
            OnCustomDrawCell = tvLogInfoViewCustomDrawCell
            OnEditing = tvLogInfoViewEditing
            DataController.DataSource = DataSumLogInfo
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = cxGridDBColumn12
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = cxGridDBColumn13
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = cxGridDBColumn14
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = cxGridDBColumn15
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = cxGridDBColumn16
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                OnGetText = tvLogInfoViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems5GetText
                Column = cxGridDBColumn18
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = cxGridDBColumn21
              end
              item
                Kind = skCount
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvLogInfoViewColumn1
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvLogInfoViewColumn2
              end
              item
                Kind = skCount
                Column = tvLogInfoViewColumn4
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #21333#20987#27492#22788#23450#20041#31579#36873#22120
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.NoDataToDisplayInfoText = '<'#26080#21487#26174#31034#30340#25968#25454'>'
            OptionsView.DataRowHeight = 23
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 23
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 14
            object cxGridDBColumn4: TcxGridDBColumn
              Caption = #24207#21495
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              OnGetDisplayText = tvGridColumn1GetDisplayText
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Options.Filtering = False
              Options.Focusing = False
              Options.Sorting = False
              Width = 40
            end
            object cxGridDBColumn5: TcxGridDBColumn
              Caption = #32534#21495
              DataBinding.FieldName = 'Id'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 78
            end
            object cxGridDBColumn6: TcxGridDBColumn
              Caption = #23545#24080#26085#26399
              DataBinding.FieldName = 'InputDate'
              PropertiesClassName = 'TcxDateEditProperties'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 70
            end
            object cxGridDBColumn7: TcxGridDBColumn
              Caption = #26045#24037#21333#20301
              DataBinding.FieldName = 'BuilderCompany'
              PropertiesClassName = 'TcxTextEditProperties'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 119
            end
            object cxGridDBColumn8: TcxGridDBColumn
              Caption = #24448#26469#21333#20301
              DataBinding.FieldName = 'SignName'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 140
            end
            object cxGridDBColumn9: TcxGridDBColumn
              Caption = #23545#24080#26085#26399
              DataBinding.FieldName = 'InputDate'
              PropertiesClassName = 'TcxDateEditProperties'
              Properties.DisplayFormat = 'yyyy-mm-dd'
              Properties.EditFormat = 'yyyy-mm-dd'
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 105
            end
            object cxGridDBColumn10: TcxGridDBColumn
              Caption = #32467#26463#26085#26399
              DataBinding.FieldName = 'EndDate'
              PropertiesClassName = 'TcxDateEditProperties'
              Visible = False
              OnCustomDrawCell = tvGridColumn7CustomDrawCell
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object cxGridDBColumn11: TcxGridDBColumn
              Caption = #24402#32435#20998#31867
              DataBinding.FieldName = 'InduceType'
              PropertiesClassName = 'TcxComboBoxProperties'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 90
            end
            object cxGridDBColumn12: TcxGridDBColumn
              Caption = #24080#30446#24635#20215
              DataBinding.FieldName = 'TotalAccount'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object cxGridDBColumn13: TcxGridDBColumn
              Caption = #23545#24080#21069#24050#20184
              DataBinding.FieldName = 'AccountPaid'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object cxGridDBColumn14: TcxGridDBColumn
              Caption = #26412#27425#25903#20184
              DataBinding.FieldName = 'ThisPaid'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object cxGridDBColumn15: TcxGridDBColumn
              Caption = #25187#38500
              DataBinding.FieldName = 'Deduct'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object cxGridDBColumn16: TcxGridDBColumn
              Caption = #22686#22870
              DataBinding.FieldName = 'AwardPrize'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 100
            end
            object cxGridDBColumn17: TcxGridDBColumn
              Caption = #27424#25454#26085#26399
              DataBinding.FieldName = 'DocumentDate'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 60
            end
            object cxGridDBColumn19: TcxGridDBColumn
              Caption = #23545#24080#29366#24577
              DataBinding.FieldName = 'CashAccountStatus'
              PropertiesClassName = 'TcxComboBoxProperties'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 60
            end
            object cxGridDBColumn20: TcxGridDBColumn
              Caption = #28165#24080#29366#24577
              DataBinding.FieldName = 'ClearingStatus'
              PropertiesClassName = 'TcxComboBoxProperties'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 60
            end
            object cxGridDBColumn21: TcxGridDBColumn
              Caption = #24080#30446#24635#20215
              DataBinding.FieldName = 'SumMoney'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 151
            end
            object tvLogInfoViewColumn1: TcxGridDBColumn
              Caption = #25187#38500
              DataBinding.FieldName = 'DeductMoney'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 93
            end
            object tvLogInfoViewColumn2: TcxGridDBColumn
              Caption = #22686#22870
              DataBinding.FieldName = 'RewardMoney'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 83
            end
            object tvLogInfoViewColumn3: TcxGridDBColumn
              Caption = #27169#22359#31867#22411
              DataBinding.FieldName = 'ModuleType'
              Visible = False
            end
            object cxGridDBColumn18: TcxGridDBColumn
              Caption = #21512#35745#37329#39069
              DataBinding.FieldName = 'ClearBalance'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Visible = False
              OnCustomDrawCell = tvGridColumn10CustomDrawCell
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 121
            end
            object tvLogInfoViewColumn4: TcxGridDBColumn
              Caption = #22823#20889
              DataBinding.FieldName = 'ClearBalanceCapital'
              Visible = False
              Width = 320
            end
          end
          object LogInfoLv: TcxGridLevel
            GridView = tvLogInfoView
          end
        end
      end
    end
    object RzPanel13: TRzPanel
      Left = 1
      Top = 0
      Width = 792
      Height = 30
      Align = alTop
      BorderOuter = fsFlat
      BorderSides = [sdBottom]
      TabOrder = 1
      OnResize = RzPanel13Resize
      object RzPanel14: TRzPanel
        Left = 0
        Top = 0
        Width = 71
        Height = 29
        Align = alLeft
        Alignment = taRightJustify
        BorderOuter = fsFlat
        BorderSides = [sdRight]
        Caption = #20538#21069#25104#26412#65306
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object RzPanel15: TRzPanel
        Left = 71
        Top = 0
        Width = 115
        Height = 29
        CustomHint = FBalloonHint
        Align = alLeft
        BorderOuter = fsFlat
        BorderSides = [sdRight]
        Caption = '0'
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object RzPanel16: TRzPanel
        Left = 186
        Top = 0
        Width = 68
        Height = 29
        Align = alLeft
        Alignment = taRightJustify
        BorderOuter = fsFlat
        BorderSides = [sdRight]
        Caption = #20538#21069#24050#20184#65306
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object RzPanel17: TRzPanel
        Left = 254
        Top = 0
        Width = 143
        Height = 29
        Align = alLeft
        BorderOuter = fsFlat
        BorderSides = [sdRight]
        Caption = '0'
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clFuchsia
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
      end
      object RzPanel18: TRzPanel
        Left = 397
        Top = 0
        Width = 80
        Height = 29
        Align = alLeft
        BorderOuter = fsFlat
        BorderSides = [sdRight]
        Caption = #20538#21518#25910#27454#65306
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
      end
      object RzPanel19: TRzPanel
        Left = 477
        Top = 0
        Width = 123
        Height = 29
        Align = alLeft
        BorderOuter = fsFlat
        BorderSides = [sdRight]
        Caption = '0'
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
      end
      object RzPanel20: TRzPanel
        Left = 600
        Top = 0
        Width = 80
        Height = 29
        Align = alLeft
        BorderOuter = fsFlat
        BorderSides = [sdRight]
        Caption = #20538#21518#25903#27454#65306
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
      end
      object RzPanel21: TRzPanel
        Left = 680
        Top = 0
        Width = 137
        Height = 29
        Align = alLeft
        BorderOuter = fsFlat
        BorderSides = [sdRight]
        Caption = '0'
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
      end
    end
  end
  object RzPanel5: TRzPanel
    Left = 0
    Top = 30
    Width = 250
    Height = 694
    Align = alLeft
    BorderOuter = fsFlat
    BorderSides = [sdLeft, sdRight, sdBottom]
    TabOrder = 3
    OnResize = RzPanel5Resize
    object SignNameGrid: TcxGrid
      Left = 1
      Top = 331
      Width = 248
      Height = 362
      Align = alClient
      TabOrder = 0
      object tvSignName: TcxGridDBTableView
        OnDblClick = tvSignNameDblClick
        Navigator.Buttons.CustomButtons = <>
        OnCustomDrawCell = tvLogInfoViewCustomDrawCell
        OnEditing = tvSignNameEditing
        DataController.DataSource = DataCompany
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.NoDataToDisplayInfoText = '<'#27809#26377#20219#20309#24448#26469#21333#20301'>'
        OptionsView.DataRowHeight = 26
        OptionsView.GroupByBox = False
        OptionsView.HeaderHeight = 28
        OptionsView.Indicator = True
        OptionsView.IndicatorWidth = 20
        object tvSignNameColumn1: TcxGridDBColumn
          Caption = #24207#21495
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taCenter
          OnGetDisplayText = tvSignNameColumn1GetDisplayText
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Focusing = False
          Options.Moving = False
          Options.Sorting = False
          Width = 40
        end
        object tvSignNameColumn2: TcxGridDBColumn
          Caption = #24448#26469#21333#20301
          DataBinding.FieldName = 'SignName'
          HeaderAlignmentHorz = taCenter
          Options.Filtering = False
          Options.Moving = False
          Options.Sorting = False
          Width = 176
        end
      end
      object SignNameLv: TcxGridLevel
        GridView = tvSignName
      end
    end
    object RzToolbar3: TRzToolbar
      Left = 1
      Top = 0
      Width = 248
      Height = 29
      AutoStyle = False
      Images = DM.cxImageList1
      BorderInner = fsNone
      BorderOuter = fsNone
      BorderSides = [sdTop]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 1
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer7
        cxLabel3
        Search
        RzSpacer13
        RzToolButton4
        RzSpacer1
        RzToolButton1)
      object RzSpacer7: TRzSpacer
        Left = 4
        Top = 2
      end
      object RzSpacer13: TRzSpacer
        Left = 172
        Top = 2
      end
      object RzToolButton4: TRzToolButton
        Left = 180
        Top = 2
        SelectionColorStop = 16119543
        ImageIndex = 10
        OnClick = RzToolButton4Click
      end
      object RzSpacer1: TRzSpacer
        Left = 205
        Top = 2
      end
      object RzToolButton1: TRzToolButton
        Left = 213
        Top = 2
        SelectionColorStop = 16119543
        ImageIndex = 11
        OnClick = RzToolButton1Click
      end
      object cxLabel3: TcxLabel
        Left = 12
        Top = 6
        Caption = #20851#38190#23383':'
        Transparent = True
      end
      object Search: TcxLookupComboBox
        Left = 56
        Top = 4
        AutoSize = False
        Properties.DropDownWidth = 260
        Properties.ImmediateDropDownWhenActivated = True
        Properties.KeyFieldNames = 'SignName'
        Properties.ListColumns = <
          item
            Caption = #24448#26469#21333#20301
            Width = 150
            FieldName = 'SignName'
          end
          item
            Caption = #31616#30721
            Width = 60
            FieldName = 'PYCode'
          end>
        Properties.ListSource = DataSign
        Properties.OnCloseUp = SearchPropertiesCloseUp
        TabOrder = 1
        OnKeyDown = SearchKeyDown
        Height = 21
        Width = 116
      end
    end
    object cxSplitter1: TcxSplitter
      Left = 1
      Top = 323
      Width = 248
      Height = 8
      HotZoneClassName = 'TcxMediaPlayer8Style'
      HotZone.SizePercent = 61
      AlignSplitter = salTop
      AutoSnap = True
      ResizeUpdate = True
      Control = Tv
    end
    object Tv: TTreeView
      AlignWithMargins = True
      Left = 4
      Top = 32
      Width = 242
      Height = 288
      Align = alTop
      DragMode = dmAutomatic
      HideSelection = False
      Images = DM.TreeImageList
      Indent = 19
      ReadOnly = True
      RowSelect = True
      TabOrder = 3
      OnClick = TvClick
      Items.NodeData = {
        0301000000260000000000000000000000FFFFFFFFFFFFFFFF00000000000000
        00000000000104805F656755534D4F}
    end
  end
  object cxProgressBar1: TcxProgressBar
    Left = 521
    Top = 65
    TabOrder = 4
    Visible = False
    Width = 527
  end
  object pmSumReport: TPopupMenu
    Images = DM.cxImageList1
    Left = 312
    Top = 160
    object N1: TMenuItem
      Caption = #25171#21360#25253#34920
      ImageIndex = 7
      OnClick = N1Click
    end
    object Excel1: TMenuItem
      Caption = #23548#20986'Excel'
      OnClick = Excel1Click
    end
  end
  object DataSumInfo: TDataSource
    DataSet = ADOSumInfo
    Left = 144
    Top = 200
  end
  object ADODet: TADODataSet
    Connection = DM.ADOconn
    LockType = ltBatchOptimistic
    Parameters = <>
    Left = 1312
    Top = 208
    object ADODetSignName: TStringField
      FieldName = 'SignName'
      Size = 255
    end
    object ADODetSign_Id: TIntegerField
      FieldName = 'Sign_Id'
    end
    object ADODetdetDate: TDateField
      FieldName = 'detDate'
    end
    object ADODetdetSumMoney: TCurrencyField
      FieldName = 'detSumMoney'
    end
    object ADODetdetType: TStringField
      FieldName = 'detType'
      Size = 255
    end
    object ADODetModuleIndex: TIntegerField
      FieldName = 'ModuleIndex'
    end
  end
  object DataDet: TDataSource
    DataSet = ADODet
    Left = 1312
    Top = 256
  end
  object ADOSumInfo: TADODataSet
    Connection = DM.ADOconn
    LockType = ltBatchOptimistic
    AfterInsert = ADOSumInfoAfterInsert
    Parameters = <>
    Left = 144
    Top = 152
    object ADOSumInfoSignName: TStringField
      FieldName = 'SignName'
      Size = 255
    end
    object ADOSumInfoInduceType: TStringField
      FieldName = 'InduceType'
      Size = 50
    end
    object ADOSumInfoClearBalance: TCurrencyField
      FieldName = 'ClearBalance'
    end
    object ADOSumInfoClearBalanceCapital: TStringField
      FieldName = 'ClearBalanceCapital'
      Size = 255
    end
    object ADOSumInfoInputDate: TDateTimeField
      FieldName = 'InputDate'
    end
    object ADOSumInfoPaidMoney: TCurrencyField
      FieldName = 'PaidMoney'
    end
    object ADOSumInfoDeductMoney: TCurrencyField
      FieldName = 'DeductMoney'
    end
    object ADOSumInfoRewardMoney: TCurrencyField
      FieldName = 'RewardMoney'
    end
    object ADOSumInfoSumMoney: TCurrencyField
      FieldName = 'SumMoney'
    end
    object ADOSumInfoSign_Id: TIntegerField
      FieldName = 'Sign_Id'
    end
    object ADOSumInforatio: TFloatField
      FieldName = 'ratio'
    end
  end
  object ADOSumLogInfo: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 184
    Top = 440
  end
  object DataSumLogInfo: TDataSource
    DataSet = ADOSumLogInfo
    Left = 184
    Top = 488
  end
  object ADOCompany: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 80
    Top = 416
  end
  object DataCompany: TDataSource
    DataSet = ADOCompany
    Left = 80
    Top = 464
  end
  object ADOSign: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 80
    Top = 152
  end
  object DataSign: TDataSource
    DataSet = ADOSign
    Left = 80
    Top = 208
  end
  object pmRigth: TPopupMenu
    Left = 376
    Top = 168
    object N5: TMenuItem
      Caption = #20010#20154#23545#24080
      OnClick = N5Click
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object N6: TMenuItem
      Caption = #27719#24635#26126#32454
      object N8: TMenuItem
        Caption = #24080#30446#24635#20215
        OnClick = N8Click
      end
      object N9: TMenuItem
        Caption = #24050#20184#25104#26412
        OnClick = N8Click
      end
      object N10: TMenuItem
        Caption = #25187#38500#22686#22870
        Visible = False
        OnClick = N8Click
      end
    end
  end
  object qry1: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 968
    Top = 192
  end
  object SUMData: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 576
    Top = 304
    object SUMDataPrjectName: TStringField
      FieldName = 'PrjectName'
    end
    object SUMDataSumMoney: TCurrencyField
      FieldName = 'SumMoney'
    end
  end
  object FBalloonHint: TBalloonHint
    Images = DM.il1
    Left = 518
    Top = 218
  end
  object PaidData: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 496
    Top = 306
    object PaidDataPrjectName: TStringField
      FieldName = 'PrjectName'
      Size = 255
    end
    object PaidDataPaidMoney: TCurrencyField
      FieldName = 'PaidMoney'
    end
  end
  object ChartData: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 424
    Top = 312
    object ChartProjectName: TStringField
      FieldName = 'ProjectName'
      Size = 100
    end
    object ChartSumMoney: TCurrencyField
      FieldName = 'SumMoney'
    end
  end
  object pmDetReport: TPopupMenu
    Left = 1150
    Top = 114
    object N11: TMenuItem
      Caption = #25171#21360#25253#34920
      OnClick = N11Click
    end
    object Exel1: TMenuItem
      Caption = #23548#20986'Excel'
      OnClick = Exel1Click
    end
  end
  object pmLogReport: TPopupMenu
    Images = DM.cxImageList1
    Left = 312
    Top = 368
    object MenuItem1: TMenuItem
      Caption = #25171#21360#25253#34920
      ImageIndex = 7
      OnClick = MenuItem1Click
    end
    object MenuItem2: TMenuItem
      Caption = #23548#20986'Excel'
      OnClick = MenuItem2Click
    end
  end
  object pmLog: TPopupMenu
    Images = DM.cxImageList1
    Left = 374
    Top = 370
    object N2: TMenuItem
      Caption = #21024#38500#26085#24535
      ImageIndex = 51
      OnClick = N2Click
    end
  end
  object pmDet: TPopupMenu
    Images = DM.cxImageList1
    Left = 1152
    Top = 174
    object N3: TMenuItem
      Caption = #21024#38500#20538#21153
      ImageIndex = 51
      OnClick = N3Click
    end
    object N4: TMenuItem
      Caption = #20538#21153#38468#20214
      ImageIndex = 55
      OnClick = N4Click
    end
  end
  object tmr: TTimer
    Enabled = False
    Interval = 500
    OnTimer = tmrTimer
    Left = 528
    Top = 384
  end
end
