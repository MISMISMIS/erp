object frmStorageTreeView: TfrmStorageTreeView
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #20179#24211#36873#25321
  ClientHeight = 397
  ClientWidth = 302
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object tv: TTreeView
    AlignWithMargins = True
    Left = 3
    Top = 32
    Width = 296
    Height = 362
    Align = alClient
    Images = DM.TreeImageList
    Indent = 19
    TabOrder = 0
    OnClick = tvClick
    OnDblClick = tvDblClick
    OnEdited = tvEdited
    Items.NodeData = {
      0301000000260000000000000000000000FFFFFFFFFFFFFFFF00000000000000
      00020000000104D34E935E0D54F079260000000000000000000000FFFFFFFFFF
      FFFFFF0000000000000000000000000104D34E935E3000310026000000000000
      0000000000FFFFFFFFFFFFFFFF0000000000000000000000000104D34E935E30
      003200}
    ExplicitTop = 3
    ExplicitHeight = 391
  end
  object RzToolbar5: TRzToolbar
    Left = 0
    Top = 0
    Width = 302
    Height = 29
    AutoStyle = False
    Images = DM.cxImageList1
    BorderInner = fsFlat
    BorderOuter = fsNone
    BorderSides = [sdBottom]
    BorderWidth = 0
    GradientColorStyle = gcsCustom
    TabOrder = 1
    VisualStyle = vsGradient
    ExplicitLeft = 1
    ExplicitWidth = 272
    ToolbarControls = (
      RzSpacer27
      RzBut1
      RzSpacer28
      RzBut2
      RzSpacer29
      RzBut3
      RzSpacer7)
    object RzSpacer27: TRzSpacer
      Left = 4
      Top = 2
    end
    object RzBut1: TRzToolButton
      Left = 12
      Top = 2
      Width = 50
      SelectionColorStop = 16119543
      ImageIndex = 37
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #26032#22686
      OnClick = RzBut1Click
    end
    object RzSpacer28: TRzSpacer
      Left = 62
      Top = 2
    end
    object RzBut2: TRzToolButton
      Left = 70
      Top = 2
      Width = 50
      SelectionColorStop = 16119543
      ImageIndex = 0
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #32534#36753
      OnClick = RzBut1Click
    end
    object RzSpacer29: TRzSpacer
      Left = 120
      Top = 2
    end
    object RzBut3: TRzToolButton
      Left = 128
      Top = 2
      Width = 50
      SelectionColorStop = 16119543
      ImageIndex = 51
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #21024#38500
      OnClick = RzBut3Click
    end
    object RzSpacer7: TRzSpacer
      Left = 178
      Top = 2
    end
  end
end
