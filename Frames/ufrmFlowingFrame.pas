unit ufrmFlowingFrame;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData,
  cxTextEdit, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, RzTabs, Vcl.ComCtrls,
  cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxLabel, RzButton, RzPanel, Vcl.ExtCtrls,UtilsTree,FillThrdTree,
  Data.Win.ADODB, Datasnap.DBClient;

type
  TfrmFlowingFrame = class(TFrame)
    RzPanel2: TRzPanel;
    TopSplitter: TSplitter;
    KeyTool: TRzToolbar;
    RzSpacer13: TRzSpacer;
    RzSpacer15: TRzSpacer;
    RzToolButton12: TRzToolButton;
    RzSpacer51: TRzSpacer;
    Refresh: TRzToolButton;
    cxLabel3: TcxLabel;
    cxLookupComboBox2: TcxLookupComboBox;
    Tv: TTreeView;
    RzPageControl1: TRzPageControl;
    TabSheet1: TRzTabSheet;
    cxGrid1: TcxGrid;
    tvSignName: TcxGridDBTableView;
    tvSignNameCol1: TcxGridDBColumn;
    tvSignNameCol2: TcxGridDBColumn;
    Lv1: TcxGridLevel;
    RzToolbar5: TRzToolbar;
    RzSpacer26: TRzSpacer;
    RzSpacer27: TRzSpacer;
    RzToolButton9: TRzToolButton;
    cxLabel1: TcxLabel;
    Search: TcxLookupComboBox;
    TabSheet2: TRzTabSheet;
    cxGrid3: TcxGrid;
    tvCompany: TcxGridDBTableView;
    tvCompanyColumn1: TcxGridDBColumn;
    tvCompanyColumn2: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    RzToolbar1: TRzToolbar;
    RzSpacer16: TRzSpacer;
    RzSpacer17: TRzSpacer;
    RzToolButton13: TRzToolButton;
    cxLabel2: TcxLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    DataCompany: TDataSource;
    ADOCompany: TClientDataSet;
    procedure cxLookupComboBox2PropertiesCloseUp(Sender: TObject);
    procedure RzToolButton12Click(Sender: TObject);
    procedure FrameClick(Sender: TObject);
    procedure TvClick(Sender: TObject);
    procedure tvSignNameDblClick(Sender: TObject);
    procedure tvCompanyDblClick(Sender: TObject);
    procedure SearchPropertiesCloseUp(Sender: TObject);
    procedure cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
    procedure RzToolButton13Click(Sender: TObject);
    procedure RzToolButton9Click(Sender: TObject);
    procedure tvSignNameCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure SearchKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxLookupComboBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RefreshClick(Sender: TObject);
  private
    { Private declarations }

    g_TreeView : TNewUtilsTree;
    procedure OnShow(Sender: TObject);
    function FunSearch(s : string):Boolean;
  public
    { Public declarations }
    dwParentHandle:THandle;
    dwTableName: string;
    constructor create(AOwner: TComponent);override;
  end;

implementation

uses
   uDataModule,global,ufrmBaseController;

{$R *.dfm}

constructor TfrmFlowingFrame.create(AOwner: TComponent);
begin
  inherited;
end;

procedure TfrmFlowingFrame.OnShow(Sender: TObject);
begin
  g_TreeView := TNewUtilsTree.Create(Self.tv,DM.ADOconn,dwTableName,'往来单位');
  g_TreeView.GetTreeTable(0);
end;

function TfrmFlowingFrame.FunSearch(s : string):Boolean;
var
  pMsg : TProject;

begin
  pMsg.dwName:= s;
  SendMessage(dwParentHandle, WM_FrameView, 0, LPARAM(@pMsg));  // 发消息
end;

function GridLocateRecord(View : TcxGridDBTableView; FieldName, LocateValue : String) : Boolean;
begin
  {表格数据查找定位}
  Result := False;
  if (View.GetColumnByFieldName(FieldName) <> nil) then
  begin
    Result := View.DataController.Search.Locate(View.GetColumnByFieldName(FieldName).Index, LocateValue);
  end;
end;

procedure TfrmFlowingFrame.cxLookupComboBox1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = 13 then
  begin
    Self.RzToolButton13.Click;
  end;
end;

procedure TfrmFlowingFrame.cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
var
  szRowIndex : Integer;
  szColName : string;

begin
  inherited;
  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;
    if szRowIndex >= 0 then
    begin
      szColName := DataController.Values[szRowIndex,0];
      GridLocateRecord(Self.tvCompany,Self.tvCompanyColumn2.DataBinding.FieldName,szColName);
    end;
  end;
end;

procedure TfrmFlowingFrame.cxLookupComboBox2PropertiesCloseUp(Sender: TObject);
var
  szRowIndex : Integer;
  szColName : string;

begin
  inherited;

  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;
    if szRowIndex >= 0 then
    begin
      szColName := DataController.Values[szRowIndex,0];
      FunSearch(VarToStr(szColName));
    end;
  end;

end;

procedure TfrmFlowingFrame.FrameClick(Sender: TObject);
begin
  OnShow(Sender);
end;

procedure TfrmFlowingFrame.RefreshClick(Sender: TObject);
begin
  SendMessage(dwParentHandle, WM_Refresh, 0, 0);  // 发消息
end;

procedure TfrmFlowingFrame.RzToolButton12Click(Sender: TObject);
var
  s : string;
begin
  inherited;
  s := Self.cxLookupComboBox2.Text;
  FunSearch(s);
end;

procedure TfrmFlowingFrame.RzToolButton13Click(Sender: TObject);
begin
  GridLocateRecord(Self.tvCompany,Self.tvCompanyColumn2.DataBinding.FieldName,Self.cxLookupComboBox1.Text);
end;

procedure TfrmFlowingFrame.RzToolButton9Click(Sender: TObject);
begin
  GridLocateRecord(Self.tvSignName,Self.tvSignNameCol2.DataBinding.FieldName,Self.Search.Text);
end;

procedure TfrmFlowingFrame.SearchKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
    Self.RzToolButton9.Click;
end;

procedure TfrmFlowingFrame.SearchPropertiesCloseUp(Sender: TObject);
var
  szRowIndex : Integer;
  szColName : string;

begin
  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;
    if szRowIndex >= 0 then
    begin
      szColName := DataController.Values[szRowIndex,0];
      GridLocateRecord(Self.tvCompany,Self.tvCompanyColumn2.DataBinding.FieldName,szColName);
    end;
  end;
end;

procedure TfrmFlowingFrame.TvClick(Sender: TObject);
var
  Node : TTreeNode;
  I: Integer;
  szData : PNodeData;
  szCodeId : Integer;

begin
  inherited;
  Node := Self.Tv.Selected;
  if Assigned(Node) then
  begin
    if Node.Level > 0 then
    begin
      szData := PNodeData(Node.Data);
      if Assigned(szData) then
      begin
        szCodeId := szData^.Index;

        with DM do
        begin
          ADOQuery1.Close;
          ADOQuery1.SQL.Text := 'Select * from ' + g_Table_CompanyManage + ' WHERE ' + 'Sign_Id' + '=' + IntToStr(szCodeId) ;
          ADOQuery1.Open;
          Self.ADOCompany.Data := DataSetProvider1.Data;
          Self.ADOCompany.Open;
        end;

      end;

    end;

  end;

end;

procedure TfrmFlowingFrame.tvCompanyDblClick(Sender: TObject);
var
  s : string;
begin
  s := VarToStr( Self.tvCompanyColumn2.EditValue );
  FunSearch(s);
end;

procedure TfrmFlowingFrame.tvSignNameCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmFlowingFrame.tvSignNameDblClick(Sender: TObject);
var
  s : string;
begin
  s := VarToStr( Self.tvSignNameCol2.EditValue );
  FunSearch( s );
end;

end.
