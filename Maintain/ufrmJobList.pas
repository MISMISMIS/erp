unit ufrmJobList;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, Vcl.ComCtrls, RzButton,
  RzPanel, Vcl.ExtCtrls, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, Data.Win.ADODB,ufrmBaseController,
  cxTextEdit;

type
  TfrmJobList = class(TfrmBaseController)
    Grid: TcxGrid;
    tvJob: TcxGridDBTableView;
    tvJobColumn1: TcxGridDBColumn;
    tvJobColumn2: TcxGridDBColumn;
    Lv: TcxGridLevel;
    RzToolbar1: TRzToolbar;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzSpacer4: TRzSpacer;
    RzToolButton5: TRzToolButton;
    StatusBar1: TStatusBar;
    ADOJob: TADOQuery;
    DataJob: TDataSource;
    procedure RzToolButton5Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure RzToolButton2Click(Sender: TObject);
    procedure RzToolButton3Click(Sender: TObject);
    procedure ADOJobAfterOpen(DataSet: TDataSet);
    procedure tvJobColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tvJobEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvJobEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmJobList: TfrmJobList;

implementation

uses
   uDataModule,global;

{$R *.dfm}

procedure TfrmJobList.ADOJobAfterOpen(DataSet: TDataSet);
begin
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmJobList.FormActivate(Sender: TObject);
begin
  with Self.ADOJob do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + g_Table_Maintain_Job;
    Open;
  end;
end;

procedure TfrmJobList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Self.ADOJob.Close;
end;

procedure TfrmJobList.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 27 then
  begin
    Close;
  end;
end;

procedure TfrmJobList.RzToolButton1Click(Sender: TObject);
begin
  with Self.ADOJob do
  begin
    if State <> dsInactive then
    begin
      Append;
      Self.tvJobColumn2.FocusWithSelection;
      Self.Grid.SetFocus;
      keybd_event(VK_RETURN,0,0,0);
    end;
  end;
end;

procedure TfrmJobList.RzToolButton2Click(Sender: TObject);
begin
  //����
  {
  IsDeleteEmptyData(Self.ADOJob ,
                 'sk_Maintain_Job' ,
                 Self.tvJobColumn2.DataBinding.FieldName);
  }
  IsDeleteEmptyData(Self.ADOJob ,'' ,'');
end;

procedure TfrmJobList.RzToolButton5Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmJobList.tvJobColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
  ARecord: TcxCustomGridRecord; var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmJobList.tvJobEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  if AItem.Index = Self.tvJobColumn1.Index then
  begin
    AAllow := False;
  end;
end;

procedure TfrmJobList.tvJobEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = 13) and (AItem.Index = Self.tvJobColumn2.Index) then
  begin
    if Self.tvJob.Controller.FocusedRow.IsLast then
    begin
      with Self.ADOJob do
      begin
        if State <> dsInactive then
        begin
          Append;
          Self.tvJobColumn2.FocusWithSelection;
        end;
      end;
    end;
  end;
end;

procedure TfrmJobList.RzToolButton3Click(Sender: TObject);
begin
  //ɾ��
  IsDelete(Self.tvJob);
end;

end.
