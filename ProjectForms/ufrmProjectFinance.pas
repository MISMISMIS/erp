unit ufrmProjectFinance;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.ComCtrls, RzButton,
  RzPanel, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxCore,
  cxDateUtils, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxCalendar, cxDropDownEdit,
  cxTextEdit, cxCurrencyEdit, cxMemo, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  cxMaskEdit, cxLabel, cxDBLookupComboBox,
  Data.Win.ADODB,ufrmBaseController,  Vcl.Menus,System.DateUtils, Vcl.StdCtrls //树线程
  ;

type
  TfrmProjectFinance = class(TfrmBaseController)
    RzPanel2: TRzPanel;
    Splitter1: TSplitter;
    RzPanel1: TRzPanel;
    Toolbar: TRzToolbar;
    RzSpacer77: TRzSpacer;
    RzBut4: TRzToolButton;
    RzSpacer78: TRzSpacer;
    RzBut5: TRzToolButton;
    RzSpacer79: TRzSpacer;
    RzBut6: TRzToolButton;
    RzSpacer80: TRzSpacer;
    RzBut7: TRzToolButton;
    RzSpacer81: TRzSpacer;
    RzBut12: TRzToolButton;
    RzSpacer82: TRzSpacer;
    RzSpacer83: TRzSpacer;
    RzSpacer84: TRzSpacer;
    RzSpacer85: TRzSpacer;
    RzBut11: TRzToolButton;
    RzSpacer86: TRzSpacer;
    RzBut10: TRzToolButton;
    RzSpacer87: TRzSpacer;
    cxLabel46: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxLabel47: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    Grid: TcxGrid;
    tvFinance: TcxGridDBTableView;
    tvFlowingCol1: TcxGridDBColumn;
    tvFlowingCol3: TcxGridDBColumn;
    tvFlowingCol4: TcxGridDBColumn;
    tvFlowingCol5: TcxGridDBColumn;
    tvFlowingCol6: TcxGridDBColumn;
    tvFlowingCol7: TcxGridDBColumn;
    tvFlowingCol8: TcxGridDBColumn;
    tvFlowingCol9: TcxGridDBColumn;
    tvFlowingCol10: TcxGridDBColumn;
    tvFlowingCol11: TcxGridDBColumn;
    tvFlowingCol12: TcxGridDBColumn;
    tvFlowingCol13: TcxGridDBColumn;
    tvFlowingCol14: TcxGridDBColumn;
    tvFlowingCol15: TcxGridDBColumn;
    tvFlowingCol16: TcxGridDBColumn;
    tvFlowingCol17: TcxGridDBColumn;
    lv: TcxGridLevel;
    DataFinance: TDataSource;
    ADOFinance: TADOQuery;
    qry: TADOQuery;
    ds: TDataSource;
    tvFlowingCol18: TcxGridDBColumn;
    ADOSignName: TADOQuery;
    DataSignName: TDataSource;
    RzBut9: TRzToolButton;
    Lv1: TcxGridLevel;
    tvView: TcxGridDBTableView;
    tvViewColumn1: TcxGridDBColumn;
    tvViewColumn2: TcxGridDBColumn;
    tvViewColumn16: TcxGridDBColumn;
    tvViewColumn3: TcxGridDBColumn;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    tvViewColumn4: TcxGridDBColumn;
    tvViewColumn5: TcxGridDBColumn;
    tvViewColumn6: TcxGridDBColumn;
    tvViewColumn7: TcxGridDBColumn;
    tvViewColumn8: TcxGridDBColumn;
    tvViewColumn9: TcxGridDBColumn;
    tvViewColumn10: TcxGridDBColumn;
    tvViewColumn11: TcxGridDBColumn;
    tvViewColumn12: TcxGridDBColumn;
    tvViewColumn13: TcxGridDBColumn;
    tvViewColumn14: TcxGridDBColumn;
    tvViewColumn15: TcxGridDBColumn;
    ADOView: TADOQuery;
    DataView: TDataSource;
    Print: TPopupMenu;
    Excel1: TMenuItem;
    N2: TMenuItem;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RzBut4Click(Sender: TObject);
    procedure tvFlowingCol4GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure tvFlowingCol1GetDisplayText(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AText: string);
    procedure tvFinanceEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure ADOFinanceAfterInsert(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure tvFinanceColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure tvProjectViewColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvFlowingCol12PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvFinanceDblClick(Sender: TObject);
    procedure tvFinanceKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure tvFlowingCol17GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure ADOFinanceAfterOpen(DataSet: TDataSet);
    procedure RzToolButton1Click(Sender: TObject);
    procedure tvViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvFinanceTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvFlowingCol9GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure Excel1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure tvFinanceEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure RzToolButton2Click(Sender: TObject);
  private
    { Private declarations }
    g_ProjectName : string;
    g_PostCode    : string;
    IsSave : Boolean;
    Prefix : string;
    Suffix : string;
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
  public
    { Public declarations }
    g_Table_ProjectTree : string;
    dwRootName  : string;
    dwParentId  : Integer;
    procedure WndProc(var Message: TMessage); override;  // 第一优先权
  end;

var
  frmProjectFinance: TfrmProjectFinance;

implementation

uses
   global,
   ufunctions,
   uDataModule,
   ufrmIsViewGrid,
   ufrmDetailed,
   uProjectFrame;

{$R *.dfm}

procedure TfrmProjectFinance.WndProc(var Message: TMessage);
var
  pMsg : PProject;
  s : string;
  szCodeList : string;
  szTableList : array[0..1] of TTableList;
begin
  case Message.Msg of
    WM_FrameClose:
      begin
        Self.ADOFinance.Close;
      end;
    WM_FrameView :
    begin
      pMsg := PProject(Message.LParam);
      g_PostCode := pMsg.dwCode;
      g_ProjectName := pMsg.dwName;

      s := 'Select *from ' + g_Table_Project_BuildFlowingAccount + ' Where Code ="' +  g_PostCode + '"';
      with Self.ADOFinance do
      begin
        Close;
        SQL.Clear;
        SQL.Text := s;
        Open;
      end;

      with Self.ADOView do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'Select * from ' + g_Table_DetailTable;
        Open;
        if RecordCount <> 0 then SetcxGrid(Self.tvView,0,g_dir_DetailTable);

      end;


    end;
    WM_FrameDele :
    begin
      pMsg   := PProject(Message.LParam);
      szCodeList := pMsg.dwCodeList;
      szTableList[0].ATableName := g_Table_Project_BuildFlowingAccount;
      szTableList[0].ADirectory := g_Dir_Project_Finance;

      DeleteTableFile(szTableList,szCodeList);
    //  DM.InDeleteData(g_Table_Project_BuildFlowingAccount,szCodeList); //清除主目录表内关联
    end;
  end;
  // 一定要加上这句，否则编译通不过。因为绝大部分消息没人处理了
  inherited WndProc(Message); // 会一路向上调用，直到TControl.WndProc调用Dispatch来寻找消息处理函数
end;

procedure  TfrmProjectFinance.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount - 1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, Self.Name + lpSuffix);
  end;
end;

procedure TfrmProjectFinance.ADOFinanceAfterInsert(DataSet: TDataSet);
var
  szColName : string;
  s : string;
  i : Integer;
  Node : TTreeNode;
  szCode : string;

begin
  inherited;
  with DataSet do
  begin
    IsSave := True;
    FieldByName('Code').Value := g_PostCode;
    szColName := Self.tvFlowingCol3.DataBinding.FieldName;
    FieldByName(szColName).Value := Date;
    s := '收款';
    FieldByName(Self.tvFlowingCol4.DataBinding.FieldName).AsVariant := s;
    s := '现金';
    FieldByName(Self.tvFlowingCol6.DataBinding.FieldName).AsVariant := s;
    szCode := Prefix + GetRowCode(g_Table_Project_BuildFlowingAccount,
                                  Self.tvFlowingCol18.DataBinding.FieldName,
                                  Suffix,
                                  30000);
    szColName := Self.tvFlowingCol18.DataBinding.FieldName;
    FieldByName(szColName).Value := szCode;

    szColName := Self.tvFlowingCol8.DataBinding.FieldName;
    FieldByName(szColName).Value := g_ProjectName;
  end;
end;

procedure TfrmProjectFinance.ADOFinanceAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmProjectFinance.Excel1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.Grid,'财务明细表');
end;

procedure TfrmProjectFinance.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmProjectFinance.FormCreate(Sender: TObject);
var
  ChildFrame : TProjectFrame;
begin
  inherited;

  Prefix := 'CW';
  Suffix := '00002';
  ChildFrame := TProjectFrame.Create(Self);
  ChildFrame.dwParentHandle := Handle;
  ChildFrame.Parent   := Self.RzPanel2;
  ChildFrame.IsSystem := True;
  ChildFrame.FrameClick(Self);

  with Self.qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + g_Table_PayType;
    Open;
  end;
  with Self.ADOSignName do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + g_Table_CompanyManage;
    Open;
  end;

end;

procedure TfrmProjectFinance.FormShow(Sender: TObject);
begin
  inherited;
  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;

  SetcxGrid(Self.tvFinance,0,g_Dir_Project_Finance);

end;

procedure TfrmProjectFinance.N2Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.Grid;
  DM.BasePrinterLink1.ReportTitleText := '项目名称：' + g_Projectname;
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmProjectFinance.RzBut4Click(Sender: TObject);
var
  szColName : string;
  Child : TfrmIsViewGrid;
  RecNo : Integer;
  i : Integer;
  s , str : string;

  sqltext : string;
  StartDate , EndDate : string;
  szName : string;
  szIndex : Integer;
  Id , dir: string;

begin
  inherited;

  if Sender = RzBut4 then
  begin
    with Self.ADOFinance do
    begin
      if State = dsInactive then
      begin
        Application.MessageBox( '当前查询状态无效，单击项目管理在进行操作？', '提示:', MB_OKCANCEL + MB_ICONWARNING)

      end else
      begin

        First;
        Insert;
      //  Append;
        Self.tvFinance.Columns[3].Focused := True;
        Self.Grid.SetFocus;
        keybd_event(VK_RETURN,0,0,0);
      end;
    end;
  end else
  if Sender = RzBut5 then
  begin
    IsDeleteEmptyData(Self.ADOFinance ,
                      g_Table_Project_BuildFlowingAccount ,
                      Self.tvFlowingCol5.DataBinding.FieldName);
    IsSave := False;
  end else
  if Sender = RzBut6 then
  begin

      if  Application.MessageBox( '您要删除所有选中的记录吗？', '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
      begin
          with Self.tvFinance.DataController.DataSource.DataSet do
          begin
            if not IsEmpty then
            begin

              for I := 0 to Self.tvFinance.Controller.SelectedRowCount -1 do
              begin
                szName  := Self.tvFlowingCol18.DataBinding.FieldName;
                szIndex := Self.tvFinance.GetColumnByFieldName(szName).Index;
                Id := Self.tvFinance.Controller.SelectedRows[i].Values[szIndex];

                dir := ConcatEnclosure( Concat(g_Resources ,  '\' + g_dir_BranchMoney) , id);
                if DirectoryExists(dir) then  DeleteDirectory(dir);

                if Length(str) = 0 then
                  str := Id
                else
                  str := str + '","' + Id;
              end;

              with DM.Qry do
              begin
                Close;
                SQL.Clear;
                SQL.Text := 'delete * from ' +  g_Table_Project_BuildFlowingAccount + ' where ' + 'numbers' +' in("' + str + '")';
                ExecSQL;
                SQL.Clear;
                SQL.Text := 'delete * from ' +  g_Table_DetailTable + ' where ' + 'parent' +' in("' + str + '")';
                ExecSQL;
              end;

              Self.ADOFinance.Requery();
              Self.ADOView.Requery();
            end;

          end;

      end;


  end else
  if Sender = RzBut7 then
  begin
    //表格设置

    Child := TfrmIsViewGrid.Create(Application);
    try
      Child.g_fromName := Self.Name + g_Dir_Project_Finance;
      Child.ShowModal;
      SetcxGrid(Self.tvFinance,0,g_Dir_Project_Finance);
    finally
      Child.Free;
    end;

  end else
  if Sender = RzBut9 then
  begin
    //报表设置

  end else
  if Sender = RzBut10 then
  begin
    //综合文档
    CreateOpenDir(Handle,Concat( g_Resources ,'\' + 'ColligateFile'),True);
  end else
  if Sender = RzBut11 then
  begin
    //范围查询
    szColName := Self.tvFlowingCol3.DataBinding.FieldName;
    sqltext := 'select *from ' + g_Table_Project_BuildFlowingAccount + ' where Code= "' + g_PostCode + ' " and ' + szColName +' between :t1 and :t2';
    SearchDateRange(Self.cxDateEdit1.Text,Self.cxDateEdit2.Text,sqltext,Self.ADOFinance);

  end else
  if Sender = RzBut12 then
  begin
    //退出
    Close;
  end;
end;

procedure TfrmProjectFinance.RzToolButton1Click(Sender: TObject);
var
  Child   : TfrmPayDetailed;
  szName  : string;
  szMoney : Currency;

begin
  inherited;
  with Self.tvFinance.DataController.DataSource.DataSet do
  begin
    if not IsEmpty then
    begin
      Child := TfrmPayDetailed.Create(Self);
      try
        Self.tvFinance.Controller.FocusedRow.Collapse(True);
        szName := Self.tvFlowingCol12.DataBinding.FieldName;
        szMoney:= FieldByName(szName).AsCurrency;
        Child.g_Money    := szMoney;
        Child.g_formName := Self.Name;
        Child.g_Detailed := g_dir_DetailTable;
        szName := Self.tvFlowingCol18.DataBinding.FieldName;
        Child.g_PactCode   := g_PostCode;
        Child.g_TableName  := g_Table_DetailTable;

        Child.g_parentCode := FieldByName(szName).AsString;
        Child.ShowModal;
        SetcxGrid(Self.tvView,0,g_dir_DetailTable);
      //  Self.qry.Requery();
        Self.ADOView.Requery();
        Self.tvFinance.Controller.FocusedRow.Expand(True);
      finally
        Child.Free;
      end;

    end;
  end;

end;

procedure TfrmProjectFinance.RzToolButton2Click(Sender: TObject);
begin
  inherited;
  with Self.ADOFinance do
  begin
    if State <> dsInactive then
    begin
      if ( State <> dsEdit ) AND (State <> dsInsert) then  Edit;
    end;
  end;
end;

procedure TfrmProjectFinance.tvFinanceColumnSizeChanged(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvFinance,1, g_Dir_Project_Finance );
end;

procedure TfrmProjectFinance.tvFinanceDblClick(Sender: TObject);
var
  szDir : string;
  Id : string;

begin
  inherited;
  szDir := Concat(g_Resources,'\'+ g_Dir_Project_Finance);
  CreateEnclosure(Self.tvFinance.DataController.DataSource.DataSet,Handle,szDir);
end;

procedure TfrmProjectFinance.tvFinanceEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  with Self.ADOFinance do
  begin
    if State <> dsInactive then
    begin
      if ( State <> dsEdit )  and (State <> dsInsert)then
        AAllow := False
      else
        AAllow := True;
    end;
  end;

  if (AItem.Index = Self.tvFlowingCol1.Index) or
     (AItem.Index = Self.tvFlowingCol18.Index)  then
  begin
    AAllow := False;
  end;
end;

procedure TfrmProjectFinance.tvFinanceEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin

  end;
end;

procedure TfrmProjectFinance.tvFinanceKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 46 then Self.RzBut6.Click;
end;

procedure TfrmProjectFinance.tvFinanceTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  t : Currency;
  szCapital : string;

begin
  inherited;
  try
    szCapital := '';
    if AValue <> null then
    begin
      t := StrToCurr( VarToStr(AValue) );
      szCapital := MoneyConvert(t);
    end else
    begin
      szCapital := '';
    end;
    Self.tvFlowingCol13.Summary.FooterFormat := szCapital;
  except
  end;

end;

procedure TfrmProjectFinance.tvFlowingCol12PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szSumMonry : Currency;

begin
  inherited;
  szSumMonry := StrToCurr(VarToStr(DisplayValue));
  with Self.ADOFinance do
  begin
    if State <> dsInactive then
    begin
      FieldByName(Self.tvFlowingCol12.DataBinding.FieldName).Value := szSumMonry;
      FieldByName(Self.tvFlowingCol13.DataBinding.FieldName).Value := MoneyConvert(szSumMonry);
      if Self.tvFinance.Controller.FocusedRow.IsLast then
      begin
        if IsNewRow then
        begin
          Append;
          Self.tvFinance.Columns[0].Focused := True;
          keybd_event(VK_RETURN,0,0,0);
        end;
      end;
    end;
  end;

end;

procedure TfrmProjectFinance.tvFlowingCol17GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  GetCompanyInfo(AProperties);
end;

procedure TfrmProjectFinance.tvFlowingCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmProjectFinance.tvFlowingCol4GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
//  GetTableMaintain(AProperties,g_Table_Maintain_Tab);
end;

procedure TfrmProjectFinance.tvFlowingCol9GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  GetTableMaintain(AProperties,g_Table_Maintain_Cost);
end;

procedure TfrmProjectFinance.tvProjectViewColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmProjectFinance.tvViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  AAllow := False;
end;

end.
