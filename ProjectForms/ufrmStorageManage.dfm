inherited frmStorageManage: TfrmStorageManage
  Caption = #24211#23384#31649#29702
  ClientHeight = 603
  ClientWidth = 1319
  ExplicitWidth = 1325
  ExplicitHeight = 631
  PixelsPerInch = 96
  TextHeight = 13
  inherited Splitter1: TSplitter
    Left = 291
    Height = 603
    ExplicitLeft = 291
    ExplicitHeight = 603
  end
  inherited RzPanel1: TRzPanel
    Left = 301
    Width = 1018
    Height = 603
    ExplicitLeft = 301
    ExplicitWidth = 1018
    ExplicitHeight = 603
    inherited RzToolbar13: TRzToolbar
      Top = 31
      Width = 1016
      Caption = #26597#35810
      ExplicitTop = 31
      ExplicitWidth = 1016
      ToolbarControls = (
        RzSpacer30
        RzToolButton2
        RzSpacer3
        RzToolButton90
        RzSpacer120
        RzToolButton85
        RzSpacer113
        btnGridSet
        RzSpacer10
        RzToolButton87
        RzSpacer1
        RzToolButton1
        RzSpacer2
        RzToolButton3
        RzSpacer4
        RzToolButton88
        RzSpacer5)
      inherited RzSpacer30: TRzSpacer
        Visible = False
      end
      inherited RzToolButton85: TRzToolButton
        Left = 152
        ImageIndex = 51
        Visible = False
        ExplicitLeft = 152
      end
      inherited RzSpacer113: TRzSpacer
        Left = 214
        Visible = False
        ExplicitLeft = 214
      end
      inherited RzToolButton87: TRzToolButton
        Left = 306
        ExplicitLeft = 306
      end
      inherited RzToolButton88: TRzToolButton
        Left = 506
        ExplicitLeft = 506
      end
      inherited RzToolButton90: TRzToolButton
        Left = 82
        Visible = False
        ExplicitLeft = 82
      end
      inherited RzSpacer120: TRzSpacer
        Left = 144
        Visible = False
        ExplicitLeft = 144
      end
      inherited RzSpacer1: TRzSpacer
        Left = 368
        ExplicitLeft = 368
      end
      inherited RzSpacer10: TRzSpacer
        Left = 298
        ExplicitLeft = 298
      end
      inherited btnGridSet: TRzToolButton
        Left = 222
        Width = 76
        SelectionColorStop = 16119543
        ExplicitLeft = 222
        ExplicitWidth = 76
      end
      object RzSpacer2: TRzSpacer
        Left = 428
        Top = 2
      end
      object RzToolButton1: TRzToolButton
        Left = 376
        Top = 2
        Width = 52
        SelectionColorStop = 16119543
        ImageIndex = 10
        ShowCaption = True
        UseToolbarShowCaption = False
        Caption = #26597#35810
        OnClick = RzToolButton1Click
      end
      object RzToolButton2: TRzToolButton
        Left = 12
        Top = 2
        Width = 62
        SelectionColorStop = 16119543
        ImageIndex = 0
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #32534#36753
        Visible = False
        OnClick = RzToolButton2Click
      end
      object RzSpacer3: TRzSpacer
        Left = 74
        Top = 2
        Visible = False
      end
      object RzSpacer4: TRzSpacer
        Left = 498
        Top = 2
      end
      object RzToolButton3: TRzToolButton
        Left = 436
        Top = 2
        Width = 62
        ImageIndex = 62
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #28165#31354
        OnClick = RzToolButton3Click
      end
      object RzSpacer5: TRzSpacer
        Left = 568
        Top = 2
      end
    end
    inherited Grid: TcxGrid
      Top = 163
      Width = 1016
      Height = 440
      ExplicitTop = 163
      ExplicitWidth = 1016
      ExplicitHeight = 440
      inherited tvView: TcxGridDBTableView
        DataController.Summary.DefaultGroupSummaryItems = <
          item
            Format = #25968#20540':0.########;-0.########'
            Kind = skSum
            Column = tvViewColumn7
          end
          item
            Format = #25968#20540':0.########;-0.########'
            Kind = skSum
            Position = spFooter
            Column = tvViewColumn7
          end
          item
            Format = #25968#37327':0.###;-0.###'
            Kind = skSum
            Column = tvViewColumn8
          end
          item
            Format = #25968#37327':0.###;-0.###'
            Kind = skSum
            Position = spFooter
            Column = tvViewColumn8
          end
          item
            Format = #37329#39069':'#165',0.00;'#165'-,0.00'
            Kind = skSum
            Position = spFooter
            Column = tvViewColumn12
          end
          item
            Format = #37329#39069':'#165',0.00;'#165'-,0.00'
            Kind = skSum
            Column = tvViewColumn12
          end>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = #37329#39069':'#165',0.00;'#165'-,0.00'
            Kind = skSum
            OnGetText = tvViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
            Column = tvViewColumn12
          end
          item
            Format = #25968#20540':0.########;-0.########'
            Kind = skSum
            Column = tvViewColumn7
          end
          item
            Format = #25968#37327':0.###;-0.###'
            Kind = skSum
            OnGetText = tvViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems3GetText
            Column = tvViewColumn8
          end
          item
            Format = #165',0.00;'#165'-,0.00'
            Kind = skSum
            Column = tvViewColumn12
          end
          item
            Format = #22823#20889':'
            Kind = skCount
            Column = tvViewColumn13
          end>
        inherited tvViewColumn15: TcxGridDBColumn
          RepositoryItem = DM.SignNameBox
        end
        inherited tvViewColumn26: TcxGridDBColumn
          Caption = #26465#30446#31867#21035
          Options.Editing = False
        end
        inherited tvViewColumn16: TcxGridDBColumn
          Properties.Items.Strings = (
            #20986#24211)
          Options.Editing = False
        end
        inherited tvViewColumn4: TcxGridDBColumn
          Properties.ListColumns = <
            item
              Caption = #26448#26009#21517#31216
              HeaderAlignment = taCenter
              Width = 150
              FieldName = 'MakingCaption'
            end
            item
              Caption = #22411#21495
              HeaderAlignment = taCenter
              Width = 60
              FieldName = 'ModelIndex'
            end
            item
              Caption = #35268#26684
              HeaderAlignment = taCenter
              Width = 60
              FieldName = 'spec'
            end
            item
              Caption = #21333#20301
              FieldName = 'Company'
            end>
        end
        inherited tvViewColumn5: TcxGridDBColumn
          Options.Editing = False
        end
      end
    end
    inherited RzPanel3: TRzPanel
      Left = 459
      Top = 221
      ExplicitLeft = 459
      ExplicitTop = 221
    end
    inherited RzPanel4: TRzPanel
      Left = 226
      Top = 403
      Height = 129
      ExplicitLeft = 226
      ExplicitTop = 403
      ExplicitHeight = 129
    end
    inherited RzPanel13: TRzPanel
      Left = 187
      Top = 221
      TabOrder = 6
      ExplicitLeft = 187
      ExplicitTop = 221
    end
    object RzPanel5: TRzPanel
      Left = 1
      Top = 0
      Width = 1016
      Height = 31
      Align = alTop
      BorderOuter = fsFlat
      TabOrder = 4
      OnResize = RzPanel5Resize
      object RzPanel6: TRzPanel
        Left = 81
        Top = 1
        Width = 160
        Height = 29
        Align = alLeft
        BorderOuter = fsFlat
        BorderSides = [sdRight]
        Caption = '0'
        Color = clBackground
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object RzPanel7: TRzPanel
        Left = 241
        Top = 1
        Width = 80
        Height = 29
        Align = alLeft
        BorderOuter = fsFlat
        BorderSides = [sdRight]
        Caption = #20986#24211#24635#25968#65306
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object RzPanel8: TRzPanel
        Left = 1
        Top = 1
        Width = 80
        Height = 29
        Align = alLeft
        BorderOuter = fsFlat
        BorderSides = [sdRight]
        Caption = #20837#24211#24635#25968#65306
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object RzPanel9: TRzPanel
        Left = 801
        Top = 1
        Width = 160
        Height = 29
        Align = alLeft
        BorderOuter = fsFlat
        BorderSides = [sdRight]
        Caption = '0'
        Color = clBackground
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
      end
      object RzPanel10: TRzPanel
        Left = 721
        Top = 1
        Width = 80
        Height = 29
        Align = alLeft
        BorderOuter = fsFlat
        BorderSides = [sdRight]
        Caption = #24211#23384#24635#25968#65306
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
      end
      object RzPanel11: TRzPanel
        Left = 321
        Top = 1
        Width = 160
        Height = 29
        Align = alLeft
        BorderOuter = fsFlat
        BorderSides = [sdRight]
        Caption = '0'
        Color = clBackground
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clFuchsia
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
      end
      object RzPanel14: TRzPanel
        Left = 481
        Top = 1
        Width = 80
        Height = 29
        Align = alLeft
        BorderOuter = fsFlat
        BorderSides = [sdRight]
        Caption = #36864#36824#24635#25968#65306
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
      end
      object RzPanel15: TRzPanel
        Left = 561
        Top = 1
        Width = 160
        Height = 29
        Align = alLeft
        BorderOuter = fsFlat
        BorderSides = [sdRight]
        Caption = '0'
        Color = clBackground
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 7
      end
    end
    object RzPanel12: TRzPanel
      Left = 1
      Top = 60
      Width = 1016
      Height = 103
      Align = alTop
      BorderOuter = fsFlat
      BorderSides = [sdLeft, sdTop, sdRight]
      TabOrder = 5
      object cxLabel3: TcxLabel
        Left = 28
        Top = 45
        Caption = #26448#26009#21517#31216#65306
      end
      object cxLabel4: TcxLabel
        Left = 28
        Top = 18
        Caption = #20379#24212#21333#20301#65306
      end
      object cxLabel5: TcxLabel
        Left = 249
        Top = 16
        Caption = #36215#22987#26085#26399#65306
      end
      object cxLabel8: TcxLabel
        Left = 249
        Top = 43
        Caption = #32467#26463#26085#26399#65306
      end
      object cxDateEdit1: TcxDateEdit
        Left = 316
        Top = 16
        TabOrder = 4
        Width = 94
      end
      object cxDateEdit2: TcxDateEdit
        Left = 316
        Top = 43
        TabOrder = 5
        Width = 94
      end
      object cxRadioButton1: TcxRadioButton
        Left = 432
        Top = 45
        Width = 89
        Height = 17
        Caption = #25353#26085#26399#26597#35810
        TabOrder = 6
      end
      object cxRadioButton2: TcxRadioButton
        Left = 432
        Top = 17
        Width = 89
        Height = 17
        Caption = #25353#21517#31216#26597#35810
        Checked = True
        TabOrder = 7
        TabStop = True
      end
      object cxButton5: TcxButton
        Left = 525
        Top = 16
        Width = 90
        Height = 48
        Caption = #26597#35810
        LookAndFeel.Kind = lfUltraFlat
        LookAndFeel.NativeStyle = False
        LookAndFeel.SkinName = 'Office2013White'
        OptionsImage.Glyph.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0002000000040000000500000004000000020000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000040000
          000A000000110000001400000011000000090000000300000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000001000000050000000D0307
          10380F2455C01D448BFA152F63BD040810310000000900000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000001000000050000000D0408133D1D45
          83EC5294CBFF63AEE5FF8AB5DAFF203E70C20000001100000004000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000001000000040000000D040A143C214E8DEC5AA8
          DEFF4598E0FF93D4F6FFEAF8FEFF2C5696F00000001400000005000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000001000000040000000C050C163B265695EC5CA9DFFF3E94
          DDFF92D3F6FFEAF9FFFF76B2DDFF254878C20000001100000004000000000000
          0000000000000000000000000000000000000000000000000000000000010000
          0002000000030000000400000004000000040000000400000004000000030000
          000100000001000000040000000C0D0B0B392B5E9CEC5BABE0FF4096DFFF92D3
          F6FFEAF9FFFF86C8EEFF32619FEC070D16380000000A00000002000000000000
          0000000000000000000000000000000000000000000100000003000000050000
          00080000000B0000000E0000000F00000010000000100000000E0000000B0000
          0008000000070000000C110A08397B4C3EF58792A6FF4398E0FF93D4F6FFEAF9
          FFFF87C7EEFF3468A4EC070E183D0000000D0000000400000001000000000000
          00000000000000000000000000010000000200000004000000090C0807263B28
          21835C3D34C4775045F77A5145FF794F42FF794F43FF5C3B33CA3B26208C110B
          093700000014110A083B784C40EBAC8071FF835548FFA7BAC7FFEAF9FFFF88C8
          F0FF376CA9EC080F193C0000000D000000050000000100000000000000000000
          0000000000000000000100000002000000060604041749332C96856154F7B294
          89FFCFB8ADFFEBD9CFFFECDCD0FFECDBD0FFEBDBD0FFCFB8ACFFAE9084FF815A
          4EFF4E332BB76A4438EEA77D6FFF8F6354FFC3A79FFFC8B2AAFFB2CAD9FF3A71
          ADEC08101A3B0000000D00000005000000010000000000000000000000000000
          0000000000000000000200000006140E0C2F725246D9B59A8FFFE6D4CAFFF0E3
          DAFFF9F2EBFFFDF9F3FFFFFCF7FFFFFCF7FFFDFAF3FFFAF3EDFFF3E7DEFFE6D4
          CAFFAE9084FF7B5447FF745045FFBDA29AFFFDFCFBFFDDCEC8FF895F53F5110F
          0F390000000C0000000400000001000000000000000000000000000000000000
          00000000000100000005110C0B29886459EDD4C2B9FFEFE0D7FFF9F2EAFFF3EA
          DEFFD4BB9CFFC09D73FFB38855FFB48956FFC49F75FFD7BE9FFFF4EBDFFFFBF5
          F0FFF0E2D9FFCDB6ABFF825A4FFFB2A29DFFD4C6C1FF825C51EB140D0B390000
          000C000000040000000100000000000000000000000000000000000000000000
          000100000003050403127A594FD5D6C5BEFFEEE2D8FFFCF6EFFFDBC5ACFFB489
          59FFCDA872FFDDC18AFFEBD49DFFECD7A2FFE2C995FFD2B481FFBC9362FFDFCA
          B0FFFDF9F3FFF1E5DCFFCCB4A9FF815D52FF6F4D41EE110B09390000000C0000
          0004000000010000000000000000000000000000000000000000000000000000
          0001000000054836307FC1ACA3FFF0E4DDFFF8F0E8FFC4A17EFFBC915EFFDCB7
          7AFFE3BF7FFFE3C07FFFE4C585FFE7CC8FFFEBD39BFFEFDCABFFE7D4A4FFC7A2
          71FFCBAC86FFFAF5EDFFF0E3DAFFB5998EFF5A3E35BD00000015000000070000
          0001000000000000000000000000000000000000000000000000000000010000
          0003090706179B786DF5F2EBE7FFF3E8E1FFCEB094FFBD8F5CFFDFB371FFDFB7
          74FFE2BA79FFE3BE7FFFE5C385FFE6C78AFFE9CB91FFEBD198FFEED9A8FFF0DE
          B2FFCAA578FFD7BD9FFFF7EFE8FFE8D7CEFF8E695CFC150E0C3A000000080000
          0002000000000000000000000000000000000000000000000000000000010000
          000443332E6FC8B2AAFFF5EDE8FFECDDD0FFAE7A4FFFDAA867FFDEAE6DFFDFB2
          73FFE2B77AFFE4BB7FFFE4BF85FFE6C38BFFE9C790FFEAC994FFEBCD98FFEED6
          A9FFEDD6AEFFBF9065FFF1E4D8FFF1E7DFFFBAA095FF4A342E940000000B0000
          0003000000000000000000000000000000000000000000000000000000010000
          0005735950B5E5D9D6FFF3EAE3FFCCAA91FFC28A55FFDDA768FFDFAC6DFFDFAF
          73FFE2B379FFE4B77FFFE5BA83FFE7BF89FFE9C38FFFEAC493FFEBC696FFEBC9
          99FFF0D4AEFFD5AF87FFD8BAA0FFF5EBE4FFDAC6BDFF74544AD50000000E0000
          0004000000000000000000000000000000000000000000000000000000010000
          0005947369E0F8F4F3FFF3E9E2FFB17F5EFFD2955EFFDDA368FFE1AC76FFEAC4
          A0FFEDCBADFFEDC8A7FFE9C39CFFE7B98AFFE8BC8BFFE9BF90FFE9C092FFEAC2
          95FFECC9A0FFE5C29DFFC59877FFF6EEE8FFEADCD4FF8F6A5EFA020101130000
          0004000000000000000000000000000000000000000000000000000000010000
          0006A58379F1FFFEFEFFF3EBE5FFA46946FFD89961FFE1AA78FFF0D5C3FFF0D4
          C2FFEFCFBBFFEECDB4FFEECAAEFFECC7A7FFE8B98DFFE8B88CFFE8BA90FFE8BB
          91FFEABE95FFECC7A5FFBD8966FFF8F0EBFFF1E4DDFF9C7669FF0B0807220000
          0005000000010000000000000000000000000000000000000000000000010000
          0005AA897EF1FFFFFFFFF5EDE9FFA16544FFDB9861FFEECBB6FFF4DCD3FFF2D8
          CBFFF1D4C3FFEFCFBCFFEFCBB5FFEDC8AFFFEABF9FFFE7B189FFE7B38BFFE7B4
          8CFFE7B68EFFEAC1A2FFBF8669FFF7F1ECFFF2E7E1FF9F7A6EFF0907061E0000
          0004000000010000000000000000000000000000000000000000000000010000
          000498796FD7FAF7F6FFF8F2EFFFAB7158FFCF8B59FFF6E2DDFFF6E1DCFFF4DC
          D3FFF3D8CCFFF1D2C3FFEFCEBCFFEFCAB6FFEDC5ADFFE6AC85FFE6AE86FFE6AE
          89FFE7B08BFFE3B495FFC69278FFF6EFEAFFF0E6E2FF987568F4020101120000
          0004000000000000000000000000000000000000000000000000000000010000
          0003776058A8EAE0DCFFFBF8F7FFC39A88FFBC764EFFF7E6E4FFF8E6E5FFF6E1
          DDFFF4DCD4FFF3D7CCFFF1D1C3FFF0CDBCFFEEC7B3FFE5A67FFFE6A782FFE6A8
          83FFE7AC89FFD39C7EFFD6B2A0FFF5EDE8FFE4D7D2FF7F6358CC0000000C0000
          0003000000000000000000000000000000000000000000000000000000010000
          000244383361D6C2BBFFFFFEFEFFE7D6CEFF9F593DFFECCBBCFFF9EBECFFF8E6
          E5FFF6E1DDFFF4DBD4FFF3D6CCFFF1D1C3FFEBBDA5FFE3A27AFFE4A37CFFE4A3
          7CFFE5AA89FFBC7D65FFEDDED7FFF4ECE7FFCCB6AFFF53423B89000000090000
          0002000000000000000000000000000000000000000000000000000000000000
          000109070711AF9187E9F8F4F3FFFBF9F8FFBD9381FFAD6A4AFFF8E9E9FFF9EB
          ECFFF7E6E5FFF6E1DDFFF4DBD3FFF2D1C5FFE4A57FFFE19D74FFE29E75FFE29F
          77FFC6896CFFD4AD9DFFF5F0EAFFF6F0EDFFAE8C80FA120E0D28000000050000
          0001000000000000000000000000000000000000000000000000000000000000
          00010000000350413D6DDAC4BEFFFFFFFFFFF8F2F1FFB17F6AFFA86444FFE5BF
          AFFFF6E1DEFFF5DDD7FFEEC7B5FFE19F76FFE0996DFFE19A6EFFDA946BFFBF7F
          62FFC99A87FFF6EEEAFFF9F4F1FFD0BBB3FF5E4B449000000009000000030000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000010202020791776EBFE8DAD6FFFFFFFFFFFBF8F6FFCDAC9EFF9F5D
          43FFBC744EFFCE855BFFDC9265FFDC9266FFD28A62FFC6825FFFB5765CFFDABB
          AEFFF7F2EEFFF9F5F2FFE5D9D4FF95776DD60B08081A00000004000000010000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000020E0C0B16A98E83DAE9DDD7FFFFFFFFFFFCFAFAFFEBDC
          D5FFC69F90FFB27C66FFA46248FFA7664BFFBA856FFFD0A999FFEEE0D9FFF8F2
          EFFFFCF9F9FFE6D9D5FFAC8C82EA1A15132F0000000600000002000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000001000000020E0C0B17957D74C2DCC8C1FFFAF6F5FFFFFF
          FFFFFEFBFBFFFAF8F6FFF9F5F2FFF8F3F0FFF9F4F1FFFBF6F5FFFDFBFBFFFAF8
          F7FFD8C4BCFF9B8075D516121129000000060000000200000001000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000100000002020202075345406DB99B90E9DDCA
          C2FFEEE5E1FFFAF8F7FFFFFFFFFFFFFFFFFFFAF7F6FFEEE5E1FFDBC7BFFFBA9C
          91F25B4B457F0706051200000005000000020000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000100000003090807104036
          3256826D65A8A1877DCEB7998EEBBE9E92F49F847BCF877068B2463935610E0C
          0B19000000060000000300000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000001000000010000
          0002000000030000000400000005000000060000000500000005000000040000
          0002000000010000000100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000010000000100000001000000010000000100000001000000010000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        TabOrder = 8
        OnClick = cxButton5Click
      end
      object cxLabel9: TcxLabel
        Left = 28
        Top = 71
        Caption = #35268#12288#12288#26684#65306
      end
      object cxLabel10: TcxLabel
        Left = 249
        Top = 70
        Caption = #21046#34920#31867#21035#65306
      end
      object cxComboBox3: TcxComboBox
        Left = 316
        Top = 70
        Properties.Items.Strings = (
          #20837#24211
          #36827#24211
          #20986#24211
          #36864#36824)
        Properties.OnCloseUp = cxComboBox3PropertiesCloseUp
        TabOrder = 11
        TextHint = #36873#25321#31867#22411
        Width = 94
      end
      object cxLookupComboBox1: TcxLookupComboBox
        Left = 91
        Top = 17
        RepositoryItem = DM.SignNameBox
        Properties.DropDownWidth = 200
        Properties.ImmediateDropDownWhenActivated = True
        Properties.KeyFieldNames = 'Name'
        Properties.ListColumns = <
          item
            FieldName = 'Name'
          end
          item
            FieldName = 'pycode'
          end>
        Properties.ListSource = DataSignName
        Properties.OnCloseUp = cxLookupComboBox1PropertiesCloseUp
        TabOrder = 12
        Width = 145
      end
      object cxLookupComboBox2: TcxLookupComboBox
        Left = 91
        Top = 44
        Properties.DropDownWidth = 330
        Properties.ImmediateDropDownWhenActivated = True
        Properties.KeyFieldNames = 'MakingCaption'
        Properties.ListColumns = <
          item
            Caption = #26448#26009#21517#31216
            Width = 150
            FieldName = 'MakingCaption'
          end
          item
            Caption = #22411#21495
            Width = 120
            FieldName = 'ModelIndex'
          end
          item
            Caption = #35268#26684
            Width = 60
            FieldName = 'spec'
          end>
        Properties.ListSource = DataMaking
        Properties.OnCloseUp = cxLookupComboBox2PropertiesCloseUp
        TabOrder = 13
        Width = 145
      end
      object cxLookupComboBox3: TcxLookupComboBox
        Left = 91
        Top = 71
        Properties.DropDownListStyle = lsEditList
        Properties.DropDownWidth = 150
        Properties.ImmediateDropDownWhenActivated = True
        Properties.KeyFieldNames = 'name'
        Properties.ListColumns = <
          item
            Caption = #35268#26684
            FieldName = 'name'
          end>
        Properties.ListSource = DataSpec
        Properties.OnCloseUp = cxLookupComboBox3PropertiesCloseUp
        TabOrder = 14
        Width = 145
      end
      object cxLabel11: TcxLabel
        Left = 453
        Top = 70
        Caption = #26465#30446#31867#21035#65306
      end
      object cxComboBox4: TcxComboBox
        Left = 523
        Top = 70
        Properties.Items.Strings = (
          #22806#20511#20986#24211
          #31199#36161#20986#24211
          #35843#23384#20986#24211
          #28040#32791#20986#24211
          #29616#37329#20837#24211
          #35843#23384#20837#24211
          #24080#30446#20837#24211
          #31199#36161#20837#24211
          #36864#36824#22238#24211
          #20511#36824#22238#24211
          #31199#36161#22238#24211
          #35843#23384#22238#24211)
        Properties.OnCloseUp = cxComboBox3PropertiesCloseUp
        TabOrder = 16
        TextHint = #36873#25321#31867#22411
        Width = 94
      end
    end
  end
  inherited RzPanel2: TRzPanel
    Width = 291
    Height = 603
    ExplicitWidth = 291
    ExplicitHeight = 603
  end
  inherited DataMaking: TDataSource
    Left = 139
    Top = 526
  end
  inherited ADOMaking: TADOQuery
    Left = 139
    Top = 470
  end
  inherited Print: TPopupMenu
    Left = 216
  end
  inherited ds: TDataSource
    Left = 72
    Top = 384
  end
  inherited Storage: TADOQuery
    Left = 72
    Top = 320
  end
  inherited ADOSignName: TADOQuery
    Left = 147
    Top = 318
  end
  inherited DataSignName: TDataSource
    Left = 147
    Top = 374
  end
  object ClientDataSet1: TClientDataSet [10]
    Aggregates = <>
    Params = <>
    Left = 72
    Top = 40
  end
  object DataSource1: TDataSource [11]
    DataSet = ClientDataSet1
    Left = 72
    Top = 96
  end
  inherited ADOSpec: TADOQuery
    Left = 240
    Top = 368
  end
  inherited DataSpec: TDataSource
    Left = 240
    Top = 432
  end
end
