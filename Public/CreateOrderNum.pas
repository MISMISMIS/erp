unit CreateOrderNum;


interface


uses
  Windows,SysUtils,ADODB,StrUtils,Messages,Dialogs;
  Function Create_OrderNum(BH_FieldName,TableName,RQ_FieldName,orderFirst:string):string;
  Function Create_RecordNum(BH_FieldName,TableName,RecordFirst:string):string;
  Function GetNewBHStr(BHTableName, BHFieldName, BHFirst:string; BHLength:Integer):string;//获取唯一编号


implementation
// 注意： 此函数要通过DataModule中的ADOConnection1与数据库相连
uses uDataModule;

Function Create_OrderNum(BH_FieldName,TableName,RQ_FieldName,orderFirst:string):string;
//-----------------------   Create_OrderNum  函数说明   ------------------------//
//----- 函数作用： 根据时间和数据库已有记录自动生成单据编号                ----//
//----- 参数说明： BH_FieldName(表的编号字段名),TableName(表名),           ----//
//-----            RQ_FieldName(日期字段名),orderFirst(单据开头标识)       ----//
//----- 调用方法： Create_OrderNum('YFK_BH','TDD_YFK','YFK_RQ','YFK');     ----//
//----- 返回值：  字符串类型  如：FKD_201008100001                         ----//
//----- 作者：    苏贵阳     2010-8-10                                     ----//
//-----------------------------------------------------------------------------//
var
  adoQ_temp : TADOQuery;
begin
  adoQ_temp:= TADOQuery.Create(nil);                     //生成临时的ADOQuery变量
  adoQ_temp.Connection := DM.ADOConn ;   //将adoQ_temp与ADOConnection1相连
  with adoQ_temp do
  begin
    Close;
    SQL.Clear;
    SQL.Add(Format('select %s from %s where %s=:rq',[BH_FieldName,TableName,RQ_FieldName]));
    Parameters.ParamByName('rq').Value := DateToStr(date());
    Open;
    if not adoQ_temp.IsEmpty then
    begin   //如果当天已有记录，则取出最大的编号，然后最后流水号加1
      Last;
      Result:=Format ('%s_%s',[orderFirst,IntToStr(StrToInt64(RightStr(Fields[0].AsString,12))+1)]);
    end
    else    //如果当天尚无记录，则生成一个当天的初始编号
    begin
       Result:=Format('%s_%s',[orderFirst,(FormatDateTime('yyyymmdd',date())+'0001')]);
    end;
    close;
  end;
  adoQ_temp.free;    //释放临时ADOQuery变量
end;
//---------------------------- 常见问题 -----------------------------//
//---- 问题：编号无法自动累加                                    ----//
//---- 解答：请检查该表中日期字段的格式                          ----//
//----       正确的格式如：  2010-08-10 00:00:00.000             ----//
//-------------------------------------------------------------------//


Function Create_RecordNum(BH_FieldName,TableName,RecordFirst:string):string;
//-----------------------   Create_RecordNum  函数说明   ----------------------//
//----- 函数作用： 自动生成单据编号，新生成的编号为已有最大编号的下一个    ----//
//----- 参数说明： BH_FieldName(表的编号字段名),TableName(表名),           ----//
//-----            orderFirst(单据开头标识)                                ----//
//----- 调用方法： Create_RecordNum('DTJ_BH','TCP_DTJ','DTJ')              ----//
//----- 返回值：  字符串类型  如：DTJ_0001                                 ----//
//----- 作者：    苏贵阳     2010-8-12                                     ----//
//-----------------------------------------------------------------------------//
var
  str : string;
  adoQ_temp : TADOQuery;
begin
  adoQ_temp:= TADOQuery.Create(nil);                     //生成临时的ADOQuery变量
  adoQ_temp.Connection := DM.ADOConn ;   //将adoQ_temp与ADOConnection1相连
  with adoQ_temp do
  begin
    Close;
    SQL.Clear;
    SQL.Add(Format('select %s from %s',[BH_FieldName,TableName]));
    Open;
    if not adoQ_temp.IsEmpty then
    begin   //取出最大的编号，然后最后流水号加1
      Last;
      str := IntToStr(StrToInt64(RightStr(Fields[0].AsString,4))+1);
      case  Length(str) of
        1: str :=Format('000%s',[str]);
        2: str :=Format('00%s',[str]);
        3: str :=Format('0%s',[str]);
        4: str :=Format('%s',[str]);
      end;
      Result:=Format ('%s_%s',[RecordFirst,str]);
    end
    else    //如果尚无记录，则生成一个初始编号
    begin
       Result:=Format('%s_%s',[RecordFirst,'0001']);
    end;
    close;
  end;
  adoQ_temp.free;
end;

Function GetNewBHStr(BHTableName, BHFieldName, BHFirst:string; BHLength:Integer):string;//获取唯一编号
//-----------------------   GetNewBHStr  函数说明   ---------------------------//
//----- 函数作用： 自动生成单据编号，新生成的编号为已有最大编号的下一个    ----//
//----- 参数说明： BHTableName(表名),BHFieldName(表的编号字段名),          ----//
//-----            BHFirst(单据开头标识),BHLength(流水号长度)              ----//
//----- 调用方法： GetNewBHStr('TCP_CPXX','CPXX_BH','CPX',4);              ----//
//----- 返回值：  字符串类型  如：CPX_0001                                 ----//
//----- 作者：                                                             ----//
//-----------------------------------------------------------------------------//
var
  BHStr, BHLen, BHNumStr: string;
  IPos, BHNum, I: Integer;
  adoQ_temp : TADOQuery;
begin
  adoQ_temp:= TADOQuery.Create(nil);                     //生成临时的ADOQuery变量
  adoQ_temp.Connection := DM.ADOConn ;   //将adoQ_temp与ADOConnection1相连
  with adoQ_temp do
  begin
    Close;
    SQL.Clear;
    SQL.Add(Format('select %s from %s',[BHFieldName,BHTableName]));
    Open;
    if not adoQ_temp.IsEmpty then
    begin   //如果当天已有记录，则取出最大的编号，然后最后流水号加1
      Last;
      BHStr:=Fields[0].AsString;
      OutputDebugString(PWideChar(BHStr));
      IPos :=Pos ('-',BHStr);

      if IPos > 0 then
      begin
        BHLen:=Copy (BHStr,IPos+1,Length (BHStr));
        BHStr:=Copy (BHStr,1,IPos);
        BHNumStr:=IntToStr(StrToIntDef(BHLen,1)+1);
        while Length (BHNumStr)<Length (BHLen) do BHNumStr:='0'+BHNumStr;
        BHStr:=BHStr+BHNumStr;
      end else
      begin
        BHStr:=Format ('%s-%d',[BHFirst,8888]);
      end;
    end
    else    //如果当天尚无记录，则生成一个当天的初始编号
    begin
      BHNumStr:=IntToStr (1);
      while Length (BHNumStr)<BHLength do BHNumStr:='0'+BHNumStr;
      BHStr:=Format('%s-%s',[BHFirst,BHNumStr]);
    end;
    close;
  end;
  Result:=BHStr;
  adoQ_temp.free;
end;

end.
